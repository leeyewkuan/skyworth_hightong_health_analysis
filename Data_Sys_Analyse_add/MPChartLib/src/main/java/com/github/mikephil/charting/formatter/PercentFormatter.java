
package com.github.mikephil.charting.formatter;


import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.List;

/**
 * This IValueFormatter is just for convenience and simply puts a "%" sign after
 * each value. (Recommeded for PieChart)
 *
 * @author Philipp Jahoda
 */
public class PercentFormatter implements IValueFormatter, IAxisValueFormatter {

    protected DecimalFormat mFormat;

    private List<PieEntry> mPieEntry;

    public PercentFormatter(List<PieEntry> entries) {
        mPieEntry = entries;
        mFormat = new DecimalFormat("###,###,##0.00");
    }

    public PercentFormatter() {
        mFormat = new DecimalFormat("###,###,##0.00");
    }
    /**
     * Allow a custom decimalformat
     *
     * @param format
     */
    public PercentFormatter(DecimalFormat format) {
        this.mFormat = format;
    }

    // IValueFormatter
    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        if (mPieEntry != null) {
            for (int i = 0; i < mPieEntry.size(); i++) {
                if (mPieEntry.get(i).getY() == entry.getY()) {
                    return mPieEntry.get(i).getLabel() + ": " + mFormat.format(value) + " %";
                }
            }
        }
        return mFormat.format(value) + " %";
    }

    // IAxisValueFormatter
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " %";
    }

    @Override
    public int getDecimalDigits() {
        return 1;
    }
}
