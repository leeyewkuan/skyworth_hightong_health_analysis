package com.sh.pangea.dsa.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.MenuUtil;
import com.sh.pangea.dsa.adapter.ExpandableListAdapter;
import com.sh.pangea.dsa.bean.MenuInfo;
import com.sh.pangea.dsa.ui.activity.LineChartActivity;
import com.sh.pangea.dsa.ui.activity.TrendAuthDayActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 主页 Content
 */
public class MainContentFragment extends Fragment {
    private static final int REFRESH_SUCCESS = 0x001;
    @BindView(R.id.expandlist)
    ExpandableListView expandlist;
    private MenuInfo parentInfo;
    private Context mContext;
    private List<MenuInfo> list = new ArrayList<>();
    private Map<Integer, List<MenuInfo>> map = new HashMap<>();
    private ExpandableListAdapter myAdapter;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case REFRESH_SUCCESS:
                    list = MenuUtil.getMenuListInfo(parentInfo.getId());
                    map = MenuUtil.getMenuChildInfo(parentInfo.getId());
                    if (myAdapter != null) {
                        myAdapter.refresh(list, map);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        View view = inflater.inflate(R.layout.fragment_maincontent, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        myAdapter = new ExpandableListAdapter(mContext, list, map);
        expandlist.setGroupIndicator(null);//这里不显示系统默认的group indicator
        expandlist.setAdapter(myAdapter);
        for (int i = 0; i < list.size(); i++) {
            expandlist.expandGroup(i);
        }
        registerForContextMenu(expandlist);//给ExpandListView添加上下文菜单
        expandlist.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.i("Main","onChildClick()   parentPosition:" + parentInfo.getId() + "   groupPosition:" + groupPosition + "   childPosition:" + childPosition);
                Intent intent;
                MenuInfo groupInfo = list.get(groupPosition);
                if(parentInfo.getId().equals("T5")){
//                    intent = new Intent(mContext, WirelessAuthenticationLineChart.class);
                    intent = new Intent(mContext, TrendAuthDayActivity.class);
                }else{
                    intent = new Intent(mContext, LineChartActivity.class);
                }
                MenuInfo childInfo = map.get(groupPosition).get(childPosition);
                Bundle bundle = new Bundle();
                bundle.putSerializable("parentInfo", parentInfo);
                bundle.putSerializable("groupInfo", groupInfo);
                bundle.putSerializable("childInfo", childInfo);

                myAdapter.setSelectedItem(groupPosition,childPosition);
                myAdapter.notifyDataSetChanged();
                Log.i("CGZ  parentInfo",parentInfo.toString());
                Log.i("CGZ  groupInfo",groupInfo.toString());
                Log.i("CGZ  childInfo",childInfo.toString());
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);
                return false;
            }
        });

        expandlist.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });
    }

    public void updateContentList(MenuInfo info) {
        Log.d("YG", "updateContentList");
        this.parentInfo = info;
        Message msg = new Message();
        msg.what = REFRESH_SUCCESS;
        mHandler.handleMessage(msg);
        if (expandlist != null) {
            for (int i = 0; i < list.size(); i++) {
                expandlist.expandGroup(i);
            }
        }
        if (myAdapter != null){
            myAdapter.setSelectedItem(-1,-1);
            myAdapter.notifyDataSetChanged();
        }

    }
}
