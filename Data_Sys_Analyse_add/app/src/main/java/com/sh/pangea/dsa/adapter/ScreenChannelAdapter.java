package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Data_Sys_Analyse
 * Created by LB on 2016/10/13,10:04
 */

public class ScreenChannelAdapter extends BaseAdapter {
    private Context context;

    public ScreenChannelAdapter(Context context) {
        this.context = context;
    }

    private List<ModuleItem> list;
    private int selectPost;

    @Override
    public int getCount() {
        int number = 0;
        if (list != null) {
            number = list.size();
        }
        return number;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.screen_channel_adapter, null);
            viewHolder = new ViewHolder();
            viewHolder.tv = (TextView) convertView.findViewById(R.id.tv_channel_item);
            viewHolder.view = convertView.findViewById(R.id.fl_background);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ModuleItem moduleItem = list.get(position);
        viewHolder.tv.setText(moduleItem.getName());
        if (position == selectPost) {
            viewHolder.view.setBackgroundResource(R.mipmap.bg_xuanji_focus);
            viewHolder.tv.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            viewHolder.view.setBackgroundResource(R.mipmap.bg_xuanji);
            viewHolder.tv.setTextColor(context.getResources().getColor(R.color.black));
        }
        return convertView;
    }

    private static class ViewHolder {
        View view;
        TextView tv;
    }

    public void update(List<ModuleItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSelect(int post) {
        this.selectPost = post;
        notifyDataSetChanged();
    }
}
