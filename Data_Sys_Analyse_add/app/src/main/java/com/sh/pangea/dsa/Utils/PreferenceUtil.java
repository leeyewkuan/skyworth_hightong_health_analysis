package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

import java.util.ArrayList;

public class PreferenceUtil {
    private static PreferenceUtil preferenceUtil;
    private static ArrayList<Editor> editors = new ArrayList<Editor>();
    /**
     * 期望倍数字段
     */
    public static final String EXPECT_MULTIPLE = "expect_multiple";
    private SharedPreferences sp;
    private Editor editor;

    public PreferenceUtil() {
    }

    public SharedPreferences createSharedPreferences(Context context,
                                                     String name) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                name, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public Editor createEditor(Context context, String name) {
        return createSharedPreferences(context, name).edit();
    }

    public PreferenceUtil(Context context) {
        sp = context.getSharedPreferences("SharedPreferences",
                Context.MODE_PRIVATE);
        editor = sp.edit();
        editors.add(editor);
    }

    public static PreferenceUtil getInstance(Context context) {
        if (preferenceUtil == null) {
            preferenceUtil = new PreferenceUtil(context);
        }
        return preferenceUtil;
    }

    public void saveLong(String key, long l) {
        editor.putLong(key, l);
        editor.commit();
    }

    public long getLong(String key, long defaultlong) {
        return sp.getLong(key, defaultlong);
    }

    public void saveBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key, boolean defaultboolean) {
        return sp.getBoolean(key, defaultboolean);
    }

    public void saveFloat(String key, Float value) {
        editor.putFloat(key, value);
        editor.commit();
    }

    public Float getFloat(String key, int defaultFloat) {
        return sp.getFloat(key, defaultFloat);
    }

    public void saveInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(String key, int defaultInt) {
        return sp.getInt(key, defaultInt);
    }

    public String getString(String key, String defaultValue) {
        return sp.getString(key, defaultValue);
    }

    public void saveString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void deleteSharedPreferences() {
        for (Editor editor : editors) {
            clearEditor(editor);
        }
    }

    private void clearEditor(Editor editor) {
        editor.clear();
        editor.commit();
    }

    public void removeAll() {
        editor.clear();
        editor.commit();
    }

    public void remove(String key) {
        editor.remove(key);
        editor.commit();
    }

    public void register(Context context, String name,
                         OnSharedPreferenceChangeListener listener) {
        if (sp != null && listener != null) {
            sp.registerOnSharedPreferenceChangeListener(listener);
        }
    }

    public void unRegister(Context context, String name,
                           OnSharedPreferenceChangeListener listener) {
        if (sp != null && listener != null) {
            sp.unregisterOnSharedPreferenceChangeListener(listener);
        }
    }

}
