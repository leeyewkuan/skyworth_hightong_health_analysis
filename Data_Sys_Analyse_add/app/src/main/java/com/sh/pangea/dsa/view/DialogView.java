package com.sh.pangea.dsa.view;

import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.OnCustomDialogListener;

public class DialogView extends FrameLayout implements OnClickListener {
	private TextView promptTextView;

	private Button confirmButton;

	private Button cancelButton;
	
	private View devidOreLine;

	private OnCustomDialogListener onCustomDialogListener;
	private AlertDialog dialog;
	public DialogView(Context context) {
		super(context);
		init();
	}

	public void setOnCustomDialogListener(
			OnCustomDialogListener onCustomDialogListener) {
		this.onCustomDialogListener = onCustomDialogListener;
	}

	private void init() {
		LayoutInflater inflator = (LayoutInflater) this.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflator.inflate(R.layout.dialog_common, this);
		promptTextView = (TextView) findViewById(R.id.promptTextView);

		confirmButton = (Button) findViewById(R.id.confirmButton);
		confirmButton.setOnClickListener(this);

		cancelButton = (Button) findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(this);

		devidOreLine = findViewById(R.id.divider_ore_line);
	}

	public void creatDialog(String promtp, boolean isSingButton,
							OnCustomDialogListener listener) {
		creatDialog(promtp, "", "", isSingButton, listener);
	}

	public void creatDialog(String promtp, String single,
							OnCustomDialogListener listener) {
		creatDialog(promtp, "", single, true, listener);
	}

	public void creatDialog(String promtp, String cacel, String confirm,
							boolean isSingleButton, OnCustomDialogListener listener) {
		if (!TextUtils.isEmpty(promtp)) {
			promptTextView.setText(promtp);
		}
		if (!TextUtils.isEmpty(cacel)) {
			cancelButton.setText(cacel);
		}
		if (!TextUtils.isEmpty(confirm)) {
			confirmButton.setText(confirm);
		}
		if (isSingleButton) {
			singButton();
		}
		if (listener != null && onCustomDialogListener == null) {
			setOnCustomDialogListener(listener);
		}
	}
	
	public void setDialog(AlertDialog dialog) {
		this.dialog = dialog;
	}
	
	private void singButton() {
		cancelButton.setVisibility(View.GONE);
		devidOreLine.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
		if(dialog != null){
			dialog.dismiss();
		}
		if (onCustomDialogListener == null) {
			return;
		}
		switch (v.getId()) {
		case R.id.confirmButton:
			onCustomDialogListener.confirm();
			break;
		case R.id.cancelButton:
			onCustomDialogListener.cancel();
			break;
		default:
			break;
		}
	}

}
