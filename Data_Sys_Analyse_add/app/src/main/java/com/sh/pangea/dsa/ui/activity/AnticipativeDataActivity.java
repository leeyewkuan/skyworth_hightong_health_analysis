package com.sh.pangea.dsa.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.PreferenceUtil;
import com.sh.pangea.dsa.adapter.AnticipativeDataAdapter;

import java.util.ArrayList;
import java.util.List;

public class AnticipativeDataActivity extends Activity  {

    //上下文
    private Context mContext;

    //控件
    private RecyclerView hope_recyclerView;
    private EditText hope_eText;
    private CheckBox hope_cBox;
    private Button hope_btn_restore;
    private Button hope_btn_confirm;
    private TextView hope_tView;
    private TextView hope_note_tView;
    private TextView tv_weight;
    private ImageButton analyze_back;

    private List<String> items = new ArrayList<>();

    private AnticipativeDataAdapter mAdapter;
    private int resultCode = 6;

    {
        items.add("1.0");
        items.add("1.1");
        items.add("1.2");
        items.add("1.5");
        items.add("2.0");
        items.add("3.0");
        items.add("5.0");
        items.add("8.0");
        items.add("10.0");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_anticipantdata);
        mContext = this;
        initView();
        initData();
        initListener();
    }

    private void initView() {
        hope_recyclerView = (RecyclerView) findViewById(R.id.hope_recyclerView);
        hope_eText = (EditText) findViewById(R.id.hope_eText);
        hope_cBox = (CheckBox) findViewById(R.id.hope_cBox);
        hope_btn_restore = (Button) findViewById(R.id.hope_btn_restore);
        hope_btn_confirm = (Button) findViewById(R.id.hope_btn_confirm);
        hope_tView = (TextView) findViewById(R.id.hope_tView);
        hope_note_tView = (TextView) findViewById(R.id.hope_note_tView);
        tv_weight = (TextView) findViewById(R.id.tv_weight);
        analyze_back = (ImageButton) findViewById(R.id.analyze_back);
    }

    private void initData() {
        hope_recyclerView.setLayoutManager(new GridLayoutManager(mContext,3));
        mAdapter = new AnticipativeDataAdapter(items,mContext);
        hope_recyclerView.setAdapter(mAdapter);

        hope_tView.setText("自动隐藏左侧栏中的\"权重\"项。");
        hope_note_tView.setText("注：权重设置后，一直生效。\r\n如需还原，请点击\"还原\"按钮。");
        tv_weight.setText("当前选择权重为:"+PreferenceUtil.getInstance(mContext).getFloat(PreferenceUtil.EXPECT_MULTIPLE, 1));

    }

    private void initListener() {
        analyze_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setIsHide(false);
                finish();
            }
        });
        //默认条目的点击事件
        mAdapter.setOnItemClickListener(new AnticipativeDataAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                hope_eText.setText(items.get(position));
                tv_weight.setText("当前选择权重为:"+items.get(position));
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });
        //还原按钮的点击事件
        hope_btn_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 保存权重为1倍
                PreferenceUtil.getInstance(mContext).saveFloat(PreferenceUtil.EXPECT_MULTIPLE, (float)1);
                setIsHide(true);
                finish();
            }
        });
        //确认按钮的点击事件
        hope_btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 保存权重
                if (!"".equals(hope_eText.getText().toString())){
                    PreferenceUtil.getInstance(mContext).saveFloat(PreferenceUtil.EXPECT_MULTIPLE,Float.parseFloat(hope_eText.getText().toString()));
                }
                setIsHide(hope_cBox.isChecked());
                finish();

            }
        });
        hope_eText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tv_weight.setText("当前选择权重为:"+hope_eText.getText().toString());
            }
        });
    }

    //通知主页是否隐藏"权重"
    private void setIsHide(boolean ishide){
        Intent mIntent = new Intent();
        mIntent.putExtra("ishide",ishide);
        // 设置结果，并进行传送
        this.setResult(resultCode, mIntent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            setIsHide(false);
        }
        return super.onKeyDown(keyCode, event);

    }
}
