package com.sh.pangea.dsa.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.ui.fragment.SelectHospitalFragment;

import java.util.List;

/**
 * Creat By 675
 * {@link RecyclerView.Adapter} that can display a {@link ModuleItem} and makes a call to the
 * specified {@link SelectHospitalFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MySelectHospitalRecyclerViewAdapter extends RecyclerView.Adapter<MySelectHospitalRecyclerViewAdapter.ViewHolder> {

    private final List<ModuleItem> mValues;
    private final SelectHospitalFragment.OnListFragmentInteractionListener mListener;

    public MySelectHospitalRecyclerViewAdapter(List<ModuleItem> items, SelectHospitalFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_selecthospital, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mView.setBackgroundResource(R.drawable.recyclerview_touch_selector);
        holder.mIdView.setText(mValues.get(position).getName());
        holder.mContentView.setText(mValues.get(position).getValue());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public ModuleItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.name);
            mContentView = (TextView) view.findViewById(R.id.value);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    public void refurbish(){
        this.notifyDataSetChanged();
    }
}
