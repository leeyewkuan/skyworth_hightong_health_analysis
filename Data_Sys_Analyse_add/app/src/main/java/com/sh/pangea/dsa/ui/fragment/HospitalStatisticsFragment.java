package com.sh.pangea.dsa.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sh.lib.bossstat.bean.WxUserStat;
import com.sh.lib.cmswifi.bean.CmsWifiStat;
import com.sh.lib.cmswifi.bean.SsInfo;
import com.sh.lib.cmswifi.callback.CmsWifiStatListener;
import com.sh.lib.cmswifi.impl.CmsWifiManager;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.PreferenceUtil;
import com.sh.pangea.dsa.adapter.MonitoringAdapter;
import com.sh.pangea.dsa.observer.CurentTypeObservable;
import com.sh.pangea.dsa.observer.CurentTypeObserver;
import com.sh.pangea.dsa.ui.activity.NounExplanationActivity;
import com.sh.pangea.dsa.ui.activity.SelectHospitalActivity;
import com.sh.pangea.dsa.view.LoadingDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 除开公众号的数据统计
 * Created by zhouyucheng on 2017/5/31.
 */

public class HospitalStatisticsFragment extends Fragment implements View.OnClickListener, CurentTypeObserver {
    private String hospitalName = "全部";
    private String hospitalValue = "";
    private TextView tvHospitol;
    private RelativeLayout rlSelectHospital;
    private int Etype = 1;
    private ListView lv_data_show;
    private ImageView iv_translate;
    private Button bt_updata;
    private TextView tv_updata_time;
    private LoadingDialog loading;
    private TextView tv_powprofit_value;
    private TextView tv_power_value;
    List<String> list = new ArrayList<>();
    List<String> datalist = new ArrayList<>();
    MonitoringAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_hospital_current_data, container, false);
        initView(inflate);
        initDatas();
        return inflate;
    }

    private void initDatas() {
        CurentTypeObservable.getInstance().registerObserver(this);
        //wxInfo
        list.add("当日吸粉数:");
        list.add("当日吸粉收益:");
        list.add("当日订单数:");
        list.add("当日微信回调次数:");
        list.add("当日微信回调用户数:");
        //ssInfo
        list.add("总吸粉数:");
        list.add("当日获取订单数:");
        list.add("当日获取订单设备数:");
        list.add("当日成功上网数:");
        list.add("当日成功上网设备数:");
        //accessInfo
        list.add("当日接入数:");
        list.add("当日接入设备数:");
        list.add("当日新增设备数:");
        //jktvInfo
        list.add("当日健康电视订单数:");

    }

    private void initView(View view) {
        loading = new LoadingDialog(getActivity());

        tvHospitol = (TextView) view.findViewById(R.id.tv_hospital);
        tv_updata_time = (TextView) view.findViewById(R.id.tv_updata_time);
        bt_updata = (Button) view.findViewById(R.id.bt_updata);
        bt_updata.setOnClickListener(this);
        iv_translate = (ImageView) view.findViewById(R.id.iv_translate);
        iv_translate.setOnClickListener(this);

        tv_powprofit_value = (TextView) view.findViewById(R.id.tv_powprofit_value);
        tv_power_value = (TextView) view.findViewById(R.id.tv_power_value);

        rlSelectHospital = (RelativeLayout) view.findViewById(R.id.rl_hospital_select);
        rlSelectHospital.setOnClickListener(this);
        lv_data_show = (ListView) view.findViewById(R.id.lv_data_show);
        adapter = new MonitoringAdapter(getActivity(), datalist);
        lv_data_show.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_hospital_select:
                Intent intent = new Intent(getActivity(), SelectHospitalActivity.class);
                intent.putExtra("name", hospitalName);
                intent.putExtra("value", hospitalValue);
                startActivityForResult(intent, 1);
                break;
            case R.id.bt_updata:
                getData();
                break;
            case R.id.iv_translate:
                Intent explanIntent = new Intent(getActivity(), NounExplanationActivity.class);
                explanIntent.putExtra("type", Etype);
                startActivityForResult(explanIntent, 1);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && data != null) {
            String name = data.getStringExtra("name");
            String value = data.getStringExtra("value");
            if (hospitalName.equals(name) && hospitalValue.equals(value)) {
                return;
            } else {
                hospitalName = name;
                hospitalValue = value;
                if (!TextUtils.isEmpty(hospitalName)) {
                    tvHospitol.setText(hospitalName);
                    getSsDayInfo();
                }
            }
        }
    }

    private void getSsDayInfo() {
        if (TextUtils.isEmpty(hospitalValue)) {
            Toast.makeText(getActivity(), "请先选择一个医院", Toast.LENGTH_LONG).show();
            return;
        }
        CmsWifiManager.instence().getSsDayInfo(hospitalValue, 5000, 5000, new CmsWifiStatListener() {
            @Override
            public void onSuccess(CmsWifiStat cmsWifiStat) {
                if (loading != null) {
                    loading.cancel();
                }
                cmsWifiStat = amplifyData(cmsWifiStat);
                setMainData(cmsWifiStat);
                setAdapterData(cmsWifiStat);
            }

            @Override
            public void onPrepare(String s) {
                initDataNull();
                if (loading != null) {
                    loading.show();
                }
            }

            @Override
            public void onException(Exception e) {
                if (loading != null) {
                    loading.cancel();
                }
                Toast.makeText(getActivity(), "网络错误", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFail(int i) {
                if (loading != null) {
                    loading.cancel();
                }
                Toast.makeText(getActivity(), "数据获取失败", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void ssDayAnalysis() {
        CmsWifiManager.instence().getSsDayAnalysis(5000, 5000, new CmsWifiStatListener() {
            @Override
            public void onSuccess(CmsWifiStat cmsWifiStat) {
                if (loading != null) {
                    loading.cancel();
                }
                cmsWifiStat = amplifyData(cmsWifiStat);
                setMainData(cmsWifiStat);
                setAdapterData(cmsWifiStat);
            }

            @Override
            public void onPrepare(String s) {
                if (loading != null) {
                    loading.show();
                }
            }

            @Override
            public void onException(Exception e) {
                if (loading != null) {
                    loading.cancel();
                }
                Toast.makeText(getActivity(), "网络错误", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFail(int i) {
                if (loading != null) {
                    loading.cancel();
                }
                Toast.makeText(getActivity(), "数据获取失败", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void yhDayAnalysis() {
        CmsWifiManager.instence().getYhDayAnalysis(5000, 5000, new CmsWifiStatListener() {
            @Override
            public void onSuccess(CmsWifiStat cmsWifiStat) {
                if (loading != null) {
                    loading.cancel();
                }
                cmsWifiStat = amplifyData(cmsWifiStat);
                setMainData(cmsWifiStat);
                setAdapterData(cmsWifiStat);
            }

            @Override
            public void onPrepare(String s) {
                if (loading != null) {
                    loading.show();
                }
            }

            @Override
            public void onException(Exception e) {
                if (loading != null) {
                    loading.cancel();
                }
                Toast.makeText(getActivity(), "网络错误", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFail(int i) {
                if (loading != null) {
                    loading.cancel();
                }
                Toast.makeText(getActivity(), "数据获取失败", Toast.LENGTH_LONG).show();
            }
        });

    }

    /**
     * 放大数据
     *
     * @param cmsWifiStat
     * @return
     */
    private CmsWifiStat amplifyData(CmsWifiStat cmsWifiStat) {
        if (cmsWifiStat == null) {
            Log.i("Main", "按期望倍数放大,现有数据为空");
            return new CmsWifiStat();
        }

        if (cmsWifiStat.getSsInfo() != null) {
            String connectDeviceNums = cmsWifiStat.getSsInfo().getConnectDeviceNums();
            if (!TextUtils.isEmpty(connectDeviceNums)) {
                String string = changeData(connectDeviceNums);
                cmsWifiStat.getSsInfo().setConnectDeviceNums(string);
            }
            String orderDeviceNums = cmsWifiStat.getSsInfo().getOrderDeviceNums();
            if (!TextUtils.isEmpty(orderDeviceNums)) {
                String string = changeData(orderDeviceNums);
                cmsWifiStat.getSsInfo().setOrderDeviceNums(string);
            }
            String connectNums = cmsWifiStat.getSsInfo().getConnectNums();
            if (!TextUtils.isEmpty(connectNums)) {
                String string = changeData(connectNums);
                cmsWifiStat.getSsInfo().setConnectNums(string);
            }
            String orderNums = cmsWifiStat.getSsInfo().getOrderNums();
            if (!TextUtils.isEmpty(orderNums)) {
                String string = changeData(orderNums);
                cmsWifiStat.getSsInfo().setOrderNums(string);
            }
            String subscribeNums = cmsWifiStat.getSsInfo().getSubscribeNums();
            if (!TextUtils.isEmpty(subscribeNums)) {
                String string = changeData(subscribeNums);
                cmsWifiStat.getSsInfo().setSubscribeNums(string);
            }
        }

        if (cmsWifiStat.getAccessInfo() != null) {
            String accessNums = cmsWifiStat.getAccessInfo().getAccessNums();
            if (!TextUtils.isEmpty(accessNums)) {
                String string = changeData(accessNums);
                cmsWifiStat.getAccessInfo().setAccessNums(string);
            }
            String accessDeviceNums = cmsWifiStat.getAccessInfo().getAccessDeviceNums();
            if (!TextUtils.isEmpty(accessDeviceNums)) {
                String string = changeData(accessDeviceNums);
                cmsWifiStat.getAccessInfo().setAccessDeviceNums(string);
            }
            String newDeviceNums = cmsWifiStat.getAccessInfo().getNewDeviceNums();
            if (!TextUtils.isEmpty(newDeviceNums)) {
                String string = changeData(newDeviceNums);
                cmsWifiStat.getAccessInfo().setNewDeviceNums(string);
            }
        }
        if (cmsWifiStat.getJktvInfo() != null) {
            String orderNums = cmsWifiStat.getJktvInfo().getOrderNums();
            if (!TextUtils.isEmpty(orderNums)) {
                String string = changeData(orderNums);
                cmsWifiStat.getJktvInfo().setOrderNums(string);
            }

        }
        if ((cmsWifiStat.getWxInfo() != null)) {
            String orderNums = cmsWifiStat.getWxInfo().getOrderNums();
            if (!TextUtils.isEmpty(orderNums)) {
                String string = changeData(orderNums);
                cmsWifiStat.getWxInfo().setOrderNums(string);
            }
            String orderDeviceNums = cmsWifiStat.getWxInfo().getOrderDeviceNums();
            if (!TextUtils.isEmpty(orderDeviceNums)) {
                String string = changeData(orderDeviceNums);
                cmsWifiStat.getWxInfo().setOrderDeviceNums(string);
            }
            String subscribeNums = cmsWifiStat.getWxInfo().getSubscribeNums();
            if (!TextUtils.isEmpty(subscribeNums)) {
                String string = changeData(subscribeNums);
                cmsWifiStat.getWxInfo().setSubscribeNums(string);
            }
            String wxcbNums = cmsWifiStat.getWxInfo().getWxcbNums();
            if (!TextUtils.isEmpty(wxcbNums)) {
                String string = changeData(wxcbNums);
                cmsWifiStat.getWxInfo().setWxcbNums(string);
            }
            String subscribeTotleNums = cmsWifiStat.getWxInfo().getSubscribeTotleNums();
            if (!TextUtils.isEmpty(subscribeTotleNums)) {
                String string = changeData(subscribeTotleNums);
                cmsWifiStat.getWxInfo().setSubscribeTotleNums(string);
            }
            String wxcbUserNums = cmsWifiStat.getWxInfo().getWxcbUserNums();
            if (!TextUtils.isEmpty(wxcbUserNums)) {
                String string = changeData(wxcbUserNums);
                cmsWifiStat.getWxInfo().setWxcbUserNums(string);
            }
            String income = cmsWifiStat.getWxInfo().getIncome();
            if (!TextUtils.isEmpty(income)) {
                String string = changeData(income);
                cmsWifiStat.getWxInfo().setIncome(string);
            }
            String incomeTotle = cmsWifiStat.getWxInfo().getIncomeTotle();
            if (!TextUtils.isEmpty(incomeTotle)) {
                String string = changeData(incomeTotle);
                cmsWifiStat.getWxInfo().setIncomeTotle(string);
            }
        }

        return cmsWifiStat;
    }


    /**
     * 重新获取本地数据清空
     */
    private void initDataNull() {
        tv_powprofit_value.setText("--");
        tv_power_value.setText("--");
        lv_data_show.setVisibility(View.GONE);
    }

    public void refreshData() {
        getData();
    }

    @Override
    public void updateType(int type) {
        Log.i("type", "updateType: " + type);
        this.Etype = type;
        if (Etype == 1) {
            hospitalValue = "";
            tvHospitol.setText("全部医院");
            rlSelectHospital.setVisibility(View.VISIBLE);
        } else {
            rlSelectHospital.setVisibility(View.GONE);
        }
        getData();
    }

    private void getData() {
        tv_updata_time.setText(getNowTime());
        initDataNull();
        switch (Etype) {
            case 1:
                getSsDayInfo();
                break;
            case 2:
                ssDayAnalysis();
                break;
            case 3:
                yhDayAnalysis();
                break;
        }
    }

    @Override
    public void onDestroy() {
        CurentTypeObservable.getInstance().unregisterObserver(this);
        Log.i("log", "onDestroy:后三个一类页面关闭 ");
        super.onDestroy();
    }

    private String getNowTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    private String changeData(String times) {
        String newData = "";
        if (!TextUtils.isEmpty(times)) {
            float data = Float.parseFloat(times);
            // 期望倍数
            float multiple = PreferenceUtil.getInstance(getContext()).getFloat(PreferenceUtil.EXPECT_MULTIPLE, 1);
            newData = newData + (int) (data * multiple);
        }
        return newData;
    }

    /**
     * 显示显目的两个数据
     *
     * @param cmsWifiStat
     */
    private void setMainData(CmsWifiStat cmsWifiStat) {
        if (cmsWifiStat.getWxInfo() != null) {
            String subscribeTotleNums = cmsWifiStat.getWxInfo().getSubscribeTotleNums() + "";
            if (!TextUtils.isEmpty(subscribeTotleNums)) {
                tv_power_value.setText(subscribeTotleNums);
            } else {
                tv_power_value.setText("--");
            }
            String incomeTotle = cmsWifiStat.getWxInfo().getIncomeTotle() + "";
            if (!TextUtils.isEmpty(incomeTotle)) {
                tv_powprofit_value.setText(incomeTotle);
            } else {
                tv_powprofit_value.setText("--");
            }
        }

    }

    /**
     * 下半部分列表处理
     *
     * @param cmsWifiStat
     */
    private void setAdapterData(CmsWifiStat cmsWifiStat) {
        datalist.clear();
        if (cmsWifiStat.getWxInfo() != null) {
            String subscribeNums = cmsWifiStat.getWxInfo().getSubscribeNums();
            if (!TextUtils.isEmpty(subscribeNums)) {
                datalist.add(list.get(0) + subscribeNums);
            } else {
                datalist.add(list.get(0) + "--");
            }
            String income = cmsWifiStat.getWxInfo().getIncome();
            if (!TextUtils.isEmpty(income)) {
                datalist.add(list.get(1) + income);
            } else {
                datalist.add(list.get(1) + "--");
            }
            String orderNums = cmsWifiStat.getWxInfo().getOrderNums();
            if (!TextUtils.isEmpty(orderNums)) {
                datalist.add(list.get(2) + orderNums);
            } else {
                datalist.add(list.get(2) + "--");
            }
            String wxcbNums = cmsWifiStat.getWxInfo().getWxcbNums();
            if (!TextUtils.isEmpty(wxcbNums)) {
                datalist.add(list.get(3) + wxcbNums);
            } else {
                datalist.add(list.get(3) + "--");
            }
            String wxcbUserNums = cmsWifiStat.getWxInfo().getWxcbUserNums();
            if (!TextUtils.isEmpty(wxcbUserNums)) {
                datalist.add(list.get(4) + wxcbUserNums);
            } else {
                datalist.add(list.get(4) + "--");
            }
        } else {
            datalist.add(list.get(0) + "--");
            datalist.add(list.get(1) + "--");
            datalist.add(list.get(2) + "--");
            datalist.add(list.get(3) + "--");
            datalist.add(list.get(4) + "--");
        }
        if (cmsWifiStat.getSsInfo() != null) {
            String subscribeNums = cmsWifiStat.getSsInfo().getSubscribeNums();
            if (!TextUtils.isEmpty(subscribeNums)) {
                datalist.add(list.get(5) + subscribeNums);
            } else {
                datalist.add(list.get(5) + "--");
            }
            String orderNums = cmsWifiStat.getSsInfo().getOrderNums();
            if (!TextUtils.isEmpty(orderNums)) {
                datalist.add(list.get(6) + orderNums);
            } else {
                datalist.add(list.get(6) + "--");
            }
            String wxcbUserNums = cmsWifiStat.getSsInfo().getOrderDeviceNums();
            if (!TextUtils.isEmpty(wxcbUserNums)) {
                datalist.add(list.get(7) + wxcbUserNums);
            } else {
                datalist.add(list.get(7) + "--");
            }
            String connectNums = cmsWifiStat.getSsInfo().getConnectNums();
            if (!TextUtils.isEmpty(connectNums)) {
                datalist.add(list.get(8) + connectNums);
            } else {
                datalist.add(list.get(8) + "--");
            }
            String connectDeviceNums = cmsWifiStat.getSsInfo().getConnectDeviceNums();
            if (!TextUtils.isEmpty(connectDeviceNums)) {
                datalist.add(list.get(9) + connectDeviceNums);
            } else {
                datalist.add(list.get(9) + "--");
            }
        } else {
            datalist.add(list.get(5) + "--");
            datalist.add(list.get(6) + "--");
            datalist.add(list.get(7) + "--");
            datalist.add(list.get(8) + "--");
            datalist.add(list.get(9) + "--");
        }
        if (cmsWifiStat.getAccessInfo() != null) {
            String accessNums = cmsWifiStat.getAccessInfo().getAccessNums();
            if (!TextUtils.isEmpty(accessNums)) {
                datalist.add(list.get(10) + accessNums);
            } else {
                datalist.add(list.get(10) + "--");
            }
            String accessDeviceNums = cmsWifiStat.getAccessInfo().getAccessDeviceNums();
            if (!TextUtils.isEmpty(accessDeviceNums)) {
                datalist.add(list.get(11) + accessDeviceNums);
            } else {
                datalist.add(list.get(11) + "--");
            }
            String newDeviceNums = cmsWifiStat.getAccessInfo().getNewDeviceNums();
            if (!TextUtils.isEmpty(newDeviceNums)) {
                datalist.add(list.get(12) + newDeviceNums);
            } else {
                datalist.add(list.get(12) + "--");
            }

        } else {
            datalist.add(list.get(10) + "--");
            datalist.add(list.get(11) + "--");
            datalist.add(list.get(12) + "--");
        }
        if (cmsWifiStat.getJktvInfo() != null) {
            String orderNums = cmsWifiStat.getJktvInfo().getOrderNums();
            if (!TextUtils.isEmpty(orderNums)) {
                datalist.add(list.get(13) + orderNums);
            } else {
                datalist.add(list.get(13) + "--");
            }
        } else {
            datalist.add(list.get(13) + "--");
        }
        lv_data_show.setVisibility(View.VISIBLE);
        adapter.updata(datalist);
    }

}
