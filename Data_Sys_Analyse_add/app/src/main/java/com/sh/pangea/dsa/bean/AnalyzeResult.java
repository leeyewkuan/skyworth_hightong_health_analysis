package com.sh.pangea.dsa.bean;

import android.os.Parcel;

import com.sh.lib.dsa.bean.analyzeresult.BaseAnalyzeResult;
import com.sh.lib.dsa.bean.analyzeresult.LinearItem;

import java.util.List;

/**
 * Created by TianGenhu on 2016/12/26.
 */

public class AnalyzeResult extends BaseAnalyzeResult {

    private List<String> xForGraph;
    private List<LinearItem> yForGraph;

    public List<LinearItem> getyForGraph() {
        return yForGraph;
    }

    public void setyForGraph(List<LinearItem> yForGraph) {
        this.yForGraph = yForGraph;
    }

    public List<String> getxForGraph() {
        return xForGraph;
    }

    public void setxForGraph(List<String> xForGraph) {
        this.xForGraph = xForGraph;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.xForGraph);
        dest.writeTypedList(this.yForGraph);
    }

    public AnalyzeResult() {
    }

    protected AnalyzeResult(Parcel in) {
        this.xForGraph = in.createStringArrayList();
        this.yForGraph = in.createTypedArrayList(LinearItem.CREATOR);
    }

    public static final Creator<AnalyzeResult> CREATOR = new Creator<AnalyzeResult>() {
        @Override
        public AnalyzeResult createFromParcel(Parcel source) {
            return new AnalyzeResult(source);
        }

        @Override
        public AnalyzeResult[] newArray(int size) {
            return new AnalyzeResult[size];
        }
    };
}
