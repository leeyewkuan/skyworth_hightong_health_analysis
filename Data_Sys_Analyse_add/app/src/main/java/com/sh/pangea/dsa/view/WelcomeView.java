package com.sh.pangea.dsa.view;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.sh.pangea.dsa.R;


/**
 *
 */
public class WelcomeView extends LinearLayout {

    private EditText userName, passWord;

    private ImageButton ib_showPassword;

    public WelcomeView(Context context) {
        super(context);
        loadView();
    }

    public WelcomeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadView();
    }

    public WelcomeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        loadView();
    }

    private void loadView(){
        setOrientation(VERTICAL);
        LayoutInflater.from(getContext()).inflate(R.layout.welcome_view, this);
        userName = (EditText) findViewById(R.id.username);
        passWord = (EditText) findViewById(R.id.password);
        ib_showPassword = (ImageButton) findViewById(R.id.ib_showPassword);
        ib_showPassword.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                if (passWord.getInputType() == 129){
                    ib_showPassword.setBackgroundResource(R.mipmap.open_eye);
                    passWord.setInputType(144);//144就是显示文字，不显示点点点
                    Log.i("密码","密码已显示");
                } else {
                    ib_showPassword.setBackgroundResource(R.mipmap.close_eye);
                    passWord.setInputType(129);//129就是显示点点点，不显示文字
                    Log.i("密码","密码已隐藏");
                }
                passWord.setSelection(passWord.getText().toString().length());
            }
        });
    }

    @Override
    public void setFocusable(boolean focusable) {
        super.setFocusable(focusable);
        userName.setFocusable(focusable);
        passWord.setFocusable(focusable);
    }

    public String getUserName()
    {
        return userName.getText().toString();
    }

    public String getPassWord()
    {
        return passWord.getText().toString();
    }
}
