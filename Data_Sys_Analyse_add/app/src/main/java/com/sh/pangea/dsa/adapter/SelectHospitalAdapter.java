package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sh.lib.dsa.bean.ModuleItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuqiwu on 2017/1/17.
 */

public class SelectHospitalAdapter extends BaseAdapter {
    private List<ModuleItem> hospitales = new ArrayList<>();
    private Context mContext;

    public SelectHospitalAdapter(List<ModuleItem> hospitales, Context mContext) {
        this.hospitales = hospitales;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return hospitales.size();
    }

    @Override
    public Object getItem(int position) {
        return hospitales.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
