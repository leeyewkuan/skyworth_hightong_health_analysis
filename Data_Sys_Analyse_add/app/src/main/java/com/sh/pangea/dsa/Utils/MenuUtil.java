package com.sh.pangea.dsa.Utils;


import com.sh.pangea.dsa.bean.MenuInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by yuangang on 2016/9/23.
 */

public class MenuUtil {
    public static List<MenuInfo> getMenuListInfo(String id) {
        List<MenuInfo> list = new ArrayList<>();
        switch (id) {
            case "T0": // 健康视频
                List<MenuInfo> health_lecture = new ArrayList<>();

//                MenuInfo video = new MenuInfo();
//                video.setId(0);
//                video.setName("健康视频走势图");
//                MenuInfo play = new MenuInfo();
//                play.setId(1);
//                play.setName("健康视频播放排行榜");
                health_lecture.add(creatMenuInfo("S0", "总览"));
                health_lecture.add(creatMenuInfo("S1", "频道统计"));

                list = health_lecture;
                break;
            case "T1": // 直播
                List<MenuInfo> cable_live = new ArrayList<>();

//                MenuInfo tv_channel_top = new MenuInfo();
//                tv_channel_top.setId(0);
//                tv_channel_top.setName("直播频道走势图");
//                MenuInfo tv_channel_trend = new MenuInfo();
//                tv_channel_trend.setId(1);
//                tv_channel_trend.setName("直播频道排行榜");

                cable_live.add(creatMenuInfo("S0", "总览"));
                cable_live.add(creatMenuInfo("S1", "各地区"));
                list = cable_live;
                break;
            case "T2": // 回看
                List<MenuInfo> cable_epg = new ArrayList<>();

//                MenuInfo epg_channel_top = new MenuInfo();
//                epg_channel_top.setId(0);
//                epg_channel_top.setName("回看频道走势图");
//                MenuInfo epg_channel_trend = new MenuInfo();
//                epg_channel_trend.setId(1);
//                epg_channel_trend.setName("回看频道排行榜");

                cable_epg.add(creatMenuInfo("S0", "总览"));
                cable_epg.add(creatMenuInfo("S1", "各地区"));
                list = cable_epg;
                break;
            case "T3":// 点播
                List<MenuInfo> cable_vod = new ArrayList<>();

//                MenuInfo vod_channel_top = new MenuInfo();
//                vod_channel_top.setId(0);
//                vod_channel_top.setName("点播节目走势图");

//                MenuInfo vod_channel_trend = new MenuInfo();
//                vod_channel_trend.setId(1);
//                vod_channel_trend.setName("点播节目排行榜");

                cable_vod.add(creatMenuInfo("S0", "总览"));
                cable_vod.add(creatMenuInfo("S1", "各地区"));
                list = cable_vod;
                break;
            case "T4": // 新兴业务
                List<MenuInfo> health_care_content = new ArrayList<>();

                health_care_content.add(creatMenuInfo("S0", "新兴业务"));
                list = health_care_content;
                break;
            case "T5": // 无线认证
                List<MenuInfo> hisportal = new ArrayList<>();

                hisportal.add(creatMenuInfo("S0", "总览"));
                hisportal.add(creatMenuInfo("S1", "按医院"));
                hisportal.add(creatMenuInfo("S2", "报表"));
                hisportal.add(creatMenuInfo("S3", "AP"));
//                hisportal.add(creatMenuInfo("S3", "AP管理"));
                list = hisportal;
                break;
            case "T6": // 医院业务
                List<MenuInfo> ap = new ArrayList<>();

                ap.add(creatMenuInfo("S0", "医院业务"));
                list = ap;
                break;
            case "T7": // 健康电视APP
                List<MenuInfo> health_tv_app = new ArrayList<>();

                health_tv_app.add(creatMenuInfo("S0", "健康电视APP"));
                list = health_tv_app;
                break;
        }
        return list;
    }

    private static MenuInfo creatMenuInfo(String id, String name) {
        MenuInfo menuInfo = new MenuInfo();
        menuInfo.setId(id);
        menuInfo.setName(name);
        return menuInfo;
    }

    private static MenuInfo creatMenuInfo(String id, String name, String dateSelect, String chartType) {
        MenuInfo menuInfo = new MenuInfo();
        menuInfo.setId(id);
        menuInfo.setName(name);
        menuInfo.setDateSelect(dateSelect);
        menuInfo.setChartType(chartType);
        return menuInfo;
    }

    public static Map<Integer, List<MenuInfo>> getMenuChildInfo(String id) {
        Map<Integer, List<MenuInfo>> map = new HashMap<>();
        switch (id) {
            case "T0":
                HashMap<Integer, List<MenuInfo>> health_lecture = new HashMap<>();

                List<MenuInfo> video = new ArrayList<>();
                video.add(creatMenuInfo("A1", "健康视频地区占比图", "", "P"));
                video.add(creatMenuInfo("B1", "健康视频频道占比图", "", "P"));
                video.add(creatMenuInfo("C1", "健康视频日走势图", "", "L"));
                video.add(creatMenuInfo("D1", "日健康视频排行榜", "222", "B"));
                video.add(creatMenuInfo("D2", "周健康视频排行榜", "999", "B"));

                List<MenuInfo> play = new ArrayList<>();
                play.add(creatMenuInfo("A1", "健康视频日走势图", "", "L"));
                play.add(creatMenuInfo("B1", "日健康视频排行榜", "222", "B"));
                play.add(creatMenuInfo("B2", "周健康视频排行榜", "999", "B"));

                health_lecture.put(0, video);
                health_lecture.put(1, play);
                map = health_lecture;

                break;
            case "T1":
                HashMap<Integer, List<MenuInfo>> cable_live = new HashMap<>();

                List<MenuInfo> tv_channel_top = new ArrayList<>();
                tv_channel_top.add(creatMenuInfo("A1", "直播频道占比图", "", "P"));
                tv_channel_top.add(creatMenuInfo("B1", "直播频道日走势图", "", "L"));
                tv_channel_top.add(creatMenuInfo("C1", "日直播频道排行榜", "222", "B"));
                tv_channel_top.add(creatMenuInfo("C2", "周直播频道排行榜", "999", "B"));

                List<MenuInfo> tv_channel_trend = new ArrayList<>();
                tv_channel_trend.add(creatMenuInfo("A1", "直播频道日走势图", "", "L"));
                tv_channel_trend.add(creatMenuInfo("B1", "日直播频道排行榜", "222", "B"));
                tv_channel_trend.add(creatMenuInfo("B2", "周直播频道排行榜", "999", "B"));

                cable_live.put(0, tv_channel_top);
                cable_live.put(1, tv_channel_trend);
                map = cable_live;

                break;
            case "T2":
                HashMap<Integer, List<MenuInfo>> cable_epg = new HashMap<>();

                List<MenuInfo> epg_channel_top = new ArrayList<>();
                epg_channel_top.add(creatMenuInfo("A1", "回看频道占比图", "", "P"));
                epg_channel_top.add(creatMenuInfo("B1", "回看频道日走势图", "", "L"));
                epg_channel_top.add(creatMenuInfo("C1", "日回看频道排行榜", "222", "B"));
                epg_channel_top.add(creatMenuInfo("C2", "周回看频道排行榜", "999", "B"));
                epg_channel_top.add(creatMenuInfo("D1", "日回看节目排行榜", "222", "B"));
                epg_channel_top.add(creatMenuInfo("D2", "周回看节目排行榜", "999", "B"));

                List<MenuInfo> epg_channel_trend = new ArrayList<>();
                epg_channel_trend.add(creatMenuInfo("A1", "回看频道日走势图", "", "L"));
                epg_channel_trend.add(creatMenuInfo("B1", "日回看频道排行榜", "222", "B"));
                epg_channel_trend.add(creatMenuInfo("B2", "周回看频道排行榜", "999", "B"));
                epg_channel_trend.add(creatMenuInfo("C1", "日回看节目排行榜", "222", "B"));
                epg_channel_trend.add(creatMenuInfo("C2", "周回看节目排行榜", "999", "B"));

                cable_epg.put(0, epg_channel_top);
                cable_epg.put(1, epg_channel_trend);
                map = cable_epg;

                break;
            case "T3":
                HashMap<Integer, List<MenuInfo>> cable_vod = new HashMap<>();
                List<MenuInfo> vod_channel_top = new ArrayList<>();
                vod_channel_top.add(creatMenuInfo("A1", "点播节目占比图", "", "P"));
                vod_channel_top.add(creatMenuInfo("B1", "点播节目日走势图", "", "L"));
                vod_channel_top.add(creatMenuInfo("C1", "日点播节目排行榜", "222", "B"));
                vod_channel_top.add(creatMenuInfo("C2", "周点播节目排行榜", "999", "B"));

                List<MenuInfo> vod_channel_trend = new ArrayList<>();
                vod_channel_trend.add(creatMenuInfo("A1", "点播节目日走势图", "", "L"));
                vod_channel_trend.add(creatMenuInfo("B1", "日点播节目排行榜", "222", "B"));
                vod_channel_trend.add(creatMenuInfo("B2", "周点播节目排行榜", "999", "B"));

                cable_vod.put(0, vod_channel_top);
                cable_vod.put(1, vod_channel_trend);
                map = cable_vod;

                break;
            case "T4":
                HashMap<Integer, List<MenuInfo>> health_care_content = new HashMap<>();

                List<MenuInfo> health_content = new ArrayList<>();
                health_content.add(creatMenuInfo("A1", "养生保健日排行榜", "222", "B"));
                health_content.add(creatMenuInfo("A2", "养生保健周排行榜", "999", "B"));

                health_care_content.put(0, health_content);
                map = health_care_content;

                break;
            case "T5":
                HashMap<Integer, List<MenuInfo>> hisportal = new HashMap<>();

                List<MenuInfo> hisportal_content0 = new ArrayList<>();
                hisportal_content0.add(creatMenuInfo("A1", "无线认证日走势图", "", "L"));
                hisportal_content0.add(creatMenuInfo("B1", "公众号用户", "", "L"));
                hisportal_content0.add(creatMenuInfo("C1", "微信用户分布情况", "222", "P"));
                hisportal_content0.add(creatMenuInfo("D1", "昨日新增用户占比图", "", "P"));

                List<MenuInfo> hisportal_content1 = new ArrayList<>();
                hisportal_content1.add(creatMenuInfo("A1", "无线认证按医院走势图", "", "L"));
                hisportal_content1.add(creatMenuInfo("B1", "无线认证按医院数据列表", "", ""));

                List<MenuInfo> hisportal_content2 = new ArrayList<>();
                hisportal_content2.add(creatMenuInfo("A1", "无线认证总览列表", "222", ""));
                hisportal_content2.add(creatMenuInfo("B1", "公众号用户总览列表", "222", ""));
                hisportal_content2.add(creatMenuInfo("C1", "无线认证按医院列表", "222", ""));

                List<MenuInfo> hisportal_content3 = new ArrayList<>();
                hisportal_content3.add(creatMenuInfo("A1", "AP日排行榜", "222", "B"));
                hisportal_content3.add(creatMenuInfo("A2", "AP周排行榜", "999", "B"));
                hisportal_content3.add(creatMenuInfo("B1", "AP日走势图", "", "L"));

                List<MenuInfo> hisportal_content4 = new ArrayList<>();
                hisportal_content4.add(creatMenuInfo("A1", "AP信息列表", "", ""));

                hisportal.put(0, hisportal_content0);
                hisportal.put(1, hisportal_content1);
                hisportal.put(2, hisportal_content2);
                hisportal.put(3, hisportal_content3);
//                hisportal.put(4, hisportal_content4);
                map = hisportal;

                break;
            case "T6":
                HashMap<Integer, List<MenuInfo>> ap = new HashMap<>();

                List<MenuInfo> ap_content1 = new ArrayList<>();
                ap_content1.add(creatMenuInfo("A1", "就诊导航日排行榜", "222", "B"));
                ap_content1.add(creatMenuInfo("A2", "就诊导航周排行榜", "999", "B"));
                ap_content1.add(creatMenuInfo("B1", "医院资讯日排行榜", "222", "B"));
                ap_content1.add(creatMenuInfo("B2", "医院资讯周排行榜", "999", "B"));
                ap_content1.add(creatMenuInfo("C1", "医生相关日排行榜", "222", "B"));
                ap_content1.add(creatMenuInfo("C2", "医生相关周排行榜", "999", "B"));

                ap.put(0, ap_content1);
                map = ap;

                break;
            case "T7":
                HashMap<Integer, List<MenuInfo>> health_tv_app = new HashMap<>();

                List<MenuInfo> health_tv_app_content1 = new ArrayList<>();
                health_tv_app_content1.add(creatMenuInfo("A1", "应用启动日走势图", "", "L"));
                health_tv_app_content1.add(creatMenuInfo("B1", "首页模块统计画面", "", "L"));

                health_tv_app.put(0, health_tv_app_content1);
                map = health_tv_app;

                break;
        }
        return map;
    }
}
