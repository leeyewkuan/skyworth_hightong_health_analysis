package com.sh.pangea.dsa.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.adapter.CurrentDataTypeAdapter;
import com.sh.pangea.dsa.observer.CurentTypeObservable;
import com.sh.pangea.dsa.view.HorizontalListView;
import com.sh.pangea.dsa.view.HospitalDetailsDataItem;

import java.util.ArrayList;
import java.util.List;

/**
 * 实时数据主页面fragment 用来包含所有类型的实时数据
 * Created by zhouyucheng on 2017/5/31.
 */

public class CurrentDataFragment extends Fragment {
    private HorizontalListView data_type;
    private LinearLayout current_datas;

    private List<String> names;
    private CurrentDataTypeAdapter adapter;

    private MonitoringFragment mMonitoringFragment;
    private HospitalStatisticsFragment mHospitalStatisticsFragment;
    private int currentType = 0;


    private Handler mHandler = new Handler() {
        public void dispatchMessage(Message msg) {
            if (0 == msg.what) {
                CurentTypeObservable.getInstance().updateType(currentType);
            }
        }

        ;
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_current_data, container, false);
        initView(inflate);
        initDatas();
        return inflate;
    }

    private void initDatas() {
        names = new ArrayList<>();
        names.add("公众号");
        names.add("医院统计");
        names.add("时速统计");
        names.add("科讯统计");
        adapter = new CurrentDataTypeAdapter(getActivity(), names);
        data_type.setAdapter(adapter);
        data_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setEnterPosition(position);
                currentType = position;
                if (0 == position) {
                    loadPublicUi();
                } else {
                    if (mHospitalStatisticsFragment == null) {
                        mHospitalStatisticsFragment = new HospitalStatisticsFragment();
                    }
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.current_datas, mHospitalStatisticsFragment).commit();
                    Message msg = new Message();
                    msg.what = 0;
                    mHandler.sendMessageDelayed(msg, 500);
                }
            }
        });
    }

    private void initView(View inflate) {
        data_type = (HorizontalListView) inflate.findViewById(R.id.data_type);
        current_datas = (LinearLayout) inflate.findViewById(R.id.current_datas);
        loadPublicUi();
    }

    private void loadPublicUi() {
        mMonitoringFragment = null;//强制值空
        if (mMonitoringFragment == null) {
            mMonitoringFragment = new MonitoringFragment();
        }
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.current_datas, mMonitoringFragment).commit();
    }

    public void refreshData() {
        if (currentType == 0) {
            mMonitoringFragment.refreshData();
        } else {
            mHospitalStatisticsFragment.refreshData();
        }

    }


    private int getCurrentType() {
        return currentType;
    }

}
