package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.util.Log;

import com.sh.lib.appbase.impl.NetBaseAppManager;
import com.sh.lib.base.callback.StringCallBackListener;
import com.sh.lib.commomportal.PortalManager;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.constant.InitConfig;
import com.skyworth_hightong.newgatherinformation.bean.CarouselPosterBean;
import com.skyworth_hightong.newgatherinformation.bean.EpgBrowseActionBean;
import com.skyworth_hightong.newgatherinformation.bean.HealthActionBean;
import com.skyworth_hightong.newgatherinformation.bean.HealthBrowseBean;
import com.skyworth_hightong.newgatherinformation.bean.LiveActionBean;
import com.skyworth_hightong.newgatherinformation.bean.LookBackActionEntity;
import com.skyworth_hightong.newgatherinformation.bean.OpsBean;
import com.skyworth_hightong.newgatherinformation.bean.SearchBean;
import com.skyworth_hightong.newgatherinformation.bean.TimeShiftingActionEntity;
import com.skyworth_hightong.newgatherinformation.bean.UpgradeBean;
import com.skyworth_hightong.newgatherinformation.bean.VODActionBean;
import com.skyworth_hightong.newgatherinformation.gather.face.impl.UserManager;
import com.skyworth_hightong.newgatherinformation.gather.utils.DateUtils;

import java.util.Date;

/**
 * 采集工具类
 */
public class GatherUtils {

    private Context context;
    private static GatherUtils mInstance;
    private UserManager userManager;

    private static final int conTimeout = 10000;

    public GatherUtils() {
        super();
    }

    private static final int soTimeout = 10000;
    private static boolean isFist = true;


    private GatherUtils(Context context) {
        this.context = context;
        userManager = UserManager.getInstance(context);
        getSystemTime();
    }

    private void getSystemTime() {
        newGetSystemTime(context);
    }

    /**
     * 健康电视的获取系统时间接口
     */
    private void newGetSystemTime(Context content) {
        NetBaseAppManager.getInstance(content).getSystemTime(InitConfig.CON_TIME_OUT, InitConfig.SO_TIME_OUT,
                new StringCallBackListener() {
                    @Override
                    public void onSuccess(String s) {

                    }

                    @Override
                    public void onPrepare(String s) {

                    }

                    @Override
                    public void onException(Exception e) {

                    }

                    @Override
                    public void onFail(int i) {

                    }
                }
        );
    }

    public synchronized static GatherUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new GatherUtils(context);
        }
        return mInstance;
    }

    /**
     * 传入服务器获取到的系统时间，如果获取失败传入当前设备时间
     *
     * @param nowTime 时间格式为YYYY-MM-DD HH:mm:SS
     */
    private void initSystmeTime(String nowTime) {
        Log.i("", "GatherUtils > initSystmeTime: nowTime" + DateUtils.getTimeStr(nowTime, DateUtils.YYYY_MM_DD0HH0MM0SS));
        userManager.initSystmeTime(nowTime);
    }


    /**
     * 用户登录时调用此方法
     */
    public String UserLogin(String onlyUserMark) {
        String userResult;
        if (isFist()) {
            userResult = initUser(onlyUserMark, 0);
            setIsFist(false);
        } else {
            userManager.userLogOut(0);
            userResult = changeUser(onlyUserMark);
        }
        Log.i("", "GatherUtils > isFist:" + isFist() + " onlyUserMark:" + onlyUserMark);
        return userResult;
    }

    /**
     * 用户第一次打开应用调用
     */
    private String initUser(String onlyUserMark, int devicType) {
        Log.i("", "GatherUtils > initUser: onlyUserMark:" + onlyUserMark);
        return userManager.initUser(onlyUserMark, devicType, PortalManager.getInstance(context).getPortalAddressFromCache().getOperatorCode());
    }

    public void insertSchame(String arg0, String arg1, String arg2, String arg3) {
        userManager.insertSchame(arg0, arg1, arg2, arg3);
    }

    public void gatherDeviceInfo(String gttype) {
        userManager.gatherDeviceInfo(gttype, PortalManager.getInstance(context).getPortalAddressFromCache().getOperatorCode());
    }

    /**
     * 切换用户都要调用此方法
     */
    private String changeUser(String onlyUserMark) {
        Log.i("", "GatherUtils > changeUser: onlyUserMark:" + onlyUserMark);
        return userManager.changeUser(onlyUserMark);
    }

    public void changeN(String n) {
        userManager.changeN(n);
    }

    /**
     * 用于异常信息采集
     */
    public void gatherOpsInfo(OpsBean opsBean) {
        Log.i("", "GatherUtils > 用于异常信息采集:" + opsBean.toString());
        userManager.gatherOpsInfo(opsBean);
    }

    /**
     * 直播采集结束
     */
    public void gatherLiveEnd(LiveActionBean liveActionBean) {
        Log.i("", "GatherUtils > 直播采集结束:" + liveActionBean.toString());
        userManager.gatherLiveEnd(liveActionBean);
    }

    /**
     * 直播采集开始(保存到内存中)
     */
    public void saveGatherLiveEndForCache(LiveActionBean liveActionBean) {
//        Logs.i("GatherUtils > 直播采集开始(保存到内存中):" + liveActionBean.toString());
//        userManager.saveGatherLiveEndForCache(liveActionBean);
    }


    /**
     * 回看采集结束
     */
    public void gatherLookBackEnd(LookBackActionEntity lookBackActionEntity) {
        Log.i("", "GatherUtils > 回看采集结束 :" + lookBackActionEntity.toString());
        userManager.gatherLookBackEnd(lookBackActionEntity);
    }

    /**
     * 回看采集开始(保存到内存中)
     */
    public void saveGatherLookBackEndForCache(
            LookBackActionEntity lookBackActionEntity) {
//        Logs.i("GatherUtils > 回看采集开始(保存到内存中) :" + lookBackActionEntity.toString());
//        userManager.saveGatherLookBackEndForCache(lookBackActionEntity);
    }


    /**
     * 点播采集结束
     */
    public void gatherVODEnd(VODActionBean vodActionBean) {
        Log.i("", "GatherUtils > 点播采集结束 :" + vodActionBean.toString());
        userManager.gatherVODEnd(vodActionBean);
    }

    /**
     * 点播采集开始(保存到内存中)
     */
    public void saveGatherVODEndForCache(VODActionBean vodActionBean) {
//        Logs.i("GatherUtils > 点播采集开始(保存到内存中) :" + vodActionBean.toString());
//        userManager.saveGatherVODEndForCache(vodActionBean);
    }


    /**
     * 点播搜索采集
     */
    public void gatherVodSearch(SearchBean searchBean) {
        Log.i("", "GatherUtils > 点播搜索:" + searchBean.toString());
        userManager.gatherVodSearch(searchBean);
    }

    /**
     * 时移采集结束
     */
    public void gatherTimeShiftingEnd(
            TimeShiftingActionEntity timeShiftingActionEntity) {
        Log.i("", "GatherUtils > 时移采集结束 :" + timeShiftingActionEntity.toString());
        userManager.gatherTimeShiftingEnd(timeShiftingActionEntity);
    }

    /**
     * 时移采集开始(保存到内存中)
     */
    public void saveGatherTimeShiftingEndForCache(
            TimeShiftingActionEntity timeShiftingActionEntity) {
//        Logs.i("GatherUtils > 时移采集开始(保存到内存中) :" + timeShiftingActionEntity.toString());
//        userManager.saveGatherTimeShiftingEndForCache(timeShiftingActionEntity);
    }

    /**
     * 医视频采集结束
     */
    public void gatherHealthEnd(
            HealthActionBean healthActionBean) {
        Log.i("", "GatherUtils > 医视频采集结束 :" + healthActionBean.toString());
        userManager.gatherHealthEnd(healthActionBean);
    }

    /**
     * 医视频采集开始(保存到内存中)
     */
    public void saveGatherHealthEndForCache(
            HealthActionBean healthActionBean) {
//        Logs.i("GatherUtils > 医视频采集开始(保存到内存中) :" + healthActionBean.toString());
//        userManager.saveGatherHealthEndForCache(healthActionBean);
    }


    /**
     * epg浏览采集
     */
    public void gatherEpgBrowse(EpgBrowseActionBean epgBrowseActionBean) {
        Log.i("", "GatherUtils > epg浏览采集:" + epgBrowseActionBean.toString());
        userManager.gatherEpgBrowse(epgBrowseActionBean);
    }

    /**
     * epg搜索采集
     */
    public void gatherEpgSearch(SearchBean searchBean) {
        Log.i("", "GatherUtils > 直播搜索:" + searchBean.toString());
        userManager.gatherEpgSearch(searchBean);
    }

    public void gatherHealthBrowses(String ID, String name) {
        String timeStr = DateUtil.getTimeStr(new Date(), DateUtil.YYYY_MM_DD0HH0MM0SS);
        Log.i("", "采集浏览信息 ID:" + ID + " name:" + name + "  timeStr:" + timeStr);
        //TODO 需要实现
        HealthBrowseBean healthBrowseBean = new HealthBrowseBean();
        healthBrowseBean.setID(ID);
        healthBrowseBean.setNAME(name);
        healthBrowseBean.setBT(timeStr);
        userManager.gatherHealthBrowses(healthBrowseBean);
    }

    public void gatherCarouselPosterBrowses(CarouselPosterBean carouselPosterBean) {
        String timeStr = DateUtil.getTimeStr(new Date(), DateUtil.YYYY_MM_DD0HH0MM0SS);
        carouselPosterBean.setBT(timeStr);
        Log.i("", "GatherUtils > 轮播图:" + carouselPosterBean.toString());
        userManager.gatherCarouselPosterBrowses(carouselPosterBean);
    }

    public void gatherAppUpgrade(String newVersion) {
        UpgradeBean upgradeBean = new UpgradeBean();
        try {
            String pkName = context.getPackageName();
            String versionName = context.getPackageManager().getPackageInfo(
                    pkName, 0).versionName;
            upgradeBean.setANAME(context.getResources().getString(R.string.app_name));
            upgradeBean.setPNAME(versionName);
            upgradeBean.setNOWVER(pkName);
        } catch (Exception e) {

        }

        upgradeBean.setNEWVER(newVersion);
        String timeStr = DateUtil.getTimeStr(new Date(), DateUtil.YYYY_MM_DD0HH0MM0SS);
        upgradeBean.setUT(timeStr);
        Log.i("", "GatherUtils > 软件升级:" + upgradeBean.toString());
        userManager.gatherAppUpgrade(upgradeBean);
    }


    public void applicationExit() {
        userManager.applicationExit();
    }

    private static boolean isFist() {
        return isFist;
    }

    private static void setIsFist(boolean isFist) {
        GatherUtils.isFist = isFist;
    }
}
