package com.sh.pangea.dsa.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sh.pangea.dsa.R;

import static java.lang.Integer.parseInt;

/**
 * Data_Sys_Analyse 日历空间里面的一个Item
 * Created by LB on 2016/11/8,10:23
 */

public class CalendarItemView extends FrameLayout implements View.OnClickListener {

    private Button add;
    private TextView edite;
    private Button subtract;

    private String defaultSting = "-";
    private int maxTime = -1;
    private int minTime = -1;

    private Context mContext;

    public CalendarItemView(Context context) {
        super(context);
        mContext = context;
        initView();
    }
    public CalendarItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public CalendarItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }



    /**
     * 获取显示的时间,
     */
    public String getShowTime() {
        return defaultSting;
    }
    public int getShowTimeInt(){
        String showTime = getShowTime();
        int i = -1;
        try {
            i = Integer.parseInt(showTime);
        }catch (Exception e){
            e.printStackTrace();
        }
        return i;
    }

    /**
     * 设置显示时间
     */
    public void setShowTime(String defaultSting) {
        this.defaultSting = defaultSting;
        edite.setText(defaultSting);
    }

    private void initView() {
        final View inflate = View.inflate(mContext, R.layout.calendar_item_view, this);
        add = (Button) inflate.findViewById(R.id.bt_add);
        subtract = (Button) inflate.findViewById(R.id.bt_subtract);
        edite = (TextView) inflate.findViewById(R.id.tv_edite);

        add.setOnClickListener(this);
        subtract.setOnClickListener(this);
    }

    public void setShowTimeColor(int color){
        edite.setTextColor(color);
    }
    public void setShowTimeSize(float size){
        edite.setTextSize(TypedValue.COMPLEX_UNIT_PX,size);
    }



    @Override
    public void onClick(View v) {
        if (!defaultSting.equals("-")) {
            int time = 0;
            try {
                time = parseInt(defaultSting);
            } catch (Exception e) {
                e.printStackTrace();
            }

            switch (v.getId()) {
                case R.id.bt_add:
                    if ((maxTime != -1 && time+1 <= maxTime) || maxTime == -1) {
                        defaultSting = time + 1 + "";
                    }
                    break;
                case R.id.bt_subtract:
                    if ((minTime != -1 && time-1 >= minTime) || maxTime == -1)
                        defaultSting = time - 1 + "";
                    break;
            }
            edite.setText(defaultSting);
            if (onDateChange != null) {
                onDateChange.changTime(v,this, defaultSting);
            }
        }
    }

    private OnDateChange onDateChange;

    /**
     * 设置时间改变监听
     */
    public void setOnDateChange(OnDateChange onDateChange) {
        this.onDateChange = onDateChange;
    }

    /** 设置空间不可以点击*/
    public void setNoOnTouch() {
        add.setClickable(false);
        add.setTextColor(getResources().getColor(R.color.colorTextSub));
        subtract.setClickable(false);
        subtract.setTextColor(getResources().getColor(R.color.colorTextSub));
        edite.setTextColor(getResources().getColor(R.color.colorTextSub));
    }

    interface OnDateChange {
        /**
         * @param time 改变后的时间
         */
        void changTime(View view,View parentView, String time);
    }

}
