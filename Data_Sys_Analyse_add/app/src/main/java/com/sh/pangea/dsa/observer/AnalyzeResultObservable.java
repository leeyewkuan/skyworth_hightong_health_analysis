package com.sh.pangea.dsa.observer;


import com.sh.pangea.dsa.bean.AnalyzeResult;

public class AnalyzeResultObservable extends Observable<AnalyzeResultObserver> {

    private static AnalyzeResultObservable mInstance;

    public static AnalyzeResultObservable getInstance() {
        if (mInstance == null) {
            mInstance = new AnalyzeResultObservable();
        }
        return mInstance;
    }

    public AnalyzeResultObservable() {
        super();
    }

    /**
     * 用于通知ui层接口调用的结果
     **/
    public void onAnalyzeResult(int code, String error, AnalyzeResult analyzeResult) {
        for (AnalyzeResultObserver cc : mObservers) {
            cc.onAnalyzeResult(code, error, analyzeResult);
        }
    }


}
