package com.sh.pangea.dsa.adapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Data_Sys_Analyse
 * Created by LB on 2016/10/13,10:04
 */

public class ScreenWeekAdapter extends BaseAdapter {

    private  List<ModuleItem> list;
    private int selectPost;
    @Override
    public int getCount() {
        int number = 0;
        if(list != null){
            number = list.size();
            Log.i("number",""+number);
        }
        return number;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(parent.getContext(), R.layout.screen_week_adapter,null);
            viewHolder = new ViewHolder();
            viewHolder.tv = (TextView) convertView.findViewById(R.id.tv_channel_item);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ModuleItem moduleItem = list.get(position);
        viewHolder.tv.setText(moduleItem.getName());
        if(position == selectPost){
//            convertView.setBackgroundResource(R.mipmap.bg_xuanji_focus);
            viewHolder.tv.setTextColor(parent.getContext().getResources().getColor(R.color.colorPrimary));
        }else{
//            convertView.setBackgroundResource(R.mipmap.bg_xuanji);
            viewHolder.tv.setTextColor(parent.getContext().getResources().getColor(R.color.black));
        }
        return convertView;
    }

    private static class ViewHolder{
        TextView tv;
    }

    public void update(List<ModuleItem> list){
        this.list = list;
        notifyDataSetChanged();
    }
    public void setSelect(int post){
        this.selectPost = post;
        notifyDataSetChanged();
    }
}
