package com.sh.pangea.dsa.view.editText;

/**
 * Created by mariotaku on 15/4/12.
 */
public abstract class METLengthChecker {

    public abstract int getLength(CharSequence text);

}
