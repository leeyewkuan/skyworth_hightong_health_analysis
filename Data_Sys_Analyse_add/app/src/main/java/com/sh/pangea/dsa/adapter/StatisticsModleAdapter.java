package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.lib.dsa.bean.filter.FilterCondition;
import com.sh.pangea.dsa.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by liuqiwu on 2016/11/23.
 */

public class StatisticsModleAdapter extends BaseAdapter {
    private Context mContext;
    private HashMap<Integer, View> viewMap = new HashMap<>();
    private List<FilterCondition> list;

    public StatisticsModleAdapter(Context mContext, List<FilterCondition> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (!viewMap.containsKey(position) || viewMap.get(position) == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_statistics_modle, null);
            holder.name = (TextView) convertView.findViewById(R.id.item_statistics_name);
            convertView.setTag(holder);
            viewMap.put(position, convertView);
        } else {
            convertView = viewMap.get(position);
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(list.get(position).getName());
//        Log.i("Main", "getView()   mType:" + mType + "      position:" + position);
        if (position == 0 && mType != null) {
            mLoadedFirstViewListener.loadedFirstView(mType, (TextView) convertView.findViewById(R.id.item_statistics_name));
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView name;
    }

    private LoadedFirstViewListener mLoadedFirstViewListener;
    private String mType;

    public void setLoadedFirstViewListener(String type, LoadedFirstViewListener listener) {
        mLoadedFirstViewListener = listener;
        mType = type;
    }

    public interface LoadedFirstViewListener {
        void loadedFirstView(String key, TextView textView);
    }
}
