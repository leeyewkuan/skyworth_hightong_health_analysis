package com.sh.pangea.dsa.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.sh.lib.dsa.bean.analyzeresult.LinearItem;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.AnalyzeIntentUtil;
import com.sh.pangea.dsa.Utils.AnalyzeStringUtil;
import com.sh.pangea.dsa.Utils.CustomAnalyzeBoard;
import com.sh.pangea.dsa.Utils.FiltersUtil;
import com.sh.pangea.dsa.Utils.PieOtherAnalyzeBoard;
import com.sh.pangea.dsa.Utils.SildingFinishLayout;
import com.sh.pangea.dsa.Utils.XYMarkerView;
import com.sh.pangea.dsa.adapter.LineChartAdapter;
import com.sh.pangea.dsa.adapter.PieChartAdapter;
import com.sh.pangea.dsa.bean.AnalyzeResult;
import com.sh.pangea.dsa.bean.Level;
import com.sh.pangea.dsa.bean.MenuInfo;
import com.sh.pangea.dsa.observer.AnalyzePopWindowObservable;
import com.sh.pangea.dsa.observer.AnalyzePopWindowObserver;
import com.sh.pangea.dsa.observer.AnalyzeResultObservable;
import com.sh.pangea.dsa.observer.AnalyzeResultObserver;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by TianGenhu on 2016/10/9.
 * 展示折线图、柱状图、饼状图图表
 */

public class LineChartActivity extends Activity implements AnalyzeResultObserver, AnalyzePopWindowObserver {
    private BarChart barChart;
    private final String TAG = "TGH";
    private LineChart tvChannelLineChart;
    private PieChart pieChart;
    @BindView(R.id.analyze_linearlayout)
    LinearLayout analyze_linearlayout;
    @BindView(R.id.sildingFinishLayout)
    SildingFinishLayout sildingFinishLayout;
    @BindView(R.id.linar_chart_layout)
    LinearLayout linarChartLayout;
    @BindView(R.id.bar_chart_layout)
    LinearLayout barChartLayout;
    //    @BindView(R.id.data_listview)
//    ListView dataListview;
    @BindView(R.id.data_expandablelistview)
    ExpandableListView dataExpandablelistview;
    @BindView(R.id.line_chart_title)
    TextView lineChartTitle;
    @BindView(R.id.big_picture)
    TextView big_picture;
    @BindView(R.id.no_data)
    TextView no_data;
    //    @BindView(R.id.no_chart_data)
//    TextView no_chart_data;
    @BindView(R.id.data_title)
    LinearLayout data_title;
    @BindView(R.id.current_data_name)
    TextView current_data_name;
    @BindView(R.id.current_data_name_linearlayout)
    LinearLayout current_data_name_linearlayout;
    @BindView(R.id.current_data_name_title)
    TextView current_data_name_title;
    @BindView(R.id.pie_chart_layout)
    LinearLayout pieChartLayout;
    @BindView(R.id.filter_ui)
    RelativeLayout filter_ui;
    @BindView(R.id.line_chart_search_day)
    TextView line_chart_search_day;
    @BindView(R.id.line_chart_search_other)
    TextView line_chart_search_other;

    private List<Level> mLevel = null;
    private ArrayList<BarEntry> barEntriyList = new ArrayList<>();
    private boolean has_K = false;
    private List<LinearItem> mMonthsValues = new ArrayList<>();
    private List<String> xForGraph = new ArrayList<>();
    private AnalyzeResult mAnalyzeResult = null;
    private Entry mEntry;
    private CustomAnalyzeBoard mCustomAnalyzeBoard = null;
    private AnalyzeResult mPopAnalyzeResult;
    private final ViewGroup.LayoutParams mLineChartLayout = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    private Context mContext;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (mAnalyzeResult != null) {
                        String nameGraph = mAnalyzeResult.getTitleGraph();
                        if (nameGraph == null) {
                            nameGraph = "";
                        }
                        getXString();
                        showIconType(mAnalyzeResult.getyForGraph(), nameGraph);
                        String titleGraph = mAnalyzeResult.getTitleGraph();
                        if (titleGraph != null && !TextUtils.isEmpty(titleGraph)) {
                            lineChartTitle.setVisibility(View.VISIBLE);
                            lineChartTitle.setText(titleGraph);
                        } else {
                            lineChartTitle.setVisibility(View.GONE);
                            lineChartTitle.setText("");
                        }
                        //适配图标下的数据
                        if (xForGraph != null && xForGraph.size() > 0) {
                            //Collections.reverse(xForGraph);
                            LineChartAdapter mTvChannelAdapter = new LineChartAdapter(LineChartActivity.this, setInvertedOrderYForGraph(mMonthsValues), setInvertedOrderXForGraph(xForGraph));
                            dataExpandablelistview.setAdapter(mTvChannelAdapter);
                            dataExpandablelistview.setGroupIndicator(getResources().getDrawable(R.drawable.expandablelistview));
                            if (dataExpandablelistview != null && mTvChannelAdapter.getGroupCount() > 0) {
                                for (int i = 0; i < mTvChannelAdapter.getGroupCount(); i++) {
                                    dataExpandablelistview.expandGroup(i);
                                }
                            }
                            if (xForGraph.size() > 0) {
                                big_picture.setVisibility(View.VISIBLE);
                                no_data.setVisibility(View.GONE);
                                data_title.setVisibility(View.VISIBLE);
                                dataExpandablelistview.setVisibility(View.VISIBLE);
                            } else {
                                big_picture.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                                data_title.setVisibility(View.GONE);
//                                no_chart_data.setVisibility(View.GONE);
                                dataExpandablelistview.setVisibility(View.INVISIBLE);
                                setNoChartData();
                            }
                        } else {
                            Log.i(TAG, "handleMessage  onSuccess  xForGraph is null ");
                            switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
                                case AnalyzeIntentUtil.PIE_CHART://初始化扇形图
                                    if (mMonthsValues != null && mMonthsValues.size() > 0) {
                                        PieChartAdapter mPieChartAdapter = new PieChartAdapter(LineChartActivity.this, mMonthsValues);
                                        dataExpandablelistview.setAdapter(mPieChartAdapter);
                                        dataExpandablelistview.setGroupIndicator(getResources().getDrawable(R.drawable.expandablelistview));
                                        if (dataExpandablelistview != null && mPieChartAdapter.getGroupCount() > 0) {
                                            for (int i = 0; i < mPieChartAdapter.getGroupCount(); i++) {
                                                dataExpandablelistview.expandGroup(i);
                                            }
                                        }
                                        big_picture.setVisibility(View.VISIBLE);
                                        no_data.setVisibility(View.GONE);
                                        data_title.setVisibility(View.VISIBLE);
//                                        no_chart_data.setVisibility(View.GONE);
                                        dataExpandablelistview.setVisibility(View.VISIBLE);
                                    } else {
                                        big_picture.setVisibility(View.GONE);
                                        no_data.setVisibility(View.VISIBLE);
                                        data_title.setVisibility(View.GONE);
//                                        no_chart_data.setVisibility(View.VISIBLE);
                                        dataExpandablelistview.setVisibility(View.INVISIBLE);
                                        setNoChartData();
                                    }
                                    break;
                                default:
                                    big_picture.setVisibility(View.GONE);
                                    no_data.setVisibility(View.VISIBLE);
                                    data_title.setVisibility(View.GONE);
//                                    no_chart_data.setVisibility(View.GONE);
                                    dataExpandablelistview.setVisibility(View.INVISIBLE);
                                    setNoChartData();
                                    Log.i(TAG, "handleMessage: onSuccess  折线图或者柱状图X轴为空");
                                    break;
                            }
                        }
                    } else {
                        Log.i(TAG, "handleMessage: mAnalyzeResult is null ");
                        big_picture.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        data_title.setVisibility(View.GONE);
                        dataExpandablelistview.setVisibility(View.INVISIBLE);
                        lineChartTitle.setVisibility(View.GONE);
//                        no_chart_data.setVisibility(View.VISIBLE);
                        if (pieChart != null) {
                            Legend l = pieChart.getLegend();
                            l.setEnabled(false);
                        }
                        setNoChartData();
                    }
                    break;
                case 3:
                    switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
                        case AnalyzeIntentUtil.LINAR_CHART://初始化折线组件
                            linarChartLayout.removeAllViews();
                            tvChannelLineChart = new LineChart(LineChartActivity.this);
                            tvChannelLineChart.setLayoutParams(mLineChartLayout);
                            setLineChart(tvChannelLineChart);
                            linarChartLayout.addView(tvChannelLineChart);
                            break;
                        case AnalyzeIntentUtil.BAR_CHART://初始化柱状图组件
                            barChartLayout.removeAllViews();
                            barChart = new BarChart(LineChartActivity.this);
                            barChart.setLayoutParams(mLineChartLayout);
                            setBarChart(barChart);
                            barChartLayout.addView(barChart);
                            isShowPopWind();
                            break;
                        case AnalyzeIntentUtil.PIE_CHART://初始化扇形图
                            pieChartLayout.removeAllViews();
                            pieChart = new PieChart(LineChartActivity.this);
                            pieChart.setLayoutParams(mLineChartLayout);
                            setPieChart(pieChart);
                            pieChartLayout.addView(pieChart);
                            break;
                        default:
                            Log.i(TAG, "handleMessage: 初始化组件没有匹配项");
                            break;
                    }
                    break;
                case 4:
                    switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
                        case AnalyzeIntentUtil.LINAR_CHART://重置折线图
                            isResetClickLineChart();
                            break;
                        case AnalyzeIntentUtil.BAR_CHART://重置柱状图
                            if (barChart != null) {
                                barChart.animateY(3000);
                                barChart.fitScreen();
                            }
                            break;
                        case AnalyzeIntentUtil.PIE_CHART://重置扇形图
                            if (pieChart != null) {
                                pieChart.animateY(3000);
                            }
                            break;
                        default:
                            Log.i(TAG, "handleMessage:重置化组件没有匹配项");
                            break;
                    }
                    break;
                case 5:
                    if (mCustomAnalyzeBoard != null && mCustomAnalyzeBoard.isShowing()) {
                        mCustomAnalyzeBoard.dismiss();
                    }
                    if (!LineChartActivity.this.isFinishing()) {
                        //初始化popWindow
                        mCustomAnalyzeBoard = new CustomAnalyzeBoard(LineChartActivity.this, mPopAnalyzeResult);
                        mCustomAnalyzeBoard.showAtLocation(LineChartActivity.this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
                        mCustomAnalyzeBoard.setOutsideTouchable(true);  //设置点击屏幕其它地方弹出框消失
                    }
                    break;
                case 6:
                    // TODO queryTrendMedicalDaysGraph直接调用，2.2.7 需要修改
                    if (mEntry != null) {
                        float x = mEntry.getX();
                        String formattedValue = xForGraph.get((int) x);
                        String format = mFormat.format(mEntry.getY());
                        String weekly = mAnalyzeResult.getWeekly();
                        if (weekly != null && !TextUtils.isEmpty(weekly)) {
                            //wk = parseInt(weekly);
                        } else {
                            weekly = AnalyzeIntentUtil.getInstance(LineChartActivity.this).getTimeWeek() + "";
                        }
                        Log.i("TGH", "onValueSelected: " + formattedValue + "    " + format + "    " + weekly);
                        AnalyzeIntentUtil.getInstance(LineChartActivity.this).setMedicalName(formattedValue);
                        AnalyzeIntentUtil.getInstance(LineChartActivity.this).smallLineChart(parentInfo, groupInfo, /*-1*/childInfo);
                    } else {
                        Log.i(TAG, "handleMessage: mEntry is null ");
                    }
                    break;
                case 7:
                    Intent intent = new Intent(LineChartActivity.this, LineChartActivity2.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("result", mAnalyzeResult);
                    bundle.putString("name", mAnalyzeResult.getTitleGraph());
                    intent.putExtras(bundle);
                    LineChartActivity.this.startActivity(intent);
                    break;
                case 8:
                    ShowScreenView(parentInfo, groupInfo, childInfo);
                    break;
                default:
                    break;
            }
        }
    };

    private long curr = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyzeResultObservable.getInstance().registerObserver(this);
        AnalyzePopWindowObservable.getInstance().registerObserver(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //一直显示手机状态栏
        this.findViewById(Window.ID_ANDROID_CONTENT);
        setContentView(R.layout.activity_tv_channel);
        ButterKnife.bind(this);
        mContext = this;
        getIntetn();
        mHandler.sendEmptyMessage(8);
        initView();
        curr = System.currentTimeMillis();
//
//        AnalyzeResult analyzeResult = TransitionAnalyzeResultUtil.getInstance(mContext).resultLinearTransitionAnalyzeResult_jiaShuJu();
//        AnalyzeResultObservable.getInstance().onAnalyzeResult(0, "", analyzeResult);

    }

    private MenuInfo parentInfo;
    private MenuInfo groupInfo;
    private MenuInfo childInfo;

    private void getIntetn() {
        Intent intent = this.getIntent();
        parentInfo = (MenuInfo) intent.getSerializableExtra("parentInfo");
        groupInfo = (MenuInfo) intent.getSerializableExtra("groupInfo");
        childInfo = (MenuInfo) intent.getSerializableExtra("childInfo");
        Log.i(TAG, "getIntetn  : " + parentInfo + "   " + groupInfo + "  " + childInfo);
    }

    /**
     * 点击柱状图是否弹起popwind窗口
     **/
    private boolean isShowPopwind = false;

    private void isShowPopWind() {
        if (childInfo.getChartType().equals("B") && childInfo.getDateSelect().equals("999")) {
            isShowPopwind = true;
        }
    }

    /**
     * 转换X轴倒序
     */
    private List<String> setInvertedOrderXForGraph(List<String> xForGraph) {
        String iconType = AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType();
        Log.i(TAG, "setInvertedOrderXForGraph    iconType: " + iconType);
        if (iconType.equals(AnalyzeIntentUtil.LINAR_CHART)) {
            List<String> mXForGraph = new ArrayList<>();
            for (int i = xForGraph.size() - 1; i >= 0; i--) {
                mXForGraph.add(xForGraph.get(i));
            }
            return mXForGraph;
        }
        return xForGraph;
    }

    /**
     * 转换Y轴倒序
     */
    private List<LinearItem> setInvertedOrderYForGraph(List<LinearItem> monthsValues) {
        String iconType = AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType();
        Log.i(TAG, "setInvertedOrderYForGraph    iconType: " + iconType);
        if (iconType.equals(AnalyzeIntentUtil.LINAR_CHART)) {
            Log.i(TAG, "setInvertedOrderYForGraph  hou :   " + AnalyzeIntentUtil.LINAR_CHART);
            List<Integer> datas = null;
            for (int i = 0; i < monthsValues.size(); i++) {
                LinearItem linearItem = monthsValues.get(i);
                List<Integer> data = linearItem.getData();
                datas = new ArrayList<>();
                for (int s = data.size() - 1; s >= 0; s--) {
                    datas.add(data.get(s));
                }
//                Collections.reverse(data);
                linearItem.setData(data);
            }
            return monthsValues;
        }
        return monthsValues;
    }

    private void setLineChart(LineChart tvChannelLineChart) {
        //此方法设置为false时图像拉大后，用手指不能左右上下拖动
        tvChannelLineChart.setDragEnabled(true);
        //去掉图标的标志
        tvChannelLineChart.getDescription().setEnabled(false);
        //禁止水平方向拖拽
        tvChannelLineChart.setScaleXEnabled(true);
        //禁止垂直方向拖拽
        tvChannelLineChart.setScaleYEnabled(false);
        //画图标的边框
        tvChannelLineChart.setDrawBorders(true);
        //设置图标边框的线的宽度
        //tvChannelLineChart.setBorderWidth(2f);
        //设置数据为空时显示的描述文字
        tvChannelLineChart.setNoDataText("暂时没有数据！");
        //设置数据为空时显示的描述文字的颜色
        tvChannelLineChart.setNoDataTextColor(getResources().getColor(R.color.colorPrimaryDark));
        //设置图表的背景颜色 不知道为什么不起作用
        //tvChannelLineChart.setGridBackgroundColor(getResources().getColor(R.color.colorSub));
        //
        //tvChannelLineChart.getYChartMin(false);

        // 设置Legend启用或禁用
        Legend legend = tvChannelLineChart.getLegend();
        legend.setEnabled(true);
        legend.setTextSize(12);
        legend.setTextColor(getResources().getColor(R.color.colorAccent));


        XAxis xAxis = tvChannelLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(mIAxisValueFormatter_X);
        //设置x轴字体显示角度,即X轴上文字呈现斜着显示
        xAxis.setLabelRotationAngle(0);
        //设置X轴的位置TOP, BOTTOM, BOTH_SIDED, TOP_INSIDE, BOTTOM_INSIDE
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = tvChannelLineChart.getAxisLeft();
        leftAxis.setInverted(false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        leftAxis.setGranularityEnabled(true);
//        leftAxis.setGridColor(10);
//        leftAxis.setDrawLimitLinesBehindData(true);
//        leftAxis.setDrawZeroLine(false);
        //leftAxis.setAxisMaximum(300f);
        leftAxis.setValueFormatter(mIAxisValueFormatter_Y);
        YAxis rightAxis = tvChannelLineChart.getAxisRight();
        rightAxis.setEnabled(true);
        rightAxis.setInverted(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setValueFormatter(mIAxisValueFormatter_Y_right);

        // get the legend (only possible after setting data)
        Legend l = tvChannelLineChart.getLegend();
        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        // dont forget to refresh the drawing
        tvChannelLineChart.invalidate();
        tvChannelLineChart.animateY(3000);
//        //点击显示数据组件
//        XYMarkerView mv = new XYMarkerView(this, mIAxisValueFormatter_X);
//        mv.setChartView(tvChannelLineChart); // For bounds control
//        tvChannelLineChart.setMarker(mv); // Set the marker to the chart

    }


    private void setBarChart(BarChart barChart) {
        //Set this to true to enable dragging (moving the chart with the finger) for the chart (this does not effect scaling).
        //此方法设置为false时图像拉大后，用手指不能左右上下拖动
        barChart.setDragEnabled(true);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(false);
        barChart.getDescription().setEnabled(false);
        //禁止水平方向拖拽
        barChart.setScaleXEnabled(true);
        //禁止垂直方向拖拽
        barChart.setScaleYEnabled(false);
        //取消双击放大功能
        barChart.setDoubleTapToZoomEnabled(false);
        //画图标的边框
        barChart.setDrawBorders(true);
        //设置图标边框的线的宽度
        //barChart.setBorderWidth(2f);
        barChart.animateY(3000);
        // if more than 60 entries are displayed in the chart, no values will be drawn
        barChart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
//        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);
        barChart.setFitBars(true);

        //设置数据为空时显示的描述文字
        barChart.setNoDataText("暂时没有数据！");
        //设置数据为空时显示的描述文字的颜色
        barChart.setNoDataTextColor(getResources().getColor(R.color.colorPrimaryDark));
        //设置图表的背景颜色  不知道为什么不起作用
        //barChart.setGridBackgroundColor(getResources().getColor(R.color.colorSub));

        // 设置Legend启用或禁用
        Legend legend = barChart.getLegend();
        legend.setEnabled(false);
        legend.setTextColor(getResources().getColor(R.color.colorAccent));
        legend.setWordWrapEnabled(true);

        XAxis xAxisc = barChart.getXAxis();
        xAxisc.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxisc.setDrawGridLines(false);
        // only intervals of 1 day  即：每柱只显示一个值
        xAxisc.setGranularity(1f);
        xAxisc.setLabelCount(7);
        xAxisc.setValueFormatter(mIAxisValueFormatter_X);
//        xAxisc.setAxisMinimum(0f);
        xAxisc.setDrawAxisLine(true);
        //设置x轴字体显示角度,即X轴上文字呈现斜着显示 -90
        xAxisc.setLabelRotationAngle(0);
        //设置X轴的位置TOP, BOTTOM, BOTH_SIDED, TOP_INSIDE, BOTTOM_INSIDE
        xAxisc.setPosition(XAxis.XAxisPosition.BOTTOM);
        //设置x轴的颜色
        xAxisc.setAxisLineColor(getResources().getColor(R.color.colorAccent));
//
        YAxis leftAxisc = barChart.getAxisLeft();
        leftAxisc.setLabelCount(8, false);
        leftAxisc.setValueFormatter(mIAxisValueFormatter_Y);
//        leftAxisc.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxisc.setSpaceTop(15f);
        leftAxisc.setAxisMinimum(0f);

        YAxis rightAxisc = barChart.getAxisRight();
        //设置右边Y轴是否显示数据
        rightAxisc.setEnabled(false);
        rightAxisc.setSpaceTop(15f);
        rightAxisc.setValueFormatter(mIAxisValueFormatter_Y);
        rightAxisc.setAxisMinimum(0f);

        Legend lc = barChart.getLegend();
        lc.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        lc.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        lc.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        lc.setDrawInside(false);
        lc.setForm(Legend.LegendForm.SQUARE);
        lc.setFormSize(9f);
        lc.setTextSize(6f);
        lc.setXEntrySpace(4f);

        //点击没柱显示数据
        XYMarkerView mv = new XYMarkerView(this, mIAxisValueFormatter_X);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart
        barChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);
    }

    private void setPieChart(PieChart pieChart) {
        //用自带的百分比算法
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        //设置整个饼状图是空心还是实心
        pieChart.setDrawHoleEnabled(false);
        pieChart.setExtraOffsets(20f, 5.f, 20f, 5.f);
        //图标log显示在中心
        pieChart.setDrawCenterText(false);
        /**
         *触摸是否可以旋转以及松手后旋转的度数
         */
        pieChart.setRotationAngle(300);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);
        //饼形图上是否显示每个模块的X轴的值
        pieChart.setDrawEntryLabels(false);
        pieChart.animateX(1000);
        //设置数据为空时显示的描述文字
        pieChart.setNoDataText("暂时没有数据！");
        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setEnabled(true);

//        XYMarkerViewPiePoylineChart mv = new XYMarkerViewPiePoylineChart(this, mIAxisValueFormatter_X);
//        mv.setChartView(pieChart); // For bounds control
//        pieChart.setMarker(mv); // Set the marker to the chart

    }

    private void initView() {
        sildingFinishLayout.setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {
            @Override
            public void onSildingFinish() {
                LineChartActivity.this.finish();
            }
        });
        // touchView要设置到ListView上面
        sildingFinishLayout.setTouchView(linarChartLayout);
        sildingFinishLayout.setTouchView(barChartLayout);
        sildingFinishLayout.setTouchView(pieChartLayout);
        sildingFinishLayout.setTouchView(analyze_linearlayout);

        //分组展开
        dataExpandablelistview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
                setLineVisible(groupPosition, true);
            }
        });
        //分组关闭
        dataExpandablelistview.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            public void onGroupCollapse(int groupPosition) {
                setLineVisible(groupPosition, false);
                if (isGroupExpanded()) {
                    isResetClickLineChart();
                } else {
                    Log.i(TAG, "onGroupCollapse: 二级列表没有都关闭");
                }
            }
        });
        //子项单击
        dataExpandablelistview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView arg0, View arg1,
                                        int groupPosition, int childPosition, long arg4) {
                switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
                    case AnalyzeIntentUtil.LINAR_CHART://折线图定位
                        if (mapEntry != null && mapEntry.size() > 0) {
                            ArrayList<Entry> entries = mapEntry.get(groupPosition);
                            if (entries != null && !entries.isEmpty()) {
                                Entry barEntry = entries.get(entries.size() - childPosition - 1);
                                if (barEntry != null) {
                                    Log.i(TAG, "onChildClick    barEntry.getX()  : " + barEntry.getX() + "     " + groupPosition);
                                    tvChannelLineChart.highlightValue(barEntry.getX(), groupPosition, true);
                                } else {
                                    Log.i(TAG, "onItemClick: Entry is null");
                                }
                            }
                        } else {
                            Log.i(TAG, "onItemClick: entryList is null");
                        }
                        break;
                    case AnalyzeIntentUtil.BAR_CHART://柱状图定位
                        if (barEntriyList != null && barEntriyList.size() > 0) {
                            BarEntry barEntry = barEntriyList.get(childPosition);
                            if (barEntry != null) {
                                setCurrentDataName(childPosition);
                                barChart.highlightValue(barEntry.getX(), 0, true);
                            } else {
                                Log.i(TAG, "onItemClick: barEntry is null");
                            }
                        } else {
                            Log.i(TAG, "onItemClick: barEntriyList is null");
                        }
                        break;
                    case AnalyzeIntentUtil.PIE_CHART://扇形图定位
                        if (entries_pie != null && entries_pie.size() > 0) {
                            pieChart.highlightValue(childPosition, 0, true);
                        }
                        showPieOther(childPosition);
                        break;
                    default:
                        Log.i(TAG, "onItemClick: 没有可匹配定位的图表");
                        break;
                }
                return false;
            }
        });
    }


    private PieOtherAnalyzeBoard mPieOtherAnalyzeBoard;

    private void showPieOther(int childPosition) {
        if (entries_pie != null && entries_pie.size() > 0) {
            if (entries_pie.size() - 1 == childPosition) {
                if (mPieOtherAnalyzeBoard != null && mPieOtherAnalyzeBoard.isShowing()) {
                    mPieOtherAnalyzeBoard.dismiss();
                }
                //初始化popWindow
                mPieOtherAnalyzeBoard = new PieOtherAnalyzeBoard(LineChartActivity.this, popWindPieList);
                mPieOtherAnalyzeBoard.showAtLocation(LineChartActivity.this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
                mPieOtherAnalyzeBoard.setOutsideTouchable(true);  //设置点击屏幕其它地方弹出框消失
            }
        }
    }

    /**
     * 判断二级列表是否全部展开
     **/
    private boolean isGroupExpanded() {
        boolean isOpen = false;
        if (dataExpandablelistview != null) {
            if (mMonthsValues != null && mMonthsValues.size() > 0) {
                for (int i = 0; i < mMonthsValues.size(); i++) {
                    if (!dataExpandablelistview.isGroupExpanded(i)) {
                        Log.i(TAG, "isGroupExpanded   : " + i);
                        isOpen = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return isOpen;
    }

    /**
     * 展示日排行榜的item的名字
     **/
    private void setCurrentDataName(int position) {
        switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
            case AnalyzeIntentUtil.BAR_CHART://柱状图
                // TODO: 2016/12/28 需要调整 
//                if (mGroupPosition == 1 && mChildPosition == 0) {
                if (xForGraph != null && xForGraph.size() > position) {
                    current_data_name_title.setText("选中: ");
                    current_data_name.setText(xForGraph.get(position));
                } else {
                    current_data_name_title.setText("");
                    current_data_name.setText("");
                }
//                } else {
//                    current_data_name_linearlayout.setVisibility(View.GONE);
//                }
                break;
            case AnalyzeIntentUtil.LINAR_CHART://折线图
            case AnalyzeIntentUtil.PIE_CHART://扇形图
                current_data_name_linearlayout.setVisibility(View.GONE);
                break;
            default:
                Log.i(TAG, "setCurrentDataName: 没有对应的图表");
                break;
        }
    }

    /**
     * 响应点击柱状图的每个小柱子，发消息弹框
     **/
    private final OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, Highlight h) {
            Log.i(TAG, "onValueSelected:  " + e + "   " + h);
            if (isShowPopwind) {
                mEntry = e;
                delayedHide(mHandler, 6);
            } else {
                float x = e.getX();
                setCurrentDataName((int) x);
                Log.i(TAG, "onValueSelected: 日排行榜不需要弹框    " + x);
            }
        }

        @Override
        public void onNothingSelected() {
            Log.i("TGH", "onNothingSelected: ");
        }
    };


    private final IAxisValueFormatter mIAxisValueFormatter_X = new IAxisValueFormatter() {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            String a = "";
            switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
                case AnalyzeIntentUtil.BAR_CHART://柱状图
                    a = ((int) value) + 1 + "";
                    break;
                case AnalyzeIntentUtil.LINAR_CHART://折线图
                    if (xForGraph != null && !xForGraph.isEmpty()) {
                        a = xForGraph.get(Math.min(Math.max((int) value, 0), xForGraph.size() - 1));
                    }
                    break;
                case AnalyzeIntentUtil.PIE_CHART://扇形图
                    break;
                default:
                    break;
            }
            return a;
        }

        @Override
        public int getDecimalDigits() {
            return 0;
        }
    };

    private final DecimalFormat mFormat = new DecimalFormat("###,###,###,##0");
    private final IAxisValueFormatter mIAxisValueFormatter_Y = new IAxisValueFormatter() {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            String s;
            if (mFormat != null) {
                s = mFormat.format(value);
            } else {
                s = null;
            }
            return s;
        }

        @Override
        public int getDecimalDigits() {
            return 0;
        }
    };

    private final IAxisValueFormatter mIAxisValueFormatter_Y_right = new IAxisValueFormatter() {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            String s;
            if (mFormat != null) {
                if (has_K) {
                    int l = levelValue / 1000;
                    String s1 = String.valueOf(l);
                    s1 = s1.substring(1);
                    s = mFormat.format(value) + s1 + " K";
                } else {
                    s = mFormat.format(value);
                }
            } else {
                s = null;
            }
            return s;
        }

        @Override
        public int getDecimalDigits() {
            return 0;
        }
    };


    /**
     * 获取最大值
     **/
    private void setNeighborhoodComparison(List<LinearItem> linearItems, String nameGraph) {
        Comparator comp = new Comparator() {
            public int compare(Object o1, Object o2) {
                Integer p1 = (Integer) o1;
                Integer p2 = (Integer) o2;
                if (p1 < p2)
                    return -1;
                else if (p1 == p2)
                    return 0;
                else if (p1 > p2)
                    return 1;
                return 0;
            }
        };

        List<Integer> maxDataList = new ArrayList<>();
        mLevel = new ArrayList<>();
        List<Integer> data = null;
        LinearItem linearItem = null;
        for (int a = 0; a < linearItems.size(); a++) {
            linearItem = linearItems.get(a);
            data = linearItem.getData();
            if (data != null) {
                Integer max = (Integer) Collections.max(data, comp);
                Integer min = (Integer) Collections.min(data, comp);
                String name = linearItem.getName();
                mLevel.add(new Level(max, name, data));
                maxDataList.add(a, max);
                Log.i(TAG, "generateDataLine 最大值  : " + max + "     " + min);
            }
        }

        if (maxDataList.size() > 0) {
            Integer min = (Integer) Collections.min(maxDataList, comp);
            for (int m = 0; m < mLevel.size(); m++) {
                Level l = mLevel.get(m);
                l.setMin(min);
            }
        }
        generateDataLine(linearItems, mLevel);
    }

    Map<Integer, ArrayList<Entry>> mapEntry = new HashMap<Integer, ArrayList<Entry>>();
    private int levelValue = 0;

    /**
     * generates a random ChartData object with just one DataSet
     */
    public void generateDataLine(List<LinearItem> linearItems, List<Level> mLevel) {
        if (linearItems != null && linearItems.size() > 0) {
            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            LineDataSet set1;
            mMonthsValues = linearItems;
            for (int a = 0; a < linearItems.size(); a++) {
                ArrayList<Entry> yVals1 = new ArrayList<Entry>();
                LinearItem linearItem = linearItems.get(a);
                String name = linearItem.getName();
                List<Integer> data = linearItem.getData();
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        float i1 = data.get(i);
                        int levelByName = getLevelByName(mLevel, name);
                        if (i1 > 1000) {
                            has_K = true;
                            levelValue = levelByName;
                        }
                        i1 = i1 / levelByName;
                        Entry entry = new Entry(i, i1);
                        entry.setLevel(levelByName);
                        entry.setName(name);
                        yVals1.add(entry);
                    }
                    mapEntry.put(a, yVals1);
                    if (tvChannelLineChart.getData() != null &&
                            tvChannelLineChart.getData().getDataSetCount() > 0) {
                        set1 = (LineDataSet) tvChannelLineChart.getData().getDataSetByIndex(a);
                        set1.setValues(yVals1);
                    } else {
                        set1 = new LineDataSet(yVals1, name);
                        if (a % 2 == 0) {
                            Log.i("TGH", "setData: " + a);
                            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
                        } else {
                            set1.setAxisDependency(YAxis.AxisDependency.RIGHT);

                        }
                        int number = AnalyzeStringUtil.getInstance().getNumber(a);
                        set1.setColor(AnalyzeStringUtil.getInstance().COLORS[number]);
                        set1.setFillColor(AnalyzeStringUtil.getInstance().COLORS[number]);
                        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
                        set1.setCircleColor(AnalyzeStringUtil.getInstance().COLORS[number]);
                        set1.setLineWidth(2f);
                        set1.setCircleRadius(3f);
                        set1.setFillAlpha(65);

                        set1.setHighLightColor(Color.rgb(244, 117, 117));
                        set1.setDrawCircleHole(false);
                        set1.setDrawValues(false);
                        dataSets.add(set1); // add the datasets
                    }
                }
            }
            if (tvChannelLineChart.getData() != null &&
                    tvChannelLineChart.getData().getDataSetCount() > 0) {
                tvChannelLineChart.getData().notifyDataChanged();
                tvChannelLineChart.notifyDataSetChanged();
            } else {
                // create a data object with the datasets
                LineData data = new LineData(dataSets);
                data.setValueTextColor(Color.RED);
                data.setValueTextSize(9f);
                data.setValueFormatter(new LargeValueFormatter());
                // set data
                tvChannelLineChart.setData(data);
            }
        } else {
            Log.i(TAG, "generateDataLine: linearItems list is null ");
        }

        //点击显示数据组件
        XYMarkerView mv = new XYMarkerView(this, mIAxisValueFormatter_X);
        mv.setChartView(tvChannelLineChart); // For bounds control
        tvChannelLineChart.setMarker(mv); // Set the marker to the chart
    }

    private int getLevelByName(List<Level> data, String name) {
        int l = 0;
        Level level = null;
        for (int i = 0; i < data.size(); i++) {
            level = data.get(i);
            if (level.getName().equals(name)) {
                l = level.getLevel();
                break;
            }
        }
        return l;
    }

    /**
     * generates a random ChartData object with just one DataSet
     */
    private void generateDataBar(List<LinearItem> linearItems, String nameGraph) {
        if (linearItems != null && linearItems.size() > 0) {
            LinearItem linearItem = linearItems.get(linearItems.size() - 1);
            if (linearItem != null) {
                List<Integer> data = linearItem.getData();
                has_K = false;
                ArrayList<BarEntry> entries = new ArrayList<>();
                //String[] split = AnalyzeStringUtil.getInstance().getStringArray(cnt);
                if (data != null && data.size() > 0) {
                    mMonthsValues = linearItems;
                    for (int i = 0; i < data.size(); i++) {
                        int i1 = data.get(i);
                        if (i1 > 1000) {
                            has_K = true;
                        }
                        entries.add(new BarEntry(i, i1));
                    }
                    barEntriyList.addAll(entries);
                    BarDataSet dSet;
                    if (barChart.getData() != null &&
                            barChart.getData().getDataSetCount() > 0) {
                        barChart.getXAxis().setAxisMinimum(0f);
                        barChart.getXAxis().setAxisMaximum(15f);
                        dSet = (BarDataSet) barChart.getData().getDataSetByIndex(0);
                        dSet.setValues(entries);
                        barChart.getData().notifyDataChanged();
                        barChart.notifyDataSetChanged();
                        Log.i(TAG, "generateDataBar: 走了柱状的数据缓存");
                        barChart.invalidate();
                    } else {
                        dSet = new BarDataSet(entries, nameGraph);
                        dSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
                        dSet.setHighLightAlpha(255);
                        dSet.setDrawValues(false);
                        //设置点击后的透明度 数值是0~99;
                        dSet.setHighLightAlpha(37);

                        BarData cd = new BarData(dSet);
                        cd.setBarWidth(0.9f);
                        cd.setValueTextSize(10f);
                        // set data
                        barChart.setData(cd);
                        //需要在设置数据源后生效
                        //barChart.setVisibleXRangeMaximum(8);
                        Log.i(TAG, "generateDataBar: 柱状图没有数据缓存");
                    }

                } else {
                    Log.i(TAG, "generateDataBar: data is null");
                }
            }
        } else {
            Log.i(TAG, "generateDataBar: linearItems list is null");
        }
    }

    /**
     * 获取百分之一值
     */
    private float getMil(List<LinearItem> linearItems) {
        float num = 0;
        if (linearItems != null && linearItems.size() > 0) {
            for (int i = 0; i < linearItems.size(); i++) {
                LinearItem linearItem = linearItems.get(i);
                int yAxis = linearItem.getYAxis();
                num = yAxis + num;
            }
        }
        Log.i(TAG, "getMil   num: " + num + "  千分之一 ： " + num / 100);
        return num / 100;
    }


    List<LinearItem> popWindPieList = new ArrayList<>();

    private List<LinearItem> getManagePieDate(List<LinearItem> linearItems) {
        popWindPieList.clear();
        float mil = getMil(linearItems);
        int num = 0;
        List<LinearItem> list = new ArrayList<>();
        if (linearItems != null && linearItems.size() > 0) {
            LinearItem linearItem = null;
            for (int i = 0; i < linearItems.size(); i++) {
                linearItem = linearItems.get(i);
                int yAxis = linearItem.getYAxis();
                if (yAxis <= mil) {
                    num = yAxis + num;
                    popWindPieList.add(linearItem);
                } else {
                    list.add(linearItem);
                }
            }
            LinearItem item = new LinearItem();
            item.setName("其他");
            if (num > mil) {
                item.setYAxis(num);
                list.add(item);
            } else {
                if (num > 0) {
                    item.setYAxis((int) mil);
                    item.setName("其他");
                    item.setType("@" + num);
                    list.add(item);
                }
            }
        }
        return list;
    }


    private ArrayList<PieEntry> entries_pie;

    private void generateDataPie(List<LinearItem> linearItems) {

        entries_pie = new ArrayList<PieEntry>();
        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        if (linearItems != null && linearItems.size() > 0) {
            mMonthsValues = linearItems;
            for (int i = 0; i < linearItems.size(); i++) {
                LinearItem linearItem = linearItems.get(i);
                entries_pie.add(new PieEntry((float) linearItem.getYAxis(), linearItem.getName()));
            }
            PieDataSet dataSet = new PieDataSet(entries_pie, /*nameGraph*/"");
            dataSet.setSliceSpace(3f);
            dataSet.setSelectionShift(5f);
            // add a lot of colors
            ArrayList<Integer> colors = new ArrayList<Integer>();
            for (int c : ColorTemplate.VORDIPLOM_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.JOYFUL_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.COLORFUL_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.LIBERTY_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.PASTEL_COLORS)
                colors.add(c);

            colors.add(ColorTemplate.getHoloBlue());
            dataSet.setColors(colors);
            //dataSet.setSelectionShift(0f);

            dataSet.setValueLinePart1OffsetPercentage(80.f);
            //设置每个扇形数值上黑线的长度
            dataSet.setValueLinePart1Length(0.5f);
            dataSet.setValueLinePart2Length(0.5f);
            //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
            dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

            PieData data = new PieData(dataSet);
            Log.i(TAG, "generateDataPie:   " + entries_pie);
            //设置百分比显示
            data.setValueFormatter(new PercentFormatter(entries_pie));
            data.setValueTextSize(8f);
            data.setValueTextColor(Color.BLACK);
            pieChart.setData(data);

            // undo all highlights
            pieChart.highlightValues(null);
            pieChart.invalidate();
        }
    }

    private void getXString() {
        if (mAnalyzeResult != null) {
            List<String> xList = mAnalyzeResult.getxForGraph();
            if (xList != null && xList.size() > 0) {
                xForGraph = xList;
            } else {
                Log.i(TAG, "getXString: xList is null");
                xForGraph = null;
            }
        } else {
            Log.i(TAG, "getXString: mAnalyzeResult is null");
            xForGraph = null;
        }

    }

    @Override
    public void onAnalyzeResult(int code, String error, AnalyzeResult analyzeResult) {
        current_data_name_title.setText("");
        current_data_name.setText("");
        Log.i("TGH", "onAnalyzeResult  接收接口返回的数据  : " + AnalyzeIntentUtil.getInstance(mContext).getIconType());
        if (analyzeResult != null) {
            mAnalyzeResult = analyzeResult;
        } else {
            Log.i(TAG, "onAnalyzeResult is null or error is : " + error + "   " + code);
        }
        mHandler.sendEmptyMessage(3);
        mHandler.sendEmptyMessageDelayed(0,300);
        curr = System.currentTimeMillis() - curr;
        Log.i(TAG, "onAnalyzeResult  时间间隔   : " + curr);
    }


    private void setNoChartData() {
        if (mMonthsValues != null) {
            mMonthsValues.add(new LinearItem());
            showIconType(mMonthsValues, null);
        }
    }

    private void showIconType(List<LinearItem> linearItems, String nameGraph) {
        if (linearItems != null && linearItems.size() > 0) {
            switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
                case AnalyzeIntentUtil.LINAR_CHART://折线图
//                    generateDataLine(linearItems, nameGraph);
                    setNeighborhoodComparison(linearItems, nameGraph);
                    barChartLayout.setVisibility(View.GONE);
                    pieChartLayout.setVisibility(View.GONE);
                    linarChartLayout.setVisibility(View.VISIBLE);
                    break;
                case AnalyzeIntentUtil.BAR_CHART://柱状图
                    generateDataBar(linearItems, nameGraph);
                    linarChartLayout.setVisibility(View.GONE);
                    pieChartLayout.setVisibility(View.GONE);
                    barChartLayout.setVisibility(View.VISIBLE);
                    break;
                case AnalyzeIntentUtil.PIE_CHART://扇形图
                    linearItems = getManagePieDate(linearItems);
                    generateDataPie(linearItems);
                    linarChartLayout.setVisibility(View.GONE);
                    barChartLayout.setVisibility(View.GONE);
                    pieChartLayout.setVisibility(View.VISIBLE);
                    break;
                default:
                    Log.i(TAG, "showIconType: 没有匹配的图表");
                    break;
            }
        } else {
            Log.i(TAG, "showIconType: linearItems is null");
        }
    }

    /**
     * 返回
     **/
    @OnClick(R.id.analyze_back)
    public void onBackClick(View view) {
        this.finish();
    }

    /**
     * 分享
     */
    @OnClick(R.id.analyze_share)
    public void onShareClick(View view) {
        Log.i(TAG, "onShareClick: ");
        Bitmap bit = null;
        switch (AnalyzeIntentUtil.getInstance(LineChartActivity.this).getIconType()) {
            case AnalyzeIntentUtil.LINAR_CHART://折线图
                if (tvChannelLineChart != null) {
                    tvChannelLineChart.saveToGallery("thumbnail" /*+ System.currentTimeMillis()*/, 100);
                    bit = tvChannelLineChart.getChartBitmap();
                }
                break;
            case AnalyzeIntentUtil.BAR_CHART://柱状图
                barChart.saveToGallery("thumbnail" /*+ System.currentTimeMillis()*/, 100);
                bit = barChart.getChartBitmap();
                break;
            case AnalyzeIntentUtil.PIE_CHART://扇形图
                pieChart.saveToGallery("thumbnail" /*+ System.currentTimeMillis()*/, 100);
                bit = pieChart.getChartBitmap();
                break;
            default:
                Log.i(TAG, "showIconType: 没有匹配的图表");
                break;
        }
        if (mAnalyzeResult != null) {
            ShareAction shareAction = new ShareAction(LineChartActivity.this);
            shareAction.withTitle(mAnalyzeResult.getTitleGraph());
            // TODO: 2016/12/26 暂时注释掉 ，需要修改
            shareAction.withText(/*mAnalyzeResult.getNameGraph()*/"");
            shareAction.withTargetUrl("http://www.skyworth-hightong.com");

            if (bit != null) {
                shareAction.withMedia(new UMImage(this.getApplicationContext(), bit));
            }
            shareAction.setDisplayList(SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE).setCallback(umShareListener).open();
        } else {
            Log.i(TAG, "onShareClick: mAnalyzeResult is null");
        }
    }

    private final UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA share_media) {
            Log.i(TAG, "分享返回值  onResult: " + share_media);
        }

        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            Log.i(TAG, "分享失败 onError: " + share_media);
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media) {
            Log.i(TAG, "取消分享  onCancel: " + share_media);
        }
    };

    /**
     * 重置
     */
    @OnClick(R.id.analyze_reset)
    public void onResetClick(View view) {
        mHandler.sendEmptyMessage(4);
    }

    /**
     * 点击大图
     **/
    @OnClick(R.id.big_picture)
    public void onBigPictureClick(View view) {
        mHandler.sendEmptyMessage(7);
        Log.i(TAG, " 进入大图之前   onBigPictureClick   : " + mAnalyzeResult);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AnalyzeResultObservable.getInstance().unregisterObserver(this);
        AnalyzePopWindowObservable.getInstance().unregisterObserver(this);
    }

    @Override
    public void onAnalyzePopWindow(int code, String error, AnalyzeResult analyzeResult) {
        if (analyzeResult != null) {
            mPopAnalyzeResult = analyzeResult;
            delayedHide(mHandler, 5);
        } else {
            Log.i(TAG, "onAnalyzePopWindow: analyzeResult is null  " + error);
        }
    }

    /**
     * 通知指定的控制栏进行延时隐藏，适合此方法的参数的都可使用此方法
     **/
    private void delayedHide(Handler handler, int what) {
        if (handler != null) {
            while (handler.hasMessages(what)) {
                handler.removeMessages(what);
            }
            handler.sendEmptyMessageDelayed(what, 300);
        } else {
            Log.i(TAG, "delayedHide: handler is null");
        }
    }

    @Override
    public void onBackPressed() {
        FiltersUtil.getInstance(mContext).closeSlidingDrawer();
    }

    /**
     * 显示根据不同页面选择不同的筛选展示内容
     */
    private void ShowScreenView(MenuInfo arg, MenuInfo groupPosition, MenuInfo childPosition) {
        FiltersUtil.getInstance(mContext).setFiltersUtilListener(mFiltersUtilListener);
        /**
         * 调用FiltersUtil，开始请求查询条件，并构建筛选节目UI，然后请求图表数据，以待返回结果。
         * 筛选UI通过FiltersUtilListener监听返回
         */
        if (arg != null && groupPosition != null && childPosition != null) {
            FiltersUtil.getInstance(mContext).launch(arg, groupPosition, childPosition);
        } else {
            AnalyzeResultObservable.getInstance().onAnalyzeResult(-200, "接到的三个实体对象为空", null);
        }
    }

    private FiltersUtil.FiltersUtilListener mFiltersUtilListener = new FiltersUtil.FiltersUtilListener() {

        @Override
        public void addFilterView(View filterView) {
            // 加筛选View
            if (filterView != null) {
                if (filter_ui.getChildCount() >= 1)
                    filter_ui.removeViewAt(0);
                filter_ui.addView(filterView, 0);
            }
        }

        @Override
        public void commitFilters(String filterString, String day, String week) {
            // TODO 显示所选筛选条件
            if (week == null) {
                week = "";
            }
            if (day == null) {
                day = "";
            }
            if (TextUtils.isEmpty(week) && TextUtils.isEmpty(day)) {
                line_chart_search_day.setVisibility(View.GONE);
            } else {
                line_chart_search_day.setVisibility(View.VISIBLE);
                line_chart_search_day.setText("筛选日期 ：" + day + "  " + week);
            }

            if (filterString == null || TextUtils.isEmpty(filterString)) {
                line_chart_search_other.setVisibility(View.GONE);
            } else {
                line_chart_search_other.setVisibility(View.VISIBLE);
                line_chart_search_other.setText("其他条件 ：" + filterString);
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * 是否重置折线图
     **/
    private void isResetClickLineChart() {
        if (tvChannelLineChart != null) {
            //把隐藏的折线图显示出来
            if (mapEntry != null && mapEntry.size() > 0) {
                for (int i = 0; i < mapEntry.size(); i++) {
                    setLineVisible(i, true);
                }
            }
            tvChannelLineChart.animateY(3000);
            tvChannelLineChart.fitScreen();
        } else {
            Log.i(TAG, "isResetClickLineChart: tvChannelLineChart is null");
        }
    }

    /**
     * 折线的显示和隐藏
     **/
    private void setLineVisible(int number, boolean isShow) {
        if (tvChannelLineChart != null) {
            LineDataSet set2 = (LineDataSet) tvChannelLineChart.getData().getDataSetByIndex(number);
            if (set2 != null) {
                set2.setVisible(isShow);
            }
            tvChannelLineChart.invalidate();
        }

    }

}
