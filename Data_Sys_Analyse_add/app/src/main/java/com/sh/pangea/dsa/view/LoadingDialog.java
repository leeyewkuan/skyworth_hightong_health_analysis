package com.sh.pangea.dsa.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sh.pangea.dsa.R;


public class LoadingDialog extends Dialog {

	private TextView tvContent;

	public LoadingDialog(Context context) {
		super(context, R.style.dialog_fullscreen2);
		setContentView(R.layout.loading_dialog);
		tvContent = (TextView) findViewById(R.id.contentTextView);
		setProperty();
	}

	public final void setProperty() {
		Window window = getWindow();// 　　　得到对话框的窗口．
		WindowManager.LayoutParams wl = window.getAttributes();
		wl.x = 0;// 这两句设置了对话框的位置．0为中间
		wl.y = 0;
		wl.alpha = 1;// 这句设置了对话框的透明度
		wl.gravity = Gravity.CENTER;
		window.setAttributes(wl);
	}
	
	public void setShowContent(String msg){
		if(msg != null){
			tvContent.setText(msg);
			tvContent.setVisibility(View.VISIBLE);
		}else{
			tvContent.setVisibility(View.GONE);
		}
	}
}
