package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.pangea.dsa.R;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by zhouyucheng on 2017/5/31.
 */

public class CurrentDataTypeAdapter extends BaseAdapter {
    private List<String> names;
    private Context mContext;
    private LayoutInflater mInflater;
    private int enterPosition = 0;

    public CurrentDataTypeAdapter(Context context, List<String> names ) {
        this.names = names;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        return names.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CurentViewHolder mCurentViewHolder = null;
        if (convertView == null) {
            mCurentViewHolder = new CurentViewHolder();
            convertView = mInflater.from(mContext).inflate(R.layout.item_cdata_type, parent, false);
            mCurentViewHolder.mText = (TextView) convertView.findViewById(R.id.ct_type);
            mCurentViewHolder.mView = convertView.findViewById(R.id.ct_line);
            convertView.setTag(mCurentViewHolder);
        } else {
            mCurentViewHolder = (CurentViewHolder) convertView.getTag();
        }
        mCurentViewHolder.mText.setText(names.get(position));
        if (enterPosition == position) {
            mCurentViewHolder.mView.setVisibility(View.VISIBLE);
        } else {
            mCurentViewHolder.mView.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    class CurentViewHolder {
        View mView;
        TextView mText;
    }

    public void setEnterPosition(int position) {
        this.enterPosition = position;
        notifyDataSetChanged();
    }
}
