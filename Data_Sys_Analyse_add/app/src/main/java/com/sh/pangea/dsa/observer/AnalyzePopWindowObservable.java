package com.sh.pangea.dsa.observer;


import com.sh.pangea.dsa.bean.AnalyzeResult;

public class AnalyzePopWindowObservable extends Observable<AnalyzePopWindowObserver> {

    private static AnalyzePopWindowObservable mInstance;

    public static AnalyzePopWindowObservable getInstance() {
        if (mInstance == null) {
            mInstance = new AnalyzePopWindowObservable();
        }
        return mInstance;
    }

    public AnalyzePopWindowObservable() {
        super();
    }

    /**
     * 用于通知ui层接口调用的结果
     **/
    public void onAnalyzeResult(int code, String error, AnalyzeResult analyzeResult) {
        for (AnalyzePopWindowObserver cc : mObservers) {
            cc.onAnalyzePopWindow(code, error, analyzeResult);
        }
    }


}
