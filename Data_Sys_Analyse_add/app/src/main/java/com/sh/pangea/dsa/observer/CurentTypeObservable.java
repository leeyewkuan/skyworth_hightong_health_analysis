package com.sh.pangea.dsa.observer;

import com.sh.pangea.dsa.bean.AnalyzeResult;

/**
 * Created by zhouyucheng on 2017/5/31.
 */

public class CurentTypeObservable extends Observable<CurentTypeObserver> {
    private static CurentTypeObservable mInstance;

    public static CurentTypeObservable getInstance() {
        if (mInstance == null) {
            mInstance = new CurentTypeObservable();
        }
        return mInstance;
    }

    public CurentTypeObservable() {
        super();
    }

    /**
     * 通知切换
     **/
    public void updateType(int code) {
        for (CurentTypeObserver cc : mObservers) {
            cc.updateType(code);
        }
    }
}
