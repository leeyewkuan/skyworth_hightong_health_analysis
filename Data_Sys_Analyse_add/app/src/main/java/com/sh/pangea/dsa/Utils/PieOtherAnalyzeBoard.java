package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sh.lib.dsa.bean.analyzeresult.LinearItem;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.adapter.PieOtherAdapter;

import java.util.List;

/**
 * Created by TianGenhu on 2016/10/12.
 */

public class PieOtherAnalyzeBoard extends PopupWindow {
    private ListView pie_other_listview;
    private TextView pie_other_title;


    public PieOtherAnalyzeBoard(Context context, List<LinearItem> linearItems) {
        initView(context);
        if (linearItems != null) {
            PieOtherAdapter pieOtherAdapter = new PieOtherAdapter(context, linearItems);
            pie_other_listview.setAdapter(pieOtherAdapter);
        }
    }

    private void initView(Context context) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.pie_other_analyze_board, null);
        rootView.findViewById(R.id.pie_other_popwindowLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        pie_other_listview = (ListView) rootView.findViewById(R.id.pie_other_listview);
        pie_other_title = (TextView) rootView.findViewById(R.id.pie_other_title);
        pie_other_title.setText("其他数据展示");
        setContentView(rootView);
        setOnTouch();
        setFocusable(true);
        setTouchable(true);
    }

    private void setOnTouch() {
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
    }


}
