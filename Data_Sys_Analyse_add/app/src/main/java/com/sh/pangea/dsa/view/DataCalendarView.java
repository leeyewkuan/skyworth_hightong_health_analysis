package com.sh.pangea.dsa.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.sh.pangea.dsa.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Data_Sys_Analyse 日历控件
 * Created by LB on 2016/11/8,10:23
 */

public class DataCalendarView extends FrameLayout implements CalendarItemView.OnDateChange {
    private static final String TAG = "DataCalendarView";

    private CalendarItemView year;
    private CalendarItemView mooth;
    private CalendarItemView date;

    private Context mContext;

    public DataCalendarView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public DataCalendarView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    int color;
    float dimension;
    public DataCalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.dateCalendarView);
        color = typedArray.getColor(R.styleable.dateCalendarView_showTimeColor, getResources().getColor(R.color.colorPrimary));
        dimension = typedArray.getDimension(R.styleable.dateCalendarView_showTimeSize, 28);
        typedArray.recycle();
        initView();
    }

    private void initView() {


        View inflate = View.inflate(mContext, R.layout.calendar_view, this);
        year = (CalendarItemView) inflate.findViewById(R.id.year);
        year.setShowTimeColor(color);
        year.setShowTimeSize(dimension);
        mooth = (CalendarItemView) inflate.findViewById(R.id.mooth);
        mooth.setShowTimeColor(color);
        mooth.setShowTimeSize(dimension);
        date = (CalendarItemView) inflate.findViewById(R.id.date);
        date.setShowTimeColor(color);
        date.setShowTimeSize(dimension);

        year.setOnDateChange(this);
        mooth.setOnDateChange(this);
        date.setOnDateChange(this);
        initTime();
    }


    // 设置的初始时间
    private int defaultYear;
    private int defaultMooth;
    private int defaultDate;

    /**
     * 初始化日期为当天
     */
    public void initTime(){
        Date date = new Date();
        initTime(date);
    }
    /** 初始化日期*/
    public void initTime(Date date) {
        defaultYear = date.getYear() + 1900;
        defaultMooth = date.getMonth() + 1;
        defaultDate = date.getDate();
        year.setShowTime(defaultYear + "");
        mooth.setShowTime(defaultMooth + "");
        this.date.setShowTime(defaultDate + "");
    }

    /** 设置时间的最大值和最小值*/
    public void setDateInterval(int yearMax,int yearMin,int moothMax,int moothMin,int dateMax,int dateMin){
        setMaxYearTime(yearMax);
        setMinYearTime(yearMin);
        setMaxMoothTime(moothMax);
        setMinMoothTime(moothMin);
        setMaxDateTime(dateMax);
        setMinDateTime(dateMin);
    }

    /**设置年最大显示   格式 YYYY  必须大于1970 小于 9999*/
    public void setMaxYearTime(int yearTime) {
        if (yearTime > 1970 && yearTime < 9999) {
            year.setMaxTime(yearTime);
        }
    }

    /** 设置年最小显示  格式 YYYY  必须大于1970 小于 9999 */
    public void setMinYearTime(int yearTime) {
        if (yearTime > 1970 && yearTime < 9999) {
            year.setMinTime(yearTime);
        }
    }

    /**设置月 最大显示*/
    public void setMaxMoothTime(int moothTime) {
        if (moothTime > 0 && moothTime < 13) {
            mooth.setMaxTime(moothTime);
        }
    }

    /**设置月 最小显示*/
    public void setMinMoothTime(int moothTime) {
        if (moothTime > 0 && moothTime < 13) {
            mooth.setMinTime(moothTime);
        }
    }

    /**设置日 最大显示*/
    public void setMaxDateTime(int dataTime) {
        if (dataTime > 0 && dataTime < 32) {
            date.setMaxTime(dataTime);
        }
    }

    /**设置日 最小显示*/
    public void setMinDateTime(int dataTime) {
        if (dataTime > 0 && dataTime < 32) {
            date.setMinTime(dataTime);
        }
    }

    /**获取一个月中最大天数*/
    private int getDaysOfMonth(int year, int mooth, int date) {
        Calendar aCalendar = Calendar.getInstance(Locale.CHINA);
        aCalendar.set(year, mooth - 1, date);
        int day = aCalendar.getActualMaximum(Calendar.DATE);
        Log.i(TAG, "day:" + day + " aCalendar:" + aCalendar.getTime());
        return day;
    }

    /**获取时间yyyy-mm-dd*/
    public String getTimeStr() {
        StringBuilder builder = new StringBuilder();
        builder.append(year.getShowTime()).append("-");

        String showTime = mooth.getShowTime();
        if (showTime.length() == 1) {
            showTime = "0" + showTime;
        }
        builder.append(showTime).append("-");

        String showTime1 = date.getShowTime();
        if (showTime1.toCharArray().length == 1) {
            showTime1 = "0" + showTime1;
        }
        builder.append(showTime1);
        return builder.toString();
    }

    public Date getTime() {
        String dateStr = getTimeStr();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(dateStr);
        }catch (Exception e){
            Log.i(TAG,"getTime: is error");
            return new Date();
        }
    }

    public int getYear(){
         return year.getShowTimeInt();
    }
    public int getMooth(){
        return mooth.getShowTimeInt();
    }
    public int getDate(){
        return  date.getShowTimeInt();
    }


    private OnDateChange onDateChange;

    /**
     * 设置时间改变监听
     */
    public void setOnDateChange(OnDateChange onDateChange) {
        this.onDateChange = onDateChange;
    }

    public interface OnDateChange {
        /**
         * 时返回格式yyyy-mm-dd   2016-11-08
         */
        void onDateChange(String time);
    }

    @Override
    public void changTime(View view, View parentView, String time) {

        if (parentView.getId() == R.id.year || parentView.getId() == R.id.mooth) {
            String showTime = year.getShowTime();
            String showTime1 = mooth.getShowTime();
            String showTime2 = date.getShowTime();
            int nowShowDate = 1;
            int showDate = 1;
            int dateNumber = 1;

            int yearTime = 0;
            int moothTime = 0;

            try {
                yearTime = Integer.parseInt(showTime);
                moothTime = Integer.parseInt(showTime1);
                nowShowDate = Integer.parseInt(showTime2);
                showDate = Integer.parseInt(showTime2);
                dateNumber = getDaysOfMonth(yearTime, moothTime, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (yearTime == defaultYear && moothTime == defaultMooth) {
                date.setMaxTime(defaultDate);
            } else {
                date.setMaxTime(dateNumber);
            }

            if (nowShowDate > dateNumber || (yearTime == defaultYear && moothTime == defaultMooth)) {
                if (yearTime == defaultYear && moothTime == defaultMooth) {
                    date.setShowTime(defaultDate + "");
                } else {
                    date.setShowTime(dateNumber + "");
                }
            }
        }
        if (onDateChange != null) {
            String currentTime = getTimeStr();
            onDateChange.onDateChange(currentTime);
        }
    }

    /**
     * 设置日期为不可点击状态
     */
    public void setNoOnTouch() {
        year.setNoOnTouch();
        mooth.setNoOnTouch();
        date.setNoOnTouch();
    }
}
