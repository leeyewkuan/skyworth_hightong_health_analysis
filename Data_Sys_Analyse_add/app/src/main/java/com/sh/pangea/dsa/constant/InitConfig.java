package com.sh.pangea.dsa.constant;


public class InitConfig {

	/****/
	public static final int CON_TIME_OUT = 5000;
	/*****/
	public static final int SO_TIME_OUT = 5000;
	public static final String portalAddress = "https://www.pangeachina.com";
	public static final String portalRegionid = "dsa";
	public static final String terminalType = "1";
	/**portal地址的区域标示*/
	public static final String regionCode ="dsa";

	private static final String[] portalFilters = new String[]{
		"UPORTAL"
	};

	public static String[] getPortalFilters(){
		return portalFilters.clone();
	}

}
