package com.sh.pangea.dsa.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;


/**
 *
 * Created by sam on 2016/3/24.
 */
public class RoundedImageView extends ImageView
{

    public static final int CORNER_NONE = 0;
    public static final int CORNER_TOP_LEFT = 1;
    public static final int CORNER_TOP_RIGHT = 1 << 1;
    public static final int CORNER_BOTTOM_LEFT = 1 << 2;
    public static final int CORNER_BOTTOM_RIGHT = 1 << 3;
    public static final int CORNER_ALL = CORNER_TOP_LEFT | CORNER_TOP_RIGHT | CORNER_BOTTOM_LEFT | CORNER_BOTTOM_RIGHT;

    public RoundedImageView(Context context) {

        super(context);
        Log.i("CGZ","RoundedImageView构造1");
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.i("CGZ","RoundedImageView构造2");
    }

    public void setImage(Bitmap bmp, int radius, int corners) {
        Log.i("CGZ","RoundedImageView中setImage");
        setImageDrawable(new RoundedDrawable(bmp, radius, corners));
    }

    public static class RoundedDrawable extends Drawable
    {

        protected final float cornerRadius;

        protected final RectF mRect = new RectF(), mBitmapRect;
        protected final BitmapShader bitmapShader;
        protected final Paint paint;
        private int corners;

        public RoundedDrawable(Bitmap bitmap, int cornerRadius, int corners) {
            this.cornerRadius = cornerRadius;
            this.corners = corners;

            bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            mBitmapRect = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());

            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(bitmapShader);
        }

        @Override
        protected void onBoundsChange(Rect bounds) {
            super.onBoundsChange(bounds);
            mRect.set(0, 0, bounds.width(), bounds.height());

            // Resize the original bitmap to fit the new bound
            Matrix shaderMatrix = new Matrix();
            shaderMatrix.setRectToRect(mBitmapRect, mRect, Matrix.ScaleToFit.FILL);
            bitmapShader.setLocalMatrix(shaderMatrix);

        }

        @Override
        public void draw(Canvas canvas) {
            canvas.drawRoundRect(mRect, cornerRadius, cornerRadius, paint);
            int notRoundedCorners = corners ^ CORNER_ALL;
            if ((notRoundedCorners & CORNER_TOP_LEFT) == 0) {
                Log.i("CGZ","左上");
                canvas.drawRect(0, 0, cornerRadius, cornerRadius, paint);
            }
            else
            {
                Log.i("CGZ","左上e");
            }
            if ((notRoundedCorners & CORNER_TOP_RIGHT) == 0) {
                Log.i("CGZ","右上");
                canvas.drawRect(mRect.right - cornerRadius, 0, mRect.right, cornerRadius, paint);
            }
            else
            {
                Log.i("CGZ","右上e");
            }
            if ((notRoundedCorners & CORNER_BOTTOM_LEFT) == 0) {
                Log.i("CGZ","左下");
                canvas.drawRect(0, mRect.bottom - cornerRadius, cornerRadius, mRect.bottom, paint);
            }
            else
            {
                Log.i("CGZ","左下e");
            }
            if ((notRoundedCorners & CORNER_BOTTOM_RIGHT) == 0) {
                Log.i("CGZ","右下");
                canvas.drawRect(mRect.right - cornerRadius, mRect.bottom - cornerRadius, mRect.right, mRect.bottom, paint);
            }
            else
            {
                Log.i("CGZ","右下e");
            }
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }

        @Override
        public void setAlpha(int alpha) {
            paint.setAlpha(alpha);
        }

        @Override
        public void setColorFilter(ColorFilter cf) {
            paint.setColorFilter(cf);
        }
    }
}
