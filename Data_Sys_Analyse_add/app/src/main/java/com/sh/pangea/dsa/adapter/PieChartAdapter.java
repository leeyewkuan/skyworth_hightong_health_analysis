package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.lib.dsa.bean.analyzeresult.LinearItem;
import com.sh.pangea.dsa.R;

import java.util.List;


/**
 * Created by TianGenhu on 2016/10/10.
 */

public class PieChartAdapter extends BaseExpandableListAdapter {
//    private final Context mContext;
//    private List<LinearItem> listLinearItem;
//    private List<String> xForGraph;
//
//    public PieChartAdapter(Context context, List<LinearItem> monthsValues, List<String> xForGraph) {
//        this.mContext = context;
//        this.listLinearItem = monthsValues;
//        this.xForGraph = xForGraph;
//    }
//
//    @Override
//    public int getCount() {
//        return listLinearItem.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return listLinearItem.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        if (convertView == null) {
//            holder = new ViewHolder();
//            convertView = LayoutInflater.from(mContext).inflate(R.layout.public_listview_item, null);
//            holder.data_item = (LinearLayout) convertView.findViewById(R.id.data_item);
//            holder.data_id = (TextView) convertView.findViewById(R.id.data_id);
//            holder.tvName = (TextView) convertView.findViewById(R.id.data_name);
//            holder.tvValue = (TextView) convertView.findViewById(R.id.data_value);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//
//        if (listLinearItem.size() > position) {
//            LinearItem linearItem = listLinearItem.get(position);
//            holder.tvName.setText(linearItem.getName());
//            holder.data_id.setText((position + 1) + "");
//            holder.tvValue.setText(linearItem.getYAxis());
//        } else {
//            holder.tvValue.setText("--");
//        }
//        if (position % 2 != 0) {
//            holder.data_item.setBackgroundResource(R.color.colorListItemPressed);
//        } else {
//            holder.data_item.setBackgroundResource(R.color.transparent_white);
//        }
//        return convertView;
//    }
//
//    private static class ViewHolder {
//        TextView tvName;
//        TextView tvValue;
//        TextView data_id;
//        LinearLayout data_item;
//    }


    private Context context;
    LayoutInflater inflater;
    private List<LinearItem> mMonthsValues;

    public PieChartAdapter(Context context, List<LinearItem> monthsValues) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mMonthsValues = monthsValues;
    }

    public Object getChild(int groupPosition, int childPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > groupPosition) {
                LinearItem linearItem = mMonthsValues.get(childPosition);
                if (linearItem != null) {
                    return linearItem.getYAxis();
                }
            }
        }
        return "--";
    }

    public Object getChildX(int childPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > childPosition) {
                LinearItem linearItem = mMonthsValues.get(childPosition);
                if (linearItem != null) {
                    String name = linearItem.getName();
                    if (name != null) {
                        return name;
                    }
                }
            }
        }
        return "--";
    }

    public Object getChildXType(int childPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > childPosition) {
                LinearItem linearItem = mMonthsValues.get(childPosition);
                if (linearItem != null) {
                    String type = linearItem.getType();
                    if (type != null) {
                        return type;
                    }
                }
            }
        }
        return "--";
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(int groupPosition, int childPosition, boolean arg2, View convertView,
                             ViewGroup arg4) {
        convertView = inflater.inflate(R.layout.public_listview_item, null);
        LinearLayout data_item = (LinearLayout) convertView.findViewById(R.id.data_item);
        TextView data_id = (TextView) convertView.findViewById(R.id.data_id);
        TextView tvName = (TextView) convertView.findViewById(R.id.data_name);
        TextView tvValue = (TextView) convertView.findViewById(R.id.data_value);
        String name = getChildX(childPosition).toString();
        String value = getChild(groupPosition, childPosition).toString();
        String type = getChildXType(childPosition).toString();
        if (type.contains("@")) {
            String[] split = type.split("@");
            value = split[1];
        }
        tvName.setText(name);
        tvValue.setText(value);
        data_id.setText((childPosition + 1) + "");
        if (childPosition % 2 != 0) {
            data_item.setBackgroundResource(R.color.colorListItemPressed);
        } else {
            data_item.setBackgroundResource(R.color.transparent_white);
        }
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > groupPosition) {
                return mMonthsValues.size();
            }
        }
        return 0;
    }

    public Object getGroup(int groupPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > groupPosition) {
                LinearItem linearItem = mMonthsValues.get(groupPosition);
                if (linearItem != null) {
                    return linearItem.getName();
                }
            }
        }
        return "";
    }

    public int getGroupCount() {
        return 1;
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup arg3) {
        convertView = inflater.inflate(R.layout.listview_group_item, null);
        TextView listview_group_num = (TextView) convertView.findViewById(R.id.listview_group_num);
        listview_group_num.setText(getChildrenCount(groupPosition) + "");
        TextView groupNameTextView = (TextView) convertView.findViewById(R.id.listview_group_name);
        groupNameTextView.setText(getGroup(groupPosition).toString());
//        ImageView image = (ImageView) convertView.findViewById(R.id.listview_image);
//        image.setImageResource(R.mipmap.ic_user_album);
//        //更换展开分组图片
//        if (!isExpanded) {
//            image.setImageResource(R.mipmap.ic_launcher);
//        }
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    // 子选项是否可以选择
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }


}
