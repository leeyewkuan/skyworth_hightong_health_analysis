package com.sh.pangea.dsa.bean;

import java.io.Serializable;

/**
 * Created by liuqiwu on 2017/1/18.
 * 名词解释之名词
 */

public class Noun implements Serializable {
    /**
     * 解释
     */
    private String content;
    /**
     * 名词
     */
    private String word;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toString() {
        return "Noun{" +
                "content='" + content + '\'' +
                ", word='" + word + '\'' +
                '}';
    }
}
