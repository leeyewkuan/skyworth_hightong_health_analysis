package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.sh.lib.dsa.callback.DataFilterListener;
import com.sh.lib.dsa.callback.ListListener;
import com.sh.pangea.dsa.adapter.ScreenWeekAdapter;
import com.sh.pangea.dsa.bean.MenuInfo;
import com.sh.pangea.dsa.observer.AnalyzeResultObservable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TianGenhu on 2016/10/11.
 */

public class AnalyzeIntentUtil {
    /**
     * 总览
     **/
    private static final String S0 = "S0";
    /**
     * 各地区
     */
    private static final String S1 = "S1";
    private static final String S2 = "S2";
    private static final String S3 = "S3";
    private static final String S4 = "S4";
    /**
     * 占比
     **/
    private static final String A1 = "A1";
    private static final String A2 = "A2";
    private static final String A3 = "A3";
    private static final String A4 = "A4";
    private static final String A5 = "A5";
    private static final String A6 = "A6";

    private static final String B1 = "B1";
    private static final String B2 = "B2";
    private static final String B3 = "B3";
    private static final String B4 = "B4";
    private static final String B5 = "B5";

    private static final String C1 = "C1";
    private static final String C2 = "C2";
    private static final String C3 = "C3";
    private static final String C4 = "C4";
    private static final String C5 = "C5";

    private static final String D1 = "D1";
    private static final String D2 = "D2";
    private static final String D3 = "D3";
    private static final String D4 = "D4";
    private static final String D5 = "D5";


    private static final String E1 = "E1";
    private static final String E2 = "E2";
    private static final String E3 = "E3";


    private volatile static AnalyzeIntentUtil mInstance;
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    private String queryDate = getTimeStr();
    private String weekly = getTimeWeek() + "";
    private Context mContext;
    /**
     * tvName  回看频道名称/直播频道名称
     * medicalName 健康视频名称
     * 统一用  medicalName表示
     */
    private String medicalName;
    /**
     * 医院编码
     */
    private String instanceCode;
    /**
     * AP冷热程度
     * 0：最热  1：最冷
     */
    private String aphot;
    /**
     * ap MAC地址
     */
    private String apmac;
    /**
     * 模块ID
     */
    private String healthId;

    /**
     * 折线图
     */
    public static final String LINAR_CHART = "L";
    /**
     * 柱状图
     */
    public static final String BAR_CHART = "B";
    /**
     * 饼状图
     */
    public static final String PIE_CHART = "P";
    /**
     * 图标的类型，
     * L:折线图
     * B:柱状图
     * P:饼状图
     ***/
    private String iconType = "L";
    /**
     * viewType	String	显示内容区分（0：认证设备数 1：放行设备数 2：新增设备数 3：新增用户数）
     */
    private String viewType;
    /**
     * timeRange	String	时间间隔（28天，14天，7天）
     */
    private String timeRange;
    /**
     * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
     */
    private String compareDate1;
    /**
     * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
     */
    private String compareDate2;

    /**
     * 查询月份(格式为yyyy-MM)，报表中的筛选条件
     */
    private String playDay;

    private Map<String, String> temFilterMap = new HashMap<>();

    private AnalyzeIntentUtil(Context context) {
        this.mContext = context;
    }

    public synchronized static AnalyzeIntentUtil getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AnalyzeIntentUtil(context);
        }
        return mInstance;
    }

    /**
     * 设置筛选条件参数集
     *
     * @param temFilterMap 所选筛选参数集
     */
    public void setTemFilterMap(Map<String, String> temFilterMap) {
        this.temFilterMap = temFilterMap;
    }

    /**
     * 获取周信息
     *
     * @return
     */
    public String getWeekly() {
        weekly = temFilterMap.get("999");
        return weekly;
    }

    /**
     * 获取日信息
     *
     * @return
     */
    public String getQueryDate() {
        queryDate = temFilterMap.get("222");
        return queryDate;
    }

    /**
     * 获取一级频道
     *
     * @return
     */
    private String getRootChannelId() {
        String rootChannelId = temFilterMap.get("rootChannelId");
        if (rootChannelId == null)
            rootChannelId = "";
        return rootChannelId;
    }

    /**
     * 获取区域编码
     *
     * @return
     */
    public String getOperatorCode() {
        String operatorCode = temFilterMap.get("operatorCode");
        if (operatorCode == null) {
            operatorCode = "";
        }
        return operatorCode;
    }

    /**
     * 获取统计方式
     *
     * @return
     */
    private int getPlayType() {
        String playType = temFilterMap.get("playType");
        if (playType != null && !playType.isEmpty()) {
            return Integer.valueOf(playType);
        }
        return -1;
    }

    /**
     * 获取入口类型
     *
     * @return
     */
    private int getEn() {
        String en = temFilterMap.get("en");
        if (en != null && !en.isEmpty()) {
            return Integer.valueOf(en);
        }
        return -1;
    }

    /**
     * 获取图表类型
     *
     * @return
     */
    public String getIconType() {
        return iconType;
    }

    /**
     * 获取健康视频名称
     *
     * @return
     */
    public String getMedicalName() {
        return medicalName;
    }

    public void setMedicalName(String medicalName) {
        this.medicalName = medicalName;
    }

    /**
     * 获取所属医院
     *
     * @return
     */
    public String getInstanceCode() {
        instanceCode = temFilterMap.get("instanceCode");
        if (instanceCode == null)
            instanceCode = "";
        return instanceCode;
    }

    /**
     * AP冷热程度
     *
     * @return
     */
    public String getAphot() {
        aphot = temFilterMap.get("aphot");
        if (aphot == null) {
            aphot = "";
        }
        return aphot;
    }

    /**
     * 获取AP名称
     *
     * @return
     */
    public String getApmac() {
        apmac = temFilterMap.get("apmac");
        if (apmac == null) {
            apmac = "";
        }
        return apmac;
    }

    /**
     * 获取模块选择
     *
     * @return
     */
    public String getHealthId() {
        healthId = temFilterMap.get("healthId");
        if (healthId == null) {
            healthId = "";
        }
        return healthId;
    }

    /**
     * 获取区分类型
     */
    public String getViewType() {
        viewType = temFilterMap.get("viewType");
        if (viewType == null) {
            viewType = "";
        }
        return viewType;
    }

    /**
     * 设置区分类型(如：新关注人数、取消关注人数、净增关注人数、累计关注人数)
     *
     * @param viewType 区分类型
     */
    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    /**
     * 获取时间间隔
     */
    public String getTimeRange() {
        timeRange = temFilterMap.get("timeRange");
        if (timeRange == null) {
            timeRange = "";
        }
        return timeRange;
    }

    /**
     * 设置时间间隔(如：最近28天、最近14天、最近7天)
     *
     * @param timeRange 时间间隔
     */
    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }

    /**
     * 获取比较时间段1
     **/
    public String getCompareDate1() {
        compareDate1 = temFilterMap.get("compareDate1");
        if (compareDate1 == null) {
            compareDate1 = "";
        }
        return compareDate1;
    }

    /**
     * 设置比较时间段1
     *
     * @param compareDate1 时间段1
     */
    public void setCompareDate1(String compareDate1) {
        this.compareDate1 = compareDate1;
    }

    /**
     * 获取比较时间段2
     **/
    public String getCompareDate2() {
        compareDate2 = temFilterMap.get("compareDate2");
        if (compareDate2 == null) {
            compareDate2 = "";
        }
        return compareDate2;
    }

    /**
     * 设置比较时间段2
     *
     * @param compareDate2 时间段2
     */
    public void setCompareDate2(String compareDate2) {
        this.compareDate2 = compareDate2;
    }

    /**
     * 获取查询月份(格式为yyyy-MM)，报表中的筛选条件
     *
     * @return String
     */
    public String getPlayDay() {
        playDay = temFilterMap.get("222");
        return playDay;
    }

    /**
     * 设置查询月份(格式为yyyy-MM)，报表中的筛选条件
     *
     * @param playDay 查询月份
     */
    public void setPlayDay(String playDay) {
        this.playDay = playDay;
    }

    private Map<String, String> getHeaders() {
        String operatorCode = getOperatorCode();
        if (operatorCode == null || operatorCode.equals("")) {
            return null;
        } else {
            Map<String, String> headersMap = new HashMap<>();
            headersMap.put("areaCode", temFilterMap.get("operatorCode"));
            return headersMap;
        }
    }

    public void analyzeIntent(MenuInfo arg, MenuInfo groupPosition, MenuInfo childPosition) {
        iconType = childPosition.getChartType();
        Log.i("Main", "arg:" + arg.getId() + "   groupPosition:" + groupPosition.getId() + "   childPosition:" + childPosition.getId() + "  iconType :  " + iconType);
        switch (arg.getId()) {
            case AnalyzeResultUtil.HEALTH_LECTURE_CODE:
                setHealthLecture(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.LIVE_CODE:
                setLive(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.EPG_CODE:
                setEpg(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.VOD_CODE:
                setVod(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.MEDICAL_CODE:
                setMedical(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.HISPORTAL_CODE:
                setHisportal(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.AP_CODE:
                setAP(groupPosition, childPosition);
                break;
            case AnalyzeResultUtil.HEALTH_TV_APP_CODE:
                setHealthTvApp(groupPosition, childPosition);
                break;
            default:
                Log.i("TGH", "analyzeIntent: 没有对应的分类id");
                break;
        }
    }

    /***
     * 健康视频
     **/
    private void setHealthLecture(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryRatioMedicalGraph();
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryRatioMedicalChannelGraph();
                        break;
                    case C1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalGraphAll(getHeaders());
                        break;
                    case D1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalTvGraph(getHeaders(), getPlayType(), "", getQueryDate());
                        break;
                    case D2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalTvWeeklyGraph(getHeaders(), getPlayType(), "", getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S1:
                switch (childPosition.getId()) {
//                    case -1:
//                        AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalDaysGraph(1, getMedicalName(), getWeekly() + "");
//                        break;
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalGraph(getHeaders(), getRootChannelId());
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalTvGraph(getHeaders(), getPlayType(), getRootChannelId(), getQueryDate());
                        break;
                    case B2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalTvWeeklyGraph(getHeaders(), getPlayType(), getRootChannelId(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            default:
                Log.i("TGH", "setHospital: 没有对应的健康讲座的条目");
                break;
        }
    }

    /***
     * 直播
     **/
    private void setLive(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryRatioTvGraph();
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendTvGraph(getHeaders(), getOperatorCode());
                        break;
                    case C1://2.3.4直播频道日排行榜
                        AnalyzeResultUtil.getInstance(mContext).queryRankingTvGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                        break;
                    case C2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingTvWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S1:
                switch (childPosition.getId()) {
//                    case -1:
//                        AnalyzeResultUtil.getInstance(mContext).queryTrendTvDaysGraph(1, getMedicalName(), getWeekly() + "");
//                        break;
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendTvGraph(getHeaders(), getOperatorCode());
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingTvGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                        break;
                    case B2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingTvWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            default:
                Log.i("TGH", "setLive: 没有对应的直播条目");
                break;
        }

    }

    /***
     * 回看
     **/
    private void setEpg(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryRatioEventGraph();
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendEventGraph(getHeaders(), getOperatorCode());
                        break;
                    case C1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                        break;
                    case C2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                        break;
                    case D1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventEventGraph(getHeaders(), getPlayType(), getQueryDate());
                        break;
                    case D2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventEventWeeklyGraph(getHeaders(), getPlayType(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S1:
                switch (childPosition.getId()) {
//                    case -1:
//                        AnalyzeResultUtil.getInstance(mContext).queryTrendEventDaysGraph(1, getMedicalName(), getWeekly() + "");
//                        break;
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendEventGraph(getHeaders(), getOperatorCode());
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                        break;
                    case B2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                        break;
                    case C1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventEventGraph(getHeaders(), getPlayType(), getQueryDate());
                        break;
                    case C2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingEventEventWeeklyGraph(getHeaders(), getPlayType(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            default:
                Log.i("TGH", "setEpg: 没有对应的回看条目");
                break;
        }

    }

    /***
     * 点播
     **/
    private void setVod(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryRatioVodGraph();
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendVodGraph(getHeaders(), getOperatorCode());
                        break;
                    case C1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingVodGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                        break;
                    case C2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingVodWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S1:
                switch (childPosition.getId()) {
//                    case -1:
//                        AnalyzeResultUtil.getInstance(mContext).queryTrendVodDaysGraph(1, getMedicalName(), getWeekly() + "");
//                        break;
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryTrendVodGraph(getHeaders(), getOperatorCode());
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingVodGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                        break;
                    case B2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingVodWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            default:
                Log.i("TGH", "setVod: 没有对应的点播条目");
                break;
        }

    }

    /***
     * 新兴业务
     **/
    private void setMedical(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingHealthDetailGraph(getHeaders(), getEn(), getQueryDate());
                        break;
                    case A2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingHealthDetailWeeklyGraph(getHeaders(), getEn(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
//            case S1: // 模块详情
//                switch (childPosition.getId()) {
//                    case 0:
//                        AnalyzeResultUtil.getInstance(mContext).queryRankingHealthDetailGraph(getEn(), getQueryDate());
//                        break;
//                    case 1:
//                        AnalyzeResultUtil.getInstance(mContext).queryRankingHealthDetailWeeklyGraph(getEn(), getWeekly() + "");
//                        break;
//                    default:
//                        showNoData();
//                        break;
//                }
//                break;
//            case 2: // 医生相关
//                switch (childPosition.getId()) {
//                    case 0:
//                        AnalyzeResultUtil.getInstance(mContext).queryRankingDoctorGraph(getEn(), getQueryDate());
//                        break;
//                    case 1:
//                        AnalyzeResultUtil.getInstance(mContext).queryRankingDoctorWeeklyGraph(getEn(), getWeekly() + "");
//                        break;
//                    default:
//                        showNoData();
//                        break;
//                }
//                break;
            default:
                Log.i("TGH", "setMedical: 没有对应医疗条目");
                break;
        }

    }

    /**
     * 无线认证
     */
    private void setHisportal(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0://总览
                switch (childPosition.getId()) {
                    case A1://"A1", "无线认证日走势图"
                        showToast();
                        break;
                    case B1://"B1", "公众号用户"
                        showToast();
                        break;
                    case C1://"C1", "微信用户分布情况"
                        showToast();
                        break;
                    case D1://"D1", "昨日新增用户占比图"
                        showToast();
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S1://报表
                switch (childPosition.getId()) {
                    case A1://"A1", "无线认证总览列表"
                        showToast();
                        break;
                    case B1://"B1", "公众号用户总览列表"
                        showToast();
                        break;
                    case C1://"C1", "无线认证按医院列表"
                        showToast();
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S2://AP
                switch (childPosition.getId()) {
                    case A1://"A1", "AP日排行榜"
                        AnalyzeResultUtil.getInstance(mContext).queryRankingApGraph(getInstanceCode(), getAphot());
                        break;
                    case A2://"B1", "AP周排行榜"
                        AnalyzeResultUtil.getInstance(mContext).queryRankingApWeeklyGraph(getInstanceCode(), getQueryDate());
                        break;
                    case B1:// "C1", "AP日走势图"
                        AnalyzeResultUtil.getInstance(mContext).queryTrendApGraph(getInstanceCode(), getApmac());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            case S3://"AP管理"
                showToast();
                break;
            case S4://"按医院"
                switch (childPosition.getId()) {
                    case A1://2.5.18无线认证按医院走势图
//                        AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaGraph(getViewType(), getInstanceCode(), getTimeRange());
                        break;
                    case A2://2.5.19无线认证按医院比较走势图
                        AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaGraphCompare(getViewType(), getInstanceCode(), getCompareDate1(), getCompareDate2());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            default:
                Log.i("TGH", "setVod: 没有对应的医院条目");
                break;
        }
    }


    /**
     * 医院业务
     */
    private void setAP(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalNavigationGraph(getHeaders(), getEn(), getQueryDate());
                        break;
                    case A2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalNavigationWeeklyGraph(getHeaders(), getEn(), getWeekly());
                        break;
                    case B1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingHospitalInfoGraph(getHeaders(), getEn(), getQueryDate());
                        break;
                    case B2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingHospitalInfoWeeklyGraph(getEn(), getWeekly());
                        break;
                    case C1:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingDoctorGraph(getHeaders(), getEn(), getQueryDate());
                        break;
                    case C2:
                        AnalyzeResultUtil.getInstance(mContext).queryRankingDoctorWeeklyGraph(getHeaders(), getEn(), getWeekly());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
//            case S1:
//                switch (childPosition.getId()) {
//                    case 0:
//                        showToast();
//                        break;
//                    default:
//                        showNoData();
//                        break;
//                }
//                break;
//            case S2:
//                switch (childPosition.getId()) {
//                    case 0:
//                        showToast();
//                        break;
//                    default:
//                        showNoData();
//                        break;
//                }
//                break;
            default:
                Log.i("TGH", "setVod: 没有对应的AP条目");
                break;
        }
    }

    /**
     * 健康电视APP
     */
    private void setHealthTvApp(MenuInfo groupPosition, MenuInfo childPosition) {
        switch (groupPosition.getId()) {
            case S0:
                switch (childPosition.getId()) {
                    case A1://"A1", "应用启动日走势图"
                        // TODO: 2016/12/26 需要修改，暂时注释掉 
                        AnalyzeResultUtil.getInstance(mContext).queryTrendAppStartupDayGraph(getHeaders());
                        break;
                    case B1://"B1", "首页模块统计画面"
                        // TODO: 2016/12/26 需要修改，暂时注释掉 
                        AnalyzeResultUtil.getInstance(mContext).queryMedicalHealthGraph(getHeaders(), getHealthId());
                        break;
                    default:
                        showNoData();
                        break;
                }
                break;
            default:
                Log.i("TGH", "setVod: 没有对应的健康电视APP条目");
                break;
        }
    }

    private static final String T1S1B2 = AnalyzeResultUtil.HEALTH_LECTURE_CODE + S1 + B2;
    private static final String T2S0C2 = AnalyzeResultUtil.LIVE_CODE + S0 + C2;
    private static final String T2S1B2 = AnalyzeResultUtil.LIVE_CODE + S1 + B2;
    private static final String T3S0C2 = AnalyzeResultUtil.EPG_CODE + S0 + C2;
    private static final String T3S0D2 = AnalyzeResultUtil.EPG_CODE + S0 + D2;
    private static final String T3S1B2 = AnalyzeResultUtil.EPG_CODE + S1 + B2;
    private static final String T3S1C2 = AnalyzeResultUtil.EPG_CODE + S1 + C2;
    private static final String T4S0C2 = AnalyzeResultUtil.VOD_CODE + S0 + C2;
    private static final String T4S0A2 = AnalyzeResultUtil.VOD_CODE + S0 + A2;
    private static final String T4S1B2 = AnalyzeResultUtil.VOD_CODE + S1 + B2;
    private static final String T5S0A2 = AnalyzeResultUtil.MEDICAL_CODE + S0 + A2;
    private static final String T6S0A2 = AnalyzeResultUtil.AP_CODE + S0 + A2;
    private static final String T6S0B2 = AnalyzeResultUtil.AP_CODE + S0 + B2;
    private static final String T6S0C2 = AnalyzeResultUtil.AP_CODE + S0 + C2;


    /**
     * 匹配周排行榜中点击每个小柱子弹折线图框的方法
     */
    public void smallLineChart(MenuInfo arg, MenuInfo groupPosition, MenuInfo childPosition) {
        if (arg != null && groupPosition != null && childPosition != null) {
            String id = arg.getId() + groupPosition.getId() + childPosition.getId();
            switch (id) {
                case T1S1B2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalDaysGraph(getHeaders(), getPlayType(), getMedicalName(), getRootChannelId(), getWeekly());
                    break;
                case T2S0C2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendTvDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T2S1B2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendTvDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T3S0C2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendEventDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T3S0D2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendEventEventDaysGraph(getHeaders(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T3S1B2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendEventDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T3S1C2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendEventEventDaysGraph(getHeaders(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T4S0C2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendVodDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T4S0A2://暂时没有对应的接口
                    //showNoData();
                    break;
                case T4S1B2:
                    AnalyzeResultUtil.getInstance(mContext).queryTrendVodDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                    break;
                case T5S0A2://暂时没有对应的接口
                    //showNoData();
                    break;
                case T6S0A2://暂时没有对应接口
                    //showNoData();
                    break;
                case T6S0B2:
                    //showNoData();
                    break;
                case T6S0C2:
                    //showNoData();
                    break;
                default:
                    Log.i("TGH", "setVod: 没有对应的健康电视APP条目");
                    break;
            }
        }
    }

    /**
     * 调用图表接口
     *
     * @param itemID item全ID
     * @return true已处理  false没有此ID
     */
    public boolean callChartInterface(String itemID, String chartType) {
        iconType = chartType;
        switch (itemID) {
            // 健康视频
            /**
             * 2.2.1健康视频地区占比图
             * 查询健康视频地区占比图显示数据。
             **/
            case "T0S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioMedicalGraph();
                break;
            /**
             * 2.2.2健康视频频道占比图
             * 查询健康视频频道占比图显示数据。
             **/
            case "T0S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioMedicalChannelGraph();
                break;
            /**
             * 2.2.3 健康视频总览日走势图
             * 功能描述：查询健康视频总览日走势图
             */
            case "T0S0C1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalGraphAll(getHeaders());
                break;
            /**
             * 2.2.4 健康视频频道统计日走势图
             * 查询健康视频日走势图。
             **/
            case "T0S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalGraph(getHeaders(), getRootChannelId());
                break;
            /**
             * 2.2.5健康视频日排行榜
             * 查询健康视频日排行榜排行榜Top20。
             **/
            case "T0S0D1":
            case "T0S1B1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalTvGraph(getHeaders(), getPlayType(), getRootChannelId(), getQueryDate());
                break;
            /**
             * 2.2.6健康视频周排行榜
             * 查询健康视频周排行榜Top20。
             **/
            case "T0S0D2":
            case "T0S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalTvWeeklyGraph(getHeaders(), getPlayType(), getRootChannelId(), getWeekly());
                break;
            // 娱乐视频
            /**
             * 2.3.1直播地区占比图
             * 查询直播地区占比图显示数据。
             **/
            case "T1S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioTvGraph();
                break;
            /**
             * 2.3.2直播频道日走势图
             * 查询直播频道日走势图。
             * 新添加参数  ：operatorCode	String	运营商编码（总览时为空）
             **/
            case "T1S0B1":
            case "T1S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendTvGraph(getHeaders(), getOperatorCode());
                break;
            /**
             * 2.3.3直播频道日排行榜
             * 查询直播频道日排行榜Top20。
             **/
            case "T1S0C1":
            case "T1S1B1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingTvGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                break;
            /**
             * 2.3.4直播频道周排行榜
             * 查询直播频道周排行榜Top20。
             **/
            case "T1S0C2":
            case "T1S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingTvWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                break;
            /**
             * 2.3.6回看地区占比图
             * 查询回看地区占比图显示数据。
             **/
            case "T2S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioEventGraph();
                break;
            /**
             * 2.3.7回看频道日走势图
             * 查询所有回看频道每日的点击次数和播放时长。
             **/
            case "T2S0B1":
            case "T2S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendEventGraph(getHeaders(), getOperatorCode());
                break;
            /**
             * 2.3.8回看频道日排行榜
             * 查询所有回看频道每日点击次数或收视时长排行榜Top20。
             **/
            case "T2S0C1":
            case "T2S1B1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingEventGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                break;
            /**
             * 2.3.9回看频道周排行榜
             * 查询所有回看频道每周点击次数或收视时长排行榜Top20。
             **/
            case "T2S0C2":
            case "T2S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingEventWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                break;
            /**
             * 2.3.11回看节目日排行榜
             * 查询所有回看节目每日点击次数或收视时长排行榜Top20。
             **/
            case "T2S0D1":
            case "T2S1C1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingEventEventGraph(getHeaders(), getPlayType(), getQueryDate());
                break;
            /**
             * 2.3.12回看节目周排行榜
             * 查询所有回看节目每周点击次数或收视时长排行榜Top20。
             **/
            case "T2S0D2":
            case "T2S1C2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingEventEventWeeklyGraph(getHeaders(), getPlayType(), getWeekly());
                break;
            /**
             * 2.3.14点播地区占比图
             * 查询点播地区占比图显示数据。
             **/
            case "T3S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioVodGraph();
                break;
            /**
             * 2.3.15回看节目折线图
             * 查询回看节目日折线图。
             **/
            case "T3S0B1":
            case "T3S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendVodGraph(getHeaders(), getOperatorCode());
                break;
            /**
             * 2.3.16点播节目日排行榜
             * 访问路径：/rankingvod/rankingVod/queryRankingVodGraph。
             **/
            case "T3S0C1":
            case "T3S1B1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingVodGraph(getHeaders(), getOperatorCode(), getPlayType(), getQueryDate());
                break;
            /**
             * 2.3.17点播节目周排行榜
             * 查询所有点播节目每周点击次数或收视时长排行榜Top20。
             **/
            case "T3S0C2":
            case "T3S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingVodWeeklyGraph(getHeaders(), getOperatorCode(), getPlayType(), getWeekly());
                break;
            // 新兴业务
            /**
             * 2.4.1养生保健日排行榜
             * 查询养生保健日排行榜Top20。
             **/
            case "T4S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingHealthDetailGraph(getHeaders(), getEn(), getQueryDate());
                break;
            /**
             * 2.4.2养生保健周排行榜
             * 查询养生保健周排行榜Top20。
             **/
            case "T4S0A2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingHealthDetailWeeklyGraph(getHeaders(), getEn(), getWeekly());
                break;
            // 无线认证
            /**
             * 2.5.4 总览微信用户分布图
             * 功能描述：查询微信用户分布情况统计数据
             */
            case "T5S0C1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioWeChatUserGraph(getPlayDay());
                break;
            /**
             * 2.5.5 总览各医院日关注用户数占比图
             * 功能描述：查询总览各医院日关注用户占比图数据
             */
            case "T5S0D1":
                AnalyzeResultUtil.getInstance(mContext).queryRatioAttentionUserWxGraph();
                break;
            /**
             * 2.5.6AP日排行榜
             * 查询AP日排行榜Top20。
             * 访问路径改变为:/rankingap/rankingAp/queryRankingApGraph
             **/
            case "T5S3A1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingApGraph(getInstanceCode(), getQueryDate());
                break;
            /**
             * 2.5.7 AP周排行榜
             * 功能描述：查询AP周排行榜Top20
             */
            case "T5S3A2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingApWeeklyGraph(getInstanceCode(), getWeekly());
                break;
            /**
             * 2.5.8AP日走势图
             * 查询AP日走势图数据。
             * 访问路径改变为:/rankingap/rankingAp/queryTrendApGraph
             **/
            case "T5S3B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendApGraph(getInstanceCode(), getApmac());
                break;
            /**
             * 2.5.10无线认证总览走势图
             * 功能描述：查询无线认证总览走势图数据。
             * timeRange	String	时间间隔（28天，14天，7天）
             **/
            case "T5S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayGraph(getTimeRange());
                break;
            /**
             * 2.5.13公众号用户总览走势图
             * 功能描述：查询公众号用户总览走势图数据。
             * timeRange	String	时间间隔（28天，14天，7天）
             * viewType	String	显示内容区分（0：新关注人数 1：取消关注人数 2：净增关注人数 3：累计关注人数）
             **/
            case "T5S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendUserWxGraph(getViewType(), getTimeRange());
                break;
            /**
             * 2.5.18无线认证按医院走势图
             * 功能描述：查询无线认证按医院走势图数据。
             * 输入参数：
             * viewType	String	显示内容区分（0：认证设备数 1：放行设备数 2：新增设备数 3：新增用户数）
             * instanceCode	String	医院编码
             * timeRange	String	时间间隔（28天，14天，7天）
             **/
            case "T5S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaGraph(getViewType(), getInstanceCode(), getTimeRange());
                break;
            // 医院业务
            /**
             * 2.6.1就诊导航日排行榜
             * 查询就诊导航日排行榜Top20。
             **/
            case "T6S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalNavigationGraph(getHeaders(), getEn(), getQueryDate());
                break;
            /**
             * 2.6.2就诊导航周排行榜
             * 查询就诊导航周排行榜Top20。
             **/
            case "T6S0A2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingMedicalNavigationWeeklyGraph(getHeaders(), getEn(), getWeekly());
                break;
            /**
             * 2.6.3医院资讯日排行榜
             * 查询医院资讯日排行榜Top20。
             **/
            case "T6S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingHospitalInfoGraph(getHeaders(), getEn(), getQueryDate());
                break;
            /**
             * 2.6.4医院资讯周排行榜
             * 查询医院资讯周排行榜Top20。
             **/
            case "T6S0B2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingHospitalInfoWeeklyGraph(getEn(), getWeekly());
                break;
            /**
             * 2.6.5出诊专家日排行榜
             * 查询出诊专家日排行榜Top20。
             **/
            case "T6S0C1":
                AnalyzeResultUtil.getInstance(mContext).queryRankingDoctorGraph(getHeaders(), getEn(), getQueryDate());
                break;
            /**
             * 2.6.6出诊专家周排行榜
             * 查询出诊专家周排行榜Top20。
             **/
            case "T6S0C2":
                AnalyzeResultUtil.getInstance(mContext).queryRankingDoctorWeeklyGraph(getHeaders(), getEn(), getWeekly());
                break;
            // 健康电视APP
            /**
             * 2.7.1启动次数走势图
             * 查询每日健康电视APP走势图数据。
             **/
            case "T7S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAppStartupDayGraph(getHeaders());
                break;
            /**
             * 2.7.2首页模块走势图
             * 查询首页模块每个模块的日走势图（人数/次数）。
             **/
            case "T7S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryMedicalHealthGraph(getHeaders(), getHealthId());
                break;
            default:
                return false;
        }
        Log.i("Main","图表接口   callChartInterface()   itemID:" + itemID);
        return true;
    }

    /**
     * 调用比较走势图的接口
     *
     * @param itemID item全ID
     * @return true已处理  false没有此ID
     */
    public boolean callCompareChartInterface(String itemID) {
        switch (itemID) {
            /**
             * 2.5.14公众号用户总览比较走势图
             * 功能描述：查询公众号用户总览比较走势图数据
             * viewType	String	显示内容区分（0：新关注人数 1：取消关注人数 2：净增关注人数 3：累计关注人数）
             * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
             * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
             **/
            case "T5S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendUserWxGraphCompare(getViewType(), getCompareDate1(), getCompareDate2());
                break;
            /**
             * 2.5.19无线认证按医院比较走势图
             * 功能描述：查询无线认证按医院比较走势图数据。
             * 输入参数：
             * viewType	String	显示内容区分（0：认证设备数 1：放行设备数 2：新增设备数 3：新增用户数）
             * instanceCode	String	医院编码
             * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
             * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
             **/
            case "T5S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaGraphCompare(getViewType(), getInstanceCode(), getCompareDate1(), getCompareDate2());
                break;
            default:
                return false;
        }
        Log.i("Main","比较走势图的接口    callCompareChartInterface()   itemID:" + itemID);
        return true;
    }

    /**
     * 调用各查询条件列表接口
     *
     * @param itemID              item全ID
     * @param mDataFilterListener 接口监听
     * @return true已处理  false没有此ID
     */
    public boolean callQueryFilterListInterface(String itemID, DataFilterListener mDataFilterListener) {
        switch (itemID) {
            // 各画面查询条件下拉框取得
            /**
             * 2.8.1获取健康视频总览排行榜的查询条件列表
             * 获取健康视频总览排行榜的查询条件列表
             **/
            case "T0S0D1":
            case "T0S0D2":
                AnalyzeResultUtil.getInstance(mContext).queryMedicalTvOverviewRankingSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.2获取健康视频走势图的查询条件列表
             * 获取健康视频走势图的查询条件列表。
             **/
//            case "T0S0C1":
            case "T0S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryMedicalTvTrendSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.3获取健康视频排行榜的查询条件列表
             * 获取健康视频排行榜的查询条件列表。
             **/
            case "T0S1B1":
            case "T0S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryMedicalTvRankingSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.4获取娱乐视频总览排行榜的查询条件列表
             * 获取娱乐视频总览排行榜的查询条件列表。
             **/
            case "T1S0C1":
            case "T1S0C2":
            case "T2S0C1":
            case "T2S0C2":
            case "T2S0D1":
            case "T2S0D2":
            case "T3S0C1":
            case "T3S0C2":
                AnalyzeResultUtil.getInstance(mContext).queryTvRankingOverviewSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.5获取娱乐视频各地区排行榜的查询条件列表
             * 获取娱乐视频各地区排行榜的查询条件列表。
             **/
            case "T1S1B1":
            case "T1S1B2":
            case "T2S1B1":
            case "T2S1B2":
            case "T2S1C1":
            case "T2S1C2":
            case "T3S1B1":
            case "T3S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryTvRankingSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.6获取娱乐视频各地区走势图的查询条件列表
             **/
//            case "T1S0B1":
            case "T1S1A1":
//            case "T2S0B1":
            case "T2S1A1":
//            case "T3S0B1":
            case "T3S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTvTrendSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.7获取就诊导航的查询条件列表
             **/
            case "T6S0A1":
            case "T6S0A2":
                AnalyzeResultUtil.getInstance(mContext).queryMedicalNavigationSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.8获取出诊专家的查询条件列表
             **/
            case "T6S0C1":
            case "T6S0C2":
                AnalyzeResultUtil.getInstance(mContext).queryExpertSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.9获取健康电视APP首页模块的查询条件列表
             **/
            case "T7S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryHomePageModuleSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.10获取AP排行榜的查询条件列表
             **/
            case "T5S3A1":
            case "T5S3A2":
                AnalyzeResultUtil.getInstance(mContext).queryAPDayRankingSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.11获取AP日走势图的查询条件列表
             **/
            case "T5S3B1":
                AnalyzeResultUtil.getInstance(mContext).queryHeatTrendSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.12获取无线认证总览的查询条件列表
             * 功能描述：获取无线认证总览的查询条件列表。
             **/
            case "T5S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDaySelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.13获取公众号用户总览的查询条件列表
             * 功能描述：获取公众号用户总览的查询条件列表。
             **/
            case "T5S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendUserWxSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.14获取无线认证安医院的查询条件列表
             * 功能描述：获取无线认证安医院的查询条件列表。
             **/
            case "T5S1A1":
            case "T5S1B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaSelCondList(mDataFilterListener);
                break;
            /**
             * 2.8.15 获取报表无线认证安医院的查询条件列表
             * 功能描述：获取报表无线认证安医院的查询条件列表
             */
            case "T5S2C1":
                AnalyzeResultUtil.getInstance(mContext).queryExcelTrendAuthDayAreaSelCondList(mDataFilterListener);
                break;
            default:
                return false;
        }
        Log.i("Main","筛选条件列表接口    callQueryFilterListInterface()   itemID:" + itemID);
        return true;
    }

    /**
     * 调用查询昨日关键指标信息接口
     *
     * @param itemID       item全ID
     * @param listListener 接口监听
     * @return true已处理  false没有此ID
     */
    public boolean callQueryYestDayInfoInterface(String itemID, ListListener listListener) {
        switch (itemID) {
            /**
             * 2.5.9无线认证总览昨日关键指标信息
             * 功能描述：查询无线认证总览昨日关键指标信息。
             **/
            case "T5S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayYestDayInfo(listListener);
                break;
            /**
             * 2.5.12公众号用户总览昨日关键指标信息
             * 功能描述：查询公众号用户总览昨日关键指标信息。
             **/
            case "T5S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendUserWxYestDayInfo(listListener);
                break;
            /**
             * 2.5.17无线认证按医院昨日关键指标信息
             * 功能描述：查询无线认证安医院昨日关键指标信息。
             * 输入参数：
             * instanceCode	String	医院编码
             **/
            case "T5S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaYestDayInfo(getInstanceCode(), listListener);
                break;
            default:
                return false;
        }
        Log.i("Main","昨日关键指标信息接口    callQueryYestDayInfoInterface()   itemID:" + itemID);
        return true;
    }

    /**
     * 调用查询数据列表接口
     *
     * @param itemID       item全ID
     * @param listListener 接口监听
     * @return true已处理  false没有此ID
     */
    public boolean callQueryDateListInterface(String itemID, ListListener listListener) {
        switch (itemID) {
            /**
             * 2.5.1 报表无线认证总览
             * 功能描述：查询无线认证总览计统计数据
             */
            case "T5S2A1":
                AnalyzeResultUtil.getInstance(mContext).querytDAuthDayList(getPlayDay(), listListener);
                break;
            /**
             * 2.5.2 报表公众号用户总览
             * 功能描述：查询微信用户统计数据
             */
            case "T5S2B1":
                AnalyzeResultUtil.getInstance(mContext).queryUserWxDayList(getPlayDay(), listListener);
                break;
            /**
             * 2.5.3 报表无线认证按医院
             * 功能描述：查询无线认证按医院计统计数据
             */
            case "T5S2C1":
                AnalyzeResultUtil.getInstance(mContext).querytDAuthDayListArea(getInstanceCode(), getPlayDay(), listListener);
                break;
            /**
             * 2.5.11无线认证总览数据列表
             * 功能描述：查询无线认证总览数据列表。
             * timeRange	String	时间间隔（28天，14天，7天）
             **/
            case "T5S0A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayList(getTimeRange(), listListener);
                break;
            /**
             * 2.5.15公众号用户总览数据列表
             * 功能描述：查询公众号用户总览数据列表。
             * 输入参数：
             * timeRange	String	时间间隔（28天，14天，7天）
             **/
            case "T5S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendUserWxList(getTimeRange(), listListener);
                break;
            /**
             * 2.5.20无线认证按医院数据列表
             * 功能描述：查询无线认证按医院数据列表。
             * 输入参数：
             * instanceCode	String	医院编码
             * timeRange	String	时间间隔（28天，14天，7天）
             **/
            case "T5S1A1":
            case "T5S1B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaList(getInstanceCode(), getTimeRange(), listListener);
                break;
            default:
                return false;
        }
        Log.i("Main","数据列表接口   callQueryDateListInterface()   itemID:" + itemID);
        return true;
    }

    /**
     * 调用查询比较数据列表接口
     *
     * @param itemID       item全ID
     * @param listListener 接口监听
     * @return true已处理  false没有此ID
     */
    public boolean callQueryCompareDateListInterface(String itemID, ListListener listListener) {
        switch (itemID) {
            /**
             * 2.5.16公众号用户总览比较数据列表
             * 功能描述：查询公众号用户总览比较数据列表。
             * 输入参数：
             * viewType	String	显示内容区分（0：新关注人数 1：取消关注人数 2：净增关注人数 3：累计关注人数）
             * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
             * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
             **/
            case "T5S0B1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendUserWxCompareList(getCompareDate1(), getCompareDate2(), listListener);
                break;
            /**
             * 2.5.21无线认证按医院比较数据列表
             * 功能描述：查询无线认证按医院比较数据列表。
             * 输入参数：
             * instanceCode	String	医院编码
             * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
             * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
             **/
            case "T5S1A1":
                AnalyzeResultUtil.getInstance(mContext).queryTrendAuthDayAreaCompareList(getInstanceCode(), getCompareDate1(), getCompareDate2(), listListener);
                break;
            default:
                return false;
        }
        Log.i("Main","比较数据列表接口   callQueryCompareDateListInterface()   itemID:" + itemID);
        return true;
    }

    /**
     * 调用小折线图接口
     *
     * @param itemID item全ID
     * @return true已处理  false没有此ID
     */
    public boolean callSmallLineChart(String itemID) {
        switch (itemID) {
            /**
             * 2.2.7健康视频折线图
             * 查询健康视频日折线图。
             **/
            case "T0S0D2":
            case "T0S0B2":
                AnalyzeResultUtil.getInstance(mContext).queryTrendMedicalDaysGraph(getHeaders(), getPlayType(), getMedicalName(), getRootChannelId(), getWeekly());
                break;
            /**
             * 2.3.5直播频道折线图
             * 查询直播频道日折线图。
             **/
            case "T1S0C2":
            case "T1S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryTrendTvDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                break;
            /**
             * 2.3.10回看频道折线图
             * 查询回看频道日折线图。
             **/
            case "T2S0C2":
            case "T2S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryTrendEventDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                break;
            /**
             * 2.3.13回看节目折线图
             * 查询回看节目日折线图。
             **/
            case "T2S0D2":
            case "T2S1C2":
                AnalyzeResultUtil.getInstance(mContext).queryTrendEventEventDaysGraph(getHeaders(), getPlayType(), getMedicalName(), getWeekly());
                break;
            /**
             * 2.3.18点播节目折线图
             * 查询点播节目日折线图。
             **/
            case "T3S0C2":
            case "T3S1B2":
                AnalyzeResultUtil.getInstance(mContext).queryTrendVodDaysGraph(getHeaders(), getOperatorCode(), getPlayType(), getMedicalName(), getWeekly());
                break;
            default:
                return false;
        }
        Log.i("Main","小折线图接口   callSmallLineChart()   itemID:" + itemID);
        return true;
    }

    private void showToast() {
        Toast.makeText(mContext, "没有对应接口", Toast.LENGTH_LONG).show();
        showNoData();
    }

    /**
     * 展示没有数据的界面
     **/
    private void showNoData() {
        AnalyzeResultObservable.getInstance().onAnalyzeResult(-1, "", null);
    }

    /**
     * @return 格式化后的字符串时间
     */
    public String getTimeStr() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYY_MM_DD);
        return simpleDateFormat.format(new Date());
    }

    /**
     * 获取当天是本年的第几周
     **/
    public int getTimeWeek() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        return c.get(Calendar.WEEK_OF_YEAR);
    }


}
