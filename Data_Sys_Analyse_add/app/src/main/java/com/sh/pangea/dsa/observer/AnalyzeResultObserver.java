package com.sh.pangea.dsa.observer;


import com.sh.pangea.dsa.bean.AnalyzeResult;

public interface AnalyzeResultObserver {
    /**
     * 用于通知ui层接口调用的结果
     **/
    void onAnalyzeResult(int code, String error, AnalyzeResult analyzeResult);
}
