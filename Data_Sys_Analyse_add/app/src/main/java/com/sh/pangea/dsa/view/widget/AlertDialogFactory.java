package com.sh.pangea.dsa.view.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.view.Window;

import com.sh.pangea.dsa.Utils.OnCustomDialogListener;
import com.sh.pangea.dsa.view.DialogView;


public class AlertDialogFactory {
	private static AlertDialogFactory instance;
	
	public static AlertDialogFactory getInstance() {
		if (instance == null) {
			instance = new AlertDialogFactory();
		}
		return instance;
	}
	
	public AlertDialogFactory() {
	}
	
	/**
	 * 
	 * @param context 上下文
	 * @param promtp   提示语
	 * @param isSingButton 是否单个按钮
	 * @param listener 监听器
	 * @return
	 */
	public AlertDialog createAlertDialog(Context context, String promtp, boolean isSingButton,
										 OnCustomDialogListener listener) {
		return createAlertDialog(context,promtp, "", "", isSingButton, listener);
	}
	/**
	 * 
	 * @param context 上下文
	 * @param promtp   提示语
	 * @param single 单个按钮文字
	 * @param listener 监听器
	 * @return
	 */
	public AlertDialog createAlertDialog(Context context, String promtp, String single,
										 OnCustomDialogListener listener) {
		return createAlertDialog(context,promtp, "", single, true, listener);
	}
	/**
	 * @param context 上下文
	 * @param promtp   提示语
	 * @param cacel 左按钮文字
	 * @param confirm 右按钮文字
	 * @param isSingleButton 是否是单个按钮
	 * @param listener 监听器
	 * @return
	 */
	public AlertDialog createAlertDialog(Context context, String promtp, String cacel, String confirm,
										 boolean isSingleButton, OnCustomDialogListener listener) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.show();
		alertDialog.setCancelable(false);
		alertDialog.setCanceledOnTouchOutside(false);
		Window window = alertDialog.getWindow();
		DialogView dialogView = new DialogView(context);
		dialogView.creatDialog(promtp, cacel, confirm, isSingleButton, listener);
		dialogView.setDialog(alertDialog);
		window.setContentView(dialogView);
		return alertDialog;
	}
	
}
