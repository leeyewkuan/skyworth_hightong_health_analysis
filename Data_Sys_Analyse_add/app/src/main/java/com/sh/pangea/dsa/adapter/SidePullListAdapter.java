package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Data_Sys_Analyse
 * Created by LB on 2016/10/13,10:04
 */

public class SidePullListAdapter extends BaseAdapter {

    private  List<String> list;
    private Context mContext;

    public SidePullListAdapter(List<String> list, Context mContext)
    {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        int number = 0;
        if(list != null){
            number = list.size();
            Log.i("number",""+number);
        }
        return number;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(parent.getContext(), R.layout.side_pull_item,null);
            viewHolder = new ViewHolder();
            viewHolder.tv = (TextView) convertView.findViewById(R.id.tv_side_pull);
            viewHolder.iv = (ImageView) convertView.findViewById(R.id.iv_side_pull);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
//        viewHolder.tv.setText(moduleItem.getName());
        viewHolder.tv.setText(list.get(position));
        return convertView;
    }

    private static class ViewHolder{
        TextView tv;
        ImageView iv;
    }

}
