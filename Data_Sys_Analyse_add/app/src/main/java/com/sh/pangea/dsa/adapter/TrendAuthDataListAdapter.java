package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.view.StretchPanel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuqiwu on 2017/2/10.
 * 无线认证中的数据列表的适配器
 */

public class TrendAuthDataListAdapter extends RecyclerView.Adapter<TrendAuthDataListAdapter.ViewHolder> {
    /**
     * 数据列表返回数据的日期集合
     */
    private ArrayList<String> groupTime = new ArrayList<>();
    /**
     * 数据列表返回数据的集合
     * key:日期   value:其他数据
     */
    private Map<String, List<String>> childDataMap = new HashMap<>();
    private Context mContext;

    public TrendAuthDataListAdapter(ArrayList<String> groupTime, Map<String, List<String>> childDataMap, Context mContext) {
        this.groupTime.addAll(groupTime);
        this.childDataMap = childDataMap;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.stretch_panel, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        View contentView = View.inflate(mContext, R.layout.stretch_panel_content_view, null);
        View stretchView = View.inflate(mContext, R.layout.stretch_panel_stretch_view, null);
        holder.stretchPanel.setContentView(contentView);
        holder.stretchPanel.setStretchView(stretchView);
        holder.stretchPanel.setHandleClikeEventOnThis(contentView);
        holder.stretchPanel.setStretchAnimationDuration(150);

        if (position % 2 != 0) {
            contentView.setBackgroundResource(R.color.colorTextInvert);
        } else {
            contentView.setBackgroundResource(R.color.transparent_white);
        }

        TextView content_text = (TextView) contentView.findViewById(R.id.stretch_panel_content_text);
        final ImageView content_img = (ImageView) contentView.findViewById(R.id.stretch_panel_content_img);
        TextView stretch_text = (TextView) stretchView.findViewById(R.id.stretch_panel_stretch_text);
        content_text.setText(groupTime.get(position));
        List<String> list = childDataMap.get(groupTime.get(position));
        String dataContent = "";
        for (int i = 0; i < list.size(); i++) {
            dataContent += list.get(i);
            if (i < list.size() - 1) {
                dataContent += "\n";
            }
        }
        stretch_text.setText(dataContent);
        holder.stretchPanel.setStretchViewOpened(false);
        holder.stretchPanel.setOnStretchListener(new StretchPanel.OnStretchListener() {

            @Override
            public void onStretchFinished(boolean isOpened) {
//                holder.stretchPanel.setStretchViewOpened(!isOpened);
                if (isOpened){
                    content_img.setImageResource(R.mipmap.arrows_down);
//                    holder.stretchPanel.openStretchView();
                } else{
                    content_img.setImageResource(R.mipmap.arrows_right);
//                    holder.stretchPanel.closeStretchView();
                }
//                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupTime.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final StretchPanel stretchPanel;

        public ViewHolder(View view) {
            super(view);
            stretchPanel = (StretchPanel) view.findViewById(R.id.stretch_panel_layout);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public void refresh() {
        this.notifyDataSetChanged();
    }
}
