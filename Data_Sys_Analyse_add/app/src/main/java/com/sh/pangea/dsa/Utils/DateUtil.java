package com.sh.pangea.dsa.Utils;

import android.text.TextUtils;
import android.util.Log;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DateUtil {
    public static final String YYYY_MM_DD0HH0MM0SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD0HH0MM = "yyyy-MM-dd HH:mm";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MM_DD2 = "yyyy:MM:dd";
    public static final String MMDD = "MM月dd日";
    public static final String HH_MM = "HH:mm";
    /**
     * 一天多少毫秒
     */
    private static final long MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;

    /**
     * String转成String
     *
     * @param time
     * @param format
     * @return
     */
    public static String getTimeStr(String time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        if (TextUtils.isEmpty(time)) {
            return null;
        }
        try {
            return simpleDateFormat.format(simpleDateFormat.parse(time))
                    .toString();
        } catch (ParseException e) {
            Log.w("",e.getLocalizedMessage());
        }
        return null;
    }

    /**
     * @param time   字符串的时间
     * @param format 字符串时间的格式
     * @return Date类型的时间
     */
    public static Date getTimeDate(String time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        if (TextUtils.isEmpty(time)) {
            return null;
        }
        try {
            return simpleDateFormat.parse(time);
        } catch (ParseException e) {
            Log.w("",e.getLocalizedMessage());
        }
        return null;
    }

    public static String getWeek(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("E");
        return df.format(date);
    }

    /**
     * @param time   Date类型时间
     * @param format 格式化的格式
     * @return 格式化后的字符串时间
     */
    public static String getTimeStr(Date time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        if (time == null) {
            return null;
        }
        return simpleDateFormat.format(time).toString();
    }

    public static String getDrationTime(int duration) {
        String durationTime = "";

        int h = duration / 3600;
        if (h < 10) {
            durationTime += "0" + h + ":";
        } else {
            durationTime += h + ":";
        }

        int m = duration % 3600 / 60;// 分钟数
        if (m < 10) {
            durationTime += "0" + m + ":";
        } else {
            durationTime += m + ":";
        }
        duration = duration % 60;
        int s = duration; // 秒数
        if (s < 10) {
            durationTime += "0" + s;
        } else {
            durationTime += s;
        }
        return durationTime;
    }

    public static String getBreakTime(int duration) {
        String durationTime = "";

        int h = duration / 3600;
        if (h < 10) {
            durationTime += "0" + h + "时";
        } else {
            durationTime += h + "时";
        }

        int m = duration % 3600 / 60;// 分钟数
        if (m < 10) {
            durationTime += "0" + m + "分";
        } else {
            durationTime += m + "分";
        }
        duration = duration % 60;
        int s = duration; // 秒数
        if (s < 10) {
            durationTime += "0" + s + "秒";
        } else {
            durationTime += s + "秒";
        }
        return durationTime;
    }

    public static String getPlaytime(int duration) {
        String durationTime = "";

        int h = duration / 3600;
        if (h < 10) {
            durationTime += "0" + h + "小时";
        } else if (h > 24) {
            int number = h / 24;
            if (h % 24 == 0) {
                durationTime = number + "天";
            } else {
                durationTime = number + 1 + "天";
            }
            return durationTime;
        } else {
            durationTime += h + "小时";
        }

        int m = duration % 3600 / 60;// 分钟数
        if (m < 10) {
            durationTime += "0" + m + "分";
        } else {
            durationTime += m + "分";
        }
        return durationTime;

    }

    /**
     * @param time   字符串的时间
     * @param format 字符串时间的格式
     * @return long类型的时间
     */
    public static long getTimeLong(String time, String format) {
        if (TextUtils.isEmpty(time)) {
            return 0;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(time).getTime();
        } catch (ParseException e) {
            Log.w("",e.getLocalizedMessage());
        }
        return 0;
    }

    /**
     * 获取当前日期
     */
    public static String getDate(int days) {
        Date d = new Date();
        SimpleDateFormat df = new SimpleDateFormat(YYYY_MM_DD);
        return df.format(new Date(d.getTime() + (days * MILLISECONDS_PER_DAY)));
    }

    public static boolean isToday(Date date) {
        boolean flag = false;
        if (date != null) {
            Calendar mCaledarStart = Calendar.getInstance(Locale.CHINA);
            mCaledarStart.set(Calendar.HOUR_OF_DAY, 0);
            mCaledarStart.set(Calendar.MINUTE, 0);
            mCaledarStart.set(Calendar.SECOND, 0);
            mCaledarStart.set(Calendar.MILLISECOND, 0);//注意毫秒值
            long startTime = mCaledarStart.getTimeInMillis();
            long judgeTime = date.getTime();
            /*String strJudgeTime = getTimeStr(date, YYYY_MM_DD0HH0MM0SS);
			String strStartTime = getTimeStr(mCaledarStart.getTime(), YYYY_MM_DD0HH0MM0SS);
			Logs.w(strJudgeTime + " | " +strStartTime);
			Logs.w(judgeTime + " | " +startTime);*/
            if (judgeTime >= startTime) {
                flag = true;
            }
        } else {
            Log.w("","d is null");
        }
        return flag;
    }
}
