package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.bean.Noun;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuqiwu on 2017/1/18.
 */

public class NounExplainAdapter extends BaseAdapter {
    private List<Noun> nouns = new ArrayList<>();
    private Context mContext;

    public NounExplainAdapter(Context mContext, List<Noun> nouns) {
        this.nouns = nouns;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return nouns.size();
    }

    @Override
    public Object getItem(int position) {
        return nouns.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_noun_explanation, null);
            viewHolder.noun = (TextView) convertView.findViewById(R.id.item_noun_noun);
            viewHolder.explain = (TextView) convertView.findViewById(R.id.item_noun_explain);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Noun noun = nouns.get(position);
        if (noun != null) {
            viewHolder.noun.setText(noun.getWord());
            viewHolder.explain.setText(noun.getContent());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView noun;
        TextView explain;
    }
}
