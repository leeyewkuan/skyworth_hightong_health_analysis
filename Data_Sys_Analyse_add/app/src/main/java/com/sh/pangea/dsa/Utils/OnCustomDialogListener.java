package com.sh.pangea.dsa.Utils;

public interface OnCustomDialogListener {
	void cancel();
	void confirm();
}
