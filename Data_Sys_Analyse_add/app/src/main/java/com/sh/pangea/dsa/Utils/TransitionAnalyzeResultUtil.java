package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.util.Log;

import com.sh.lib.dsa.bean.analyzeresult.AnalyzeResultLinear;
import com.sh.lib.dsa.bean.analyzeresult.AnalyzeResultRatio;
import com.sh.lib.dsa.bean.analyzeresult.LinearItem;
import com.sh.lib.dsa.bean.analyzeresult.RatioItem;
import com.sh.pangea.dsa.bean.AnalyzeResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TianGenhu on 2016/12/26.
 */

public class TransitionAnalyzeResultUtil {
    private volatile static TransitionAnalyzeResultUtil mInstance;
    private final String TAG = "TransitionAnalyzeResultUtil";
    private Context mContext;

    private TransitionAnalyzeResultUtil(Context context) {
        this.mContext = context;
    }

    public synchronized static TransitionAnalyzeResultUtil getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new TransitionAnalyzeResultUtil(context);
        }
        return mInstance;
    }


    /**
     * 把饼状图接口返回数据转换成本地通用实体
     **/
    public AnalyzeResult resultRatioTransitionAnalyzeResult(AnalyzeResultRatio analyzeResultRatio) {
        AnalyzeResult mAnalyzeResult = new AnalyzeResult();
        if (analyzeResultRatio != null) {
            mAnalyzeResult.setMessage(analyzeResultRatio.getMessage());
            mAnalyzeResult.setNowDate(analyzeResultRatio.getNowDate());
            mAnalyzeResult.setMinDate(analyzeResultRatio.getMinDate());
            mAnalyzeResult.setQueryDate(analyzeResultRatio.getQueryDate());
            mAnalyzeResult.setWeekly(analyzeResultRatio.getWeekly());
            mAnalyzeResult.setTitleGraph(analyzeResultRatio.getTitleGraph());
            List<RatioItem> listRatioItem = analyzeResultRatio.getYForGraph();
            List<LinearItem> yForGraph = new ArrayList<>();
            if (listRatioItem != null) {
                if (listRatioItem.size() > 0) {
                    // 期望倍数
                    float multiple = PreferenceUtil.getInstance(mContext).getFloat(PreferenceUtil.EXPECT_MULTIPLE, 1);
                    for (int i = 0; i < listRatioItem.size(); i++) {
                        LinearItem mLinearItem = new LinearItem();
                        RatioItem ratioItem = listRatioItem.get(i);
                        mLinearItem.setName(ratioItem.getName());
                        mLinearItem.setYAxis(ratioItem.getY() * (int) multiple);
                        yForGraph.add(mLinearItem);
                    }
                }
            }
            mAnalyzeResult.setyForGraph(yForGraph);
        }
        Log.i("TGH", "resultRatioTransitionAnalyzeResult:     " + mAnalyzeResult);
        return mAnalyzeResult;
    }

    /**
     * 把折线图、柱状图接口返回数据转换成本地通用实体
     **/
    public AnalyzeResult resultLinearTransitionAnalyzeResult(AnalyzeResultLinear analyzeResultLinear) {
        AnalyzeResult mAnalyzeResult = new AnalyzeResult();
        if (analyzeResultLinear != null) {
            mAnalyzeResult.setMessage(analyzeResultLinear.getMessage());
            mAnalyzeResult.setNowDate(analyzeResultLinear.getNowDate());
            mAnalyzeResult.setMinDate(analyzeResultLinear.getMinDate());
            mAnalyzeResult.setQueryDate(analyzeResultLinear.getQueryDate());
            mAnalyzeResult.setWeekly(analyzeResultLinear.getWeekly());
            mAnalyzeResult.setTitleGraph(analyzeResultLinear.getTitleGraph());
            mAnalyzeResult.setxForGraph(analyzeResultLinear.getXForGraph());

            // 期望倍数
            float multiple = PreferenceUtil.getInstance(mContext).getFloat(PreferenceUtil.EXPECT_MULTIPLE, 1);

            // 原来的Y轴数据
            List<LinearItem> linearItems = analyzeResultLinear.getYForGraph();
            // 新的Y轴数据
            List<LinearItem> linearItems2 = new ArrayList<>();
            for (LinearItem item : linearItems) {

                List<Integer> data = item.getData();
                List<Integer> data2 = new ArrayList<>();
                // 把data里的数值都乘以期望倍数后，塞到一个新的Datalist中
                for (Integer integer : data) {
                    data2.add(integer * (int) multiple);
                }
                // 把LinearItem中的data换成新数据
                item.setData(data2);
                // 把更换成新数据的LinearItem添加到新的List<LinearItem>中
                linearItems2.add(item);
            }
            // 设置成新的Y轴数据
            analyzeResultLinear.setYForGraph(linearItems2);

            mAnalyzeResult.setyForGraph(analyzeResultLinear.getYForGraph());

        }
        return mAnalyzeResult;
    }

}
