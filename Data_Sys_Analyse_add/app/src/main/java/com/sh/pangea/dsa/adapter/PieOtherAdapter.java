package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.lib.dsa.bean.analyzeresult.LinearItem;
import com.sh.pangea.dsa.R;

import java.util.List;


/**
 * Created by TianGenhu on 2016/10/10.
 */

public class PieOtherAdapter extends BaseAdapter {
    private final Context mContext;
    private List<LinearItem> listLinearItem;

    public PieOtherAdapter(Context context, List<LinearItem> linearItems) {
        this.mContext = context;
        this.listLinearItem = linearItems;
    }

    @Override
    public int getCount() {
        return listLinearItem.size();
    }

    @Override
    public Object getItem(int position) {
        return listLinearItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.public_listview_item, null);
            holder.data_item = (LinearLayout) convertView.findViewById(R.id.data_item);
            holder.data_id = (TextView) convertView.findViewById(R.id.data_id);
            holder.tvName = (TextView) convertView.findViewById(R.id.data_name);
            holder.tvValue = (TextView) convertView.findViewById(R.id.data_value);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (listLinearItem.size() > position) {
            LinearItem linearItem = listLinearItem.get(position);
            if (linearItem != null) {
                holder.tvName.setText(linearItem.getName());
                holder.data_id.setText((position + 1) + "");
                int yAxis = linearItem.getYAxis();
                holder.tvValue.setText(yAxis + "");
            }
        } else {
            holder.tvValue.setText("--");
        }
        if (position % 2 != 0) {
            holder.data_item.setBackgroundResource(R.color.colorListItemPressed);
        } else {
            holder.data_item.setBackgroundResource(R.color.transparent_white);
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView tvName;
        TextView tvValue;
        TextView data_id;
        LinearLayout data_item;
    }

}
