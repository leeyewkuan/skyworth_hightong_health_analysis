package com.sh.pangea.dsa.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sh.pangea.dsa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Data_Sys_Analyse
 * Created by LB on 2017/1/9,14:23
 */

public class TextDataItem extends RelativeLayout {
    private Context mContext;
    @BindView(R.id.tv_date)
    TextView tvTitle;
    @BindView(R.id.tv_details_data)
    TextView tvTitleData;
    @BindView(R.id.iv_data_icon)
    ImageView imIcon;

    public TextDataItem(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public TextDataItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    public TextDataItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View inflate = View.inflate(mContext, R.layout.text_data_item, this);
        ButterKnife.bind(this);

    }

    public void updata(String title, String changeRangeDay) {
        tvTitle.setText(title);
        tvTitleData.setText(changeRangeDay+"%");
        if(changeRangeDay.startsWith("-")){
            imIcon.setImageResource(R.mipmap.down_trend);
        }else{
            imIcon.setImageResource(R.mipmap.up_trend);
        }
    }
}
