package com.sh.pangea.dsa.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.adapter.NounExplainAdapter;
import com.sh.pangea.dsa.bean.Noun;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Create By 675   2017-1-18
 */
public class NounExplanationActivity extends AppCompatActivity {

    @BindView(R.id.noun_opening_sentence)
    TextView nounOpeningSentence;
    @BindView(R.id.noun_listview)
    ListView nounListview;
    @BindView(R.id.title_layout_back)
    ImageButton titleLayoutBack;
    @BindView(R.id.title_layout_text)
    TextView titleLayoutText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            // Translucent status bar
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        setContentView(R.layout.activity_noun_explanation);
        ButterKnife.bind(this);

        initDate();
        initListener();
    }

    private void initDate() {
        Intent mIn = getIntent();
        int type = mIn.getIntExtra("type", 0);
        titleLayoutText.setText(getResources().getString(R.string.noun_explain));
        String json = getJson(this, "wordsExplain.json");

        JSONObject jo = null;
        JSONArray jsArray = null;
        try {
            jo = new JSONObject(json);
            if (!jo.isNull("title")) {
                String title = jo.getString("title");
                nounOpeningSentence.setText(title);
            }

            if (!jo.isNull("items")) {
                List<Noun> nouns = null;
                Gson gson = new Gson();
                Type gtype = new TypeToken<List<Noun>>() {
                }.getType();
                if (type == 0) {
                    nouns = gson.fromJson(jo.getString("items"), gtype);
                } else {
                    nouns = gson.fromJson(jo.getString("itemPower"), gtype);
                }
                if (nouns != null && !nouns.isEmpty()) {
                    // setAdapter
                    NounExplainAdapter mNounExplainAdapter = new NounExplainAdapter(this, nouns);
                    nounListview.setAdapter(mNounExplainAdapter);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initListener() {
        titleLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 从asset路径下读取对应文件转String输出
     *
     * @param mContext
     * @return
     */
    public static String getJson(Context mContext, String fileName) {
        StringBuilder sb = new StringBuilder();
        AssetManager am = mContext.getAssets();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    am.open(fileName)));
            String next = "";
            while (null != (next = br.readLine())) {
                sb.append(next);
            }
        } catch (IOException e) {
            e.printStackTrace();
            sb.delete(0, sb.length());
        }
        return sb.toString().trim();
    }
}
