package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.lib.dsa.bean.SummaryInfo;
import com.sh.lib.dsa.bean.filter.DataFilter;
import com.sh.lib.dsa.bean.filter.FilterCondition;
import com.sh.lib.dsa.bean.filter.FilterType;
import com.sh.lib.dsa.callback.DataFilterListener;
import com.sh.lib.dsa.callback.ListListener;
import com.sh.lib.dsa.impl.AnalyzeManager;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.adapter.ScreenWeekAdapter;
import com.sh.pangea.dsa.adapter.StatisticsModleAdapter;
import com.sh.pangea.dsa.bean.MenuInfo;
import com.sh.pangea.dsa.view.DataCalendarView;
import com.sh.pangea.dsa.view.HospitalDetailsDataItem;
import com.sh.pangea.dsa.view.MyGridView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;

/**
 * Created by liuqiwu on 2016/12/26.
 * 筛选条件工具类
 */

public class FiltersUtil {
    private Context mContext;
    private static FiltersUtil instance;
    private AnalyzeResultUtil mAnalyzeResultUtil;
    // 子分类实例
    private MenuInfo mParentPosition;
    private MenuInfo mGroupPosition;
    private MenuInfo mChildPosition;
    // 返回filterView的监听
    private FiltersUtilListener mFiltersUtilListener;
    // filterView 筛选界面
    private View mView;
    // 获取筛选条件失败的提示
    private TextView filter_faild_text;

    public FiltersUtil(Context context) {
        this.mContext = context;
        mAnalyzeResultUtil = AnalyzeResultUtil.getInstance(mContext);
//        initView();
//        initListener();
        // 通知调用端添加筛选UI
//        mFiltersUtilListener.addFilterView(mView);
    }

    public static FiltersUtil getInstance(Context context) {
        if (instance == null) {
            instance = new FiltersUtil(context);
        }
        return instance;
    }

    /**
     * 返回filterView的监听
     */
    public interface FiltersUtilListener {
        void addFilterView(View filterView);

        void commitFilters(String filterString, String day, String week);
    }

    /**
     * 设置接收filterView的监听
     *
     * @param filtersUtilListener filterView的监听
     */
    public void setFiltersUtilListener(FiltersUtilListener filtersUtilListener) {
        mFiltersUtilListener = filtersUtilListener;
    }

    // 筛选框
    private LinearLayout linear_filter_other;
    // 统计方式的容器
    private RelativeLayout relative_statistical_methods;
    // 背景
    private FrameLayout filter_background;
    // 抽屉
    private SlidingDrawer filter_slidingdrawer;
    // 头部图片
    private ImageView filter_head_img;
    // 重置、提交
    private Button filter_bt_restart, filter_bt_submit;

    /**
     * ITEM 全ID
     */
    private String itemID = "";
    private String cancleYestDayInfo = null;
    private String cancleDataFilter = null;

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.filter_sliding_drawer_layout, null);
        linear_filter_other = (LinearLayout) mView.findViewById(R.id.linear_filter_other);
        relative_statistical_methods = (RelativeLayout) mView.findViewById(R.id.relative_statistical_methods);
        filter_week_list = (ListView) mView.findViewById(R.id.filter_week_list);
        filter_calendar_view = (DataCalendarView) mView.findViewById(R.id.filter_calendar_view);
        filter_date_modle = (RelativeLayout) mView.findViewById(R.id.filter_date_modle);
        filter_background = (FrameLayout) mView.findViewById(R.id.filter_background);
        filter_slidingdrawer = (SlidingDrawer) mView.findViewById(R.id.filter_slidingdrawer);
        filter_head_img = (ImageView) mView.findViewById(R.id.filter_head_img);
        filter_bt_restart = (Button) mView.findViewById(R.id.filter_bt_restart);
        filter_bt_submit = (Button) mView.findViewById(R.id.filter_bt_submit);
        filter_faild_text = (TextView) mView.findViewById(R.id.filter_faild_text);
    }

    private void initListener() {
        mAnalyzeResultUtil.setDataFilterListener(mDataFilterListener);

        filter_background.setOnClickListener(new View.OnClickListener() {//筛选阴影部分点击关闭筛选框
            @Override
            public void onClick(View v) {
                if (filter_slidingdrawer.isOpened()) {
                    filter_slidingdrawer.close();
                }
            }
        });
        filter_slidingdrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
                setImageState(R.mipmap.bg_home_poster_content, R.mipmap.icon_screen_up);
            }
        });
        filter_slidingdrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
                setImageState(mContext.getResources().getColor(R.color.transparent), R.color.transparent);
            }
        });

        /**
         * 重置
         */
        filter_bt_restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initDate();
            }
        });
        /**
         * 提交
         */
        filter_bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter_slidingdrawer.close();
                notifiLoadingDate();
            }
        });
    }

    /**
     * 设置头题头图片的背景及资源
     */
    private void setImageState(int bgRes, int imgRes) {
        filter_head_img.setBackgroundResource(bgRes);
        filter_head_img.setImageResource(imgRes);
    }

    /**
     * 获取图表的查询条件1
     *
     * @param tMenuInfo    一级MenuInfo
     * @param sMenuInfo    二级MenuInfo
     * @param itemMenuInfo 三级MenuInfo
     */
    public void launch(MenuInfo tMenuInfo, MenuInfo sMenuInfo, MenuInfo itemMenuInfo) {
        initView();
        initListener();
        temFilterMap.clear();
        focusButtonMa.clear();

        mParentPosition = tMenuInfo;
        mGroupPosition = sMenuInfo;
        mChildPosition = itemMenuInfo;
        if (mParentPosition != null && mGroupPosition != null && mChildPosition != null) {
//            querySelCondList(mParentPosition.getId(), mGroupPosition.getId(), mChildPosition.getId(), mChildPosition.getChartType());

            itemID = mParentPosition.getId() + mGroupPosition.getId() + mChildPosition.getId();

            // 流程
            // 1、调用昨日关键数据列表接口
            // 2、调用筛选条件接口
            // 3、拼数据
            // 4、画UI
            // 5、调用图表接口
            startYestDayBranch();
        }
    }

    /**
     * 流程一：调用昨日关键数据列表接口及后续逻辑
     */
    private void startYestDayBranch() {
        Log.i("Main","FiltersUtil   startYestDayBranch()");
        if (itemID.equals("T5S0A1")){//无线认证日走势图
            // 有昨日列表接口，但不作为筛选条件。直接调用筛选接口
            startFilterBranch();
            return;
        }
        // 调用昨日接口
        boolean isExistYestDay = AnalyzeIntentUtil.getInstance(mContext).callQueryYestDayInfoInterface(itemID, yestDayInfoListListener);
        // 不存在昨日接口，直接调筛选接口
        if (!isExistYestDay) {
            startFilterBranch();
        }
    }

    /**
     * 流程二：调用筛选条件接口及后续逻辑
     */
    private void startFilterBranch() {
        // 调用筛选条件接口
        boolean isExistkFilter = AnalyzeIntentUtil.getInstance(mContext).callQueryFilterListInterface(itemID, mDataFilterListener);

        if (mChildPosition.getDateSelect() == null || mChildPosition.getDateSelect().isEmpty() && !isExistkFilter){
            // 没有日期没有筛选条件，显示提示，隐藏筛选条件控件
            filter_slidingdrawer.setVisibility(View.INVISIBLE);
            filter_faild_text.setVisibility(View.VISIBLE);
        } else{
            filter_slidingdrawer.setVisibility(View.VISIBLE);
            filter_faild_text.setVisibility(View.INVISIBLE);
            // 加日期选择UI
            setWeekTouch(mChildPosition.getDateSelect());
            // 通知调用端添加筛选UI
            mFiltersUtilListener.addFilterView(mView);
        }

        // 不存在筛选接口，直接调图表接口
        if (!isExistkFilter) {
            // 调用图表接口
            notifiLoadingDate();
        }
    }

    /**
     * 获取图表的查询条件2
     *
     * @param tID       一级ID
     * @param sID       二级ID
     * @param itemID    三级ID
     * @param chartType 图表类型
     */
/*    public void querySelCondList(String tID, String sID, String itemID, String chartType) {
        switch (tID) {
            case "T0": // 健康视频
                switch (sID) {
                    case "S0":
                        noFilters();
                        break;
                    case "S1":
                        if (chartType.equals("L")) { // 各地区走势图
                            mAnalyzeResultUtil.queryMedicalTvTrendSelCondList(mDataFilterListener);
                        } else if (chartType.equals("B")) { // 各地区排行榜
                            mAnalyzeResultUtil.queryMedicalTvRankingSelCondList(mDataFilterListener);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "T1": // 直播
                switch (sID) {
                    case "S0":
                        if (chartType.equals("B")) { // 总览排行榜
                            mAnalyzeResultUtil.queryTvRankingOverviewSelCondList(mDataFilterListener);
                        } else {
                            noFilters();
                        }
                        break;
                    case "S1":
                        if (chartType.equals("L")) { // 各地区走势图
                            mAnalyzeResultUtil.queryTvTrendSelCondList(mDataFilterListener);
                        } else if (chartType.equals("B")) { // 各地区排行榜
                            mAnalyzeResultUtil.queryTvRankingSelCondList(mDataFilterListener);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "T2": // 回看
                switch (sID) {
                    case "S0":
                        if (chartType.equals("B")) { // 总览排行榜
                            mAnalyzeResultUtil.queryTvRankingOverviewSelCondList(mDataFilterListener);
                        } else {
                            noFilters();
                        }
                        break;
                    case "S1":
                        if (chartType.equals("L")) { // 各地区走势图
                            mAnalyzeResultUtil.queryTvTrendSelCondList(mDataFilterListener);
                        } else if (chartType.equals("B")) { // 各地区排行榜
                            mAnalyzeResultUtil.queryTvRankingSelCondList(mDataFilterListener);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "T3": // 点播
                switch (sID) {
                    case "S0":
                        if (chartType.equals("B")) { // 总览排行榜
                            mAnalyzeResultUtil.queryTvRankingOverviewSelCondList(mDataFilterListener);
                        } else {
                            noFilters();
                        }
                        break;
                    case "S1":
                        if (chartType.equals("L")) { // 各地区走势图
                            mAnalyzeResultUtil.queryTvTrendSelCondList(mDataFilterListener);
                        } else if (chartType.equals("B")) { // 各地区排行榜
                            mAnalyzeResultUtil.queryTvRankingSelCondList(mDataFilterListener);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "T4": // 新兴业务
                switch (sID) {
                    case "S0":
                        noFilters();
                        break;
                    default:
                        break;
                }
                break;
            case "T5": // 无线认证
                switch (sID) {
                    case "S2":
                        switch (itemID) {
                            case "A1": // AP冷热排行榜
                                mAnalyzeResultUtil.queryHeatRangeSelCondList(mDataFilterListener);
                                break;
                            case "B1": // AP日排行榜
                                mAnalyzeResultUtil.queryAPDayRankingSelCondList(mDataFilterListener);
                                break;
                            case "C1": // AP日走势图
                                mAnalyzeResultUtil.queryHeatTrendSelCondList(mDataFilterListener);
                                break;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "T6": // 医院业务
                switch (itemID) {
                    case "A1": // 就诊导航
                    case "A2":
                        mAnalyzeResultUtil.queryMedicalNavigationSelCondList(mDataFilterListener);
                        break;
                    case "B1": // 医院资讯
                    case "B2":
                        noFilters();
                        break;
                    case "C1": // 出诊专家
                    case "C2":
                        mAnalyzeResultUtil.queryExpertSelCondList(mDataFilterListener);
                        break;
                    default:
                        break;
                }
                break;
            case "T7": // 健康电视APP
                switch (itemID) {
                    case "A1": // 启动次数
                        noFilters();
                        break;
                    case "B1": // 首页模块
                        mAnalyzeResultUtil.queryHomePageModuleSelCondList(mDataFilterListener);
                        break;
                    default:
                        break;
                }
                break;
            default:
                noFilters();
                break;
        }
    }

    private void noFilters() {
        Log.i("Main", "筛选Util    noFilters()    mChildPosition:" + mChildPosition.toString());
        if (mChildPosition.getDateSelect() != null && !mChildPosition.getDateSelect().isEmpty()) {
            // 加日期选择UI
            setWeekTouch(mChildPosition.getDateSelect());
            // 通知调用端添加筛选UI
            mFiltersUtilListener.addFilterView(mView);
        }
        // 没有筛选条件
        // 直接请求图表
        notifiLoadingDate();
    }*/

    /**
     * 昨日关键指标数据回调
     */
    private ListListener yestDayInfoListListener = new ListListener<SummaryInfo>() {
        @Override
        public void onSuccess(List<SummaryInfo> list) {
            // 把昨日关键指标数据转化为显示内容区分筛选项
            parserYestDayInfo(list);
            // 调用筛选接口
            startFilterBranch();
        }

        @Override
        public void onPrepare(String s) {
            if (cancleYestDayInfo == null) {
                cancleYestDayInfo = s;
            } else {
                AnalyzeManager.instence().cancelReq(cancleYestDayInfo);
                cancleYestDayInfo = s;
            }
        }

        @Override
        public void onFail(int i, String s) {
            // 调用筛选接口
            startFilterBranch();
        }
    };

    /**
     * 获取查询列表回调监听
     */
    private DataFilterListener mDataFilterListener = new DataFilterListener() {
        @Override
        public void onSuccess(DataFilter dataFilter) {
            // 根据请求到的数据构建筛选项UI
            parserDataFilter(dataFilter);
        }

        @Override
        public void onPrepare(String s) {
            if (cancleDataFilter == null) {
                cancleDataFilter = s;
            } else {
                AnalyzeManager.instence().cancelReq(cancleDataFilter);
                cancleDataFilter = s;
            }
            // 通知调用端添加筛选UI
            mFiltersUtilListener.addFilterView(mView);
        }

        @Override
        public void onFail(int i, String s) {

        }
    };

    private List<FilterCondition> mYestDayInfoes = null;

    /**
     * 把昨日关键指标数据转化为显示内容区分筛选项
     *
     * @param list 昨日关键指标数据
     */
    private void parserYestDayInfo(List<SummaryInfo> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        mYestDayInfoes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            // 获取昨日关键指标数据中的name，作为显示内容区分筛选项。
            // value值从0开始，依次排开
            SummaryInfo summaryInfo = list.get(i);
            FilterCondition filterCondition = new FilterCondition();

            filterCondition.setName(summaryInfo.getName());
            filterCondition.setValue(i + "");
            // 把构建好的筛选项，添加到list中
            mYestDayInfoes.add(filterCondition);
        }
    }

    /**
     * 筛选项&状态
     * -1：未处理
     * 0：已经处理
     * 其他：选中位置
     */
    private Map<String, String> temFilterMap = new HashMap<>();

    /**
     * 解析查询列表数据
     *
     * @param dataFilter 查询列表数据
     */
    private void parserDataFilter(DataFilter dataFilter) {
        // 筛选项list
        List<FilterType> list = (List<FilterType>) dataFilter.getDataList();

        // 如果有昨日关键数据，则添加显示内容区分筛选项
        if (mYestDayInfoes != null) {
            FilterType viewTypeFilterType = new FilterType();
            viewTypeFilterType.setName("viewType");
            viewTypeFilterType.setLable("显示内容区分:");
            viewTypeFilterType.setComboxList(mYestDayInfoes);
            list.add(viewTypeFilterType);
        }

        // 遍历筛选大项，构建筛选UI
        for (FilterType filterType : list) {
            String type = filterType.getName();
            String lable = filterType.getLable(); // 筛选项名称
            Log.i("Main", "parserDataFilter()   解析查询列表数据   lable:" + lable);
            List<FilterCondition> items = filterType.getComboxList();
            temFilterMap.put(type, "-1");
            // 构建筛选项UI
            successAddView(type, lable, items);
        }
    }


    // 不同筛选条件里当前选择的TextView
    private Map<String, TextView> focusButtonMa = new HashMap<>();
    private Map<String, TextView> focusButtonMa1 = new HashMap<>();
    private Map<String, String> temFilterMap1 = new HashMap<>();

    private void successAddView(final String type, String lable, final List<FilterCondition> list) {
        if (list == null || list.isEmpty()) return;

        View layout = LayoutInflater.from(mContext).inflate(R.layout.item_filter_of_chart, null);
        // 设置筛选项名称
        final TextView textView = (TextView) layout.findViewById(R.id.item_filter_name);
        textView.setText(lable);
        final MyGridView gridView = (MyGridView) layout.findViewById(R.id.item_filter_gridview);
        gridView.setHaveScrollbar(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView text = (TextView) view.findViewById(R.id.item_statistics_name);
                setButtonSelect(type, i, list, text, focusButtonMa.get(type));
            }
        });
        // 设置Adapter
        StatisticsModleAdapter modleAdapter = new StatisticsModleAdapter(mContext, list);
        modleAdapter.setLoadedFirstViewListener(type, new StatisticsModleAdapter.LoadedFirstViewListener() {
            @Override
            public void loadedFirstView(String key, TextView textView) {
                if (focusButtonMa.get(key) != null) {
                    return;
                }
                gridView.setSelection(0);
                focusButtonMa1.put(key, textView);
                temFilterMap1.put(key, list.get(0).getValue());
                setButtonSelect(key, 0, list, textView, null);
                notifiLoadingDate();

            }
        });
        gridView.setAdapter(modleAdapter);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, (int) mContext.getResources().getDimension(R.dimen.dime_px2dip_24), 0, 0);
        // 添加layout
        if (type.equals("playType")) {
            relative_statistical_methods.addView(layout, layoutParams);
        } else {
            linear_filter_other.addView(layout, layoutParams);
        }
    }

    private void setButtonSelect(String type, int position, List<FilterCondition> list, TextView focus, TextView nor) {
        if (focus != null) {
            focus.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            focus.setBackgroundResource(R.mipmap.bg_xuanji_focus);
        }

        focusButtonMa.put(type, focus);
        if (!list.isEmpty() && list.size() > position) {
            temFilterMap.put(type, list.get(position).getValue());
        }

        if (nor != null) {
            nor.setTextColor(mContext.getResources().getColor(R.color.colorTextTitle));
            nor.setBackgroundResource(R.mipmap.bg_xuanji);
        }
    }


    //周信息适配器
    private ScreenWeekAdapter weekAdapter;
    // 周信息集合
    private List<ModuleItem> weekList;
    // 周信息UI
    private ListView filter_week_list;
    // 日期选择控件
    private DataCalendarView filter_calendar_view;
    // 日期&周父控件
    private RelativeLayout filter_date_modle;

    /**
     * 显示周信息，还是日信息，是否可以点击,不可点击目前是隐藏控件
     *
     * @param dateSelect 999 周 222 日
     */
    private void setWeekTouch(String dateSelect) {
        if (dateSelect == null || dateSelect.isEmpty()) {
            filter_date_modle.setVisibility(View.GONE);
            return;
        } else {
            filter_date_modle.setVisibility(View.VISIBLE);
        }
        temFilterMap.put(dateSelect, "-1");
        if (dateSelect.equals("999")) {
            filter_calendar_view.setVisibility(View.GONE);
            filter_week_list.setVisibility(View.VISIBLE);
            weekAdapter = new ScreenWeekAdapter();
            filter_week_list.setAdapter(weekAdapter);
            filter_week_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    onClickWeekItem(position);
                }
            });
            initWeekDate();
        } else if (dateSelect.equals("222")) {
            filter_week_list.setVisibility(View.GONE);
            filter_calendar_view.setVisibility(View.VISIBLE);
            filter_calendar_view.setOnDateChange(new DataCalendarView.OnDateChange() {
                @Override
                public void onDateChange(String time) {
                    temFilterMap.put("222", time);
                }
            });
            initDtePicker();
            filter_calendar_view.setDateInterval(filter_calendar_view.getYear(), 2010, filter_calendar_view.getMooth(), 1, filter_calendar_view.getDate(), 1);
            String dateStr = filter_calendar_view.getTimeStr();

            temFilterMap.put("222", dateStr);
//            notifiLoadingDate();
            filter_calendar_view.bringToFront();
        }
    }

    /**
     * 初始化日期为前一天
     */
    private void initDtePicker() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        Date time = calendar.getTime();
        filter_calendar_view.initTime(time);
    }

    /**
     * 周信息条目的点击事件
     */
    private void onClickWeekItem(int position) {
        if (weekList != null && weekList.size() > position) {
            weekAdapter.setSelect(position);
            ModuleItem moduleItem = weekList.get(position);
            String value = moduleItem.getValue();
            try {
                int i = parseInt(value);
                temFilterMap.put("999", i + "");
            } catch (Exception e) {
            }
        }
    }

    private final int CON_TIME_OUT = 1000;
    private final int SO_TIME_OUT = 1000;

    /**
     * 获取周信息
     */
    private void initWeekDate() {
        AnalyzeManager.instence().queryWeeklyList(CON_TIME_OUT, SO_TIME_OUT, new ListListener<ModuleItem>() {
            @Override
            public void onSuccess(List<ModuleItem> list) {
                weekList = list;
                updateWeekView(weekList);
            }

            @Override
            public void onPrepare(String s) {

            }

            @Override
            public void onFail(int i, String s) {
                temFilterMap.put("999", "-1");
            }
        });
    }

    /**
     * 更新周信息空间
     */
    private void updateWeekView(List<ModuleItem> list) {
        if (list != null && list.size() > 0) {
            if (weekAdapter != null) {
                weekAdapter.update(weekList);
                filter_week_list.setSelection(0);
                ModuleItem moduleItem = list.get(0);
                String time = moduleItem.getValue();
                try {
                    int weekTime = Integer.parseInt(time);
                    temFilterMap.put("999", weekTime + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            temFilterMap.put("999", "-1");
        }
        notifiLoadingDate();
    }

    /**
     * 通知显示界面更新界面数据
     */
    private void notifiLoadingDate() {
        boolean isLoadingOver = true;

        if (temFilterMap.size() > 0) {
            for (String key : temFilterMap.keySet()) {
//                if (key.isEmpty()) {
//                    continue;
//                }
                String value = temFilterMap.get(key);
                Log.i("Main", "notifiLoadingDate()   准备的筛选项:" + key + "    值:" + value);
                if (value == null || value.equals("-1")) {
                    Log.i("Main", "notifiLoadingDate()   尚未准备好的筛选项:" + key);
                    isLoadingOver = false;
                }
            }
        }
        Log.i("Main", "notifiLoadingDate()   isLoadingOver:" + isLoadingOver);
        if (isLoadingOver) {
            String filterString = "";
            for (String key : focusButtonMa.keySet()) {
                TextView nor = focusButtonMa.get(key);
                filterString += "," + nor.getText().toString();
            }
            // 把选中的筛选条件的名称传递给UI层
            if (filterString.length() > 1) {
                filterString = filterString.substring(1);
            }
            // 设置筛选参数
            AnalyzeIntentUtil.getInstance(mContext).setTemFilterMap(temFilterMap);

            Log.i("Main", "选中的筛选条件   filterString:" + filterString);
            mFiltersUtilListener.commitFilters(filterString, temFilterMap.get("222"), temFilterMap.get("999"));
//            AnalyzeIntentUtil.getInstance(mContext).analyzeIntent(mParentPosition, mGroupPosition, mChildPosition);
            // 调用获取图表方法
            Log.i("Main", "调用获取图表方法itemID:" + itemID);
            AnalyzeIntentUtil.getInstance(mContext).callChartInterface(itemID, mChildPosition.getChartType());
        }
    }


    /**
     * 重置所有状态
     */
    private void initDate() {
        // 重置所有项
        // 数据重置
        temFilterMap.clear();
        temFilterMap.putAll(temFilterMap1);

        // UI重置
        for (String key : focusButtonMa.keySet()) {
            TextView nor = focusButtonMa.get(key);
            if (nor != null) {
                // 恢复原色
                nor.setTextColor(mContext.getResources().getColor(R.color.colorTextTitle));
                nor.setBackgroundResource(R.mipmap.bg_xuanji);
            }

            TextView focusView = focusButtonMa1.get(key);
            if (focusView != null) {
                // 加选中色
                focusView.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                focusView.setBackgroundResource(R.mipmap.bg_xuanji_focus);
            }
        }
        focusButtonMa.clear();
        focusButtonMa.putAll(focusButtonMa1);

        //重置日期选择
        if (filter_calendar_view.getVisibility() == View.VISIBLE) {
            initDtePicker();
            String date = filter_calendar_view.getTimeStr();
            temFilterMap.put("222", date);
        }
        if (filter_week_list.getVisibility() == View.VISIBLE) {
            filter_week_list.setSelection(0);
            onClickWeekItem(0);
        }

        AnalyzeIntentUtil.getInstance(mContext).setTemFilterMap(temFilterMap);
    }

    public void closeSlidingDrawer() {
        if (filter_slidingdrawer.isOpened()) {
            filter_slidingdrawer.close();
        }
    }
}
