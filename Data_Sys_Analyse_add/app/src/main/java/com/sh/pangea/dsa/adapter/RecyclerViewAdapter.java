package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.sh.pangea.dsa.R;

import java.util.List;


/**
 * Data_Sys_Analyse
 * Created by LB on 2016/10/13,10:04
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private  List<String> names;
    private LayoutInflater mInflater;

    private int sidePosition;

    //是否隐藏加权平均值
    private boolean notShowAnticipativeDate = true;

    public void setSidePosition(int sidePosition) {
        this.sidePosition = sidePosition;
    }

    public interface OnItemClickListener
    {
        void onItemClick(View view,int position);

        void onItemLongClick(View view,int position);
    }

    public RecyclerViewAdapter(List<String> names,Context mContext)
    {
        this.names = names;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =mInflater.inflate(R.layout.side_pull_item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mText.setText(names.get(position));
        for (int i= 0; i<names.size();i++){
            if (position ==sidePosition ){
                holder.total_layout.setBackgroundResource(R.color.transparent_white);
            }
            else {
                holder.total_layout.setBackgroundResource(R.color.white);
            }
        }
        setClick(holder);
    }

    @Override
    public int getItemCount() {
        if (notShowAnticipativeDate){
            return names.size()-1;
        }else {
            return names.size();
        }
    }

    public void setShowAnticipativeDate(boolean notShowAnticipativeDate) {
        this.notShowAnticipativeDate = notShowAnticipativeDate;
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public void setClick(final MyViewHolder holder)
    {
        if (mOnItemClickListener != null)
        {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int layoutPosition = holder.getLayoutPosition();
                    mOnItemClickListener.onItemClick(holder.itemView,layoutPosition);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int layoutPosition = holder.getLayoutPosition();
                    mOnItemClickListener.onItemLongClick(holder.itemView,layoutPosition);
                    return false;
                }
            });
        }
    }

    public void add(int position)
    {

    }

    public void delete(int position)
    {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mText;
        public LinearLayout total_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            mText = (TextView) itemView.findViewById(R.id.tv_side_pull);
            total_layout = (LinearLayout)itemView.findViewById(R.id.total_layout);
        }
    }

}
