package com.sh.pangea.dsa.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.lib.dsa.callback.ListListener;
import com.sh.lib.dsa.impl.AnalyzeManager;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.AnalyzeResultUtil;
import com.sh.pangea.dsa.adapter.MySelectHospitalRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * Creat By 675 2017-1-17
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SelectHospitalFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private MySelectHospitalRecyclerViewAdapter mMySelectHospitalRecyclerViewAdapter;
    private RecyclerView recyclerView;
    /**
     * 医院数据
     */
    private List<ModuleItem> hospitales = new ArrayList<>();
    /**
     * 请求医院列表接口打断
     */
    private String cancleQueryInstanceListValNotNull;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SelectHospitalFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SelectHospitalFragment newInstance(int columnCount) {
        SelectHospitalFragment fragment = new SelectHospitalFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selecthospital_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            mMySelectHospitalRecyclerViewAdapter = new MySelectHospitalRecyclerViewAdapter(hospitales, mListener);
            recyclerView.setAdapter(mMySelectHospitalRecyclerViewAdapter);
        }
        getHospitalList();
        return view;
    }

    /**
     * 获取医院列表
     */
    private void getHospitalList() {
        AnalyzeManager.instence().queryInstanceListValNotNull(AnalyzeResultUtil.CON_TIME_OUT, AnalyzeResultUtil.SO_TIME_OUT, mListListener);
    }

    /**
     * 获取医院列表回调
     */
    private ListListener mListListener = new ListListener() {
        @Override
        public void onSuccess(List list) {
            // 更新adapter
            cancleQueryInstanceListValNotNull = null;
            ModuleItem moduleItem = new ModuleItem();
            moduleItem.setName("全部");
            moduleItem.setValue("");
            list.add(0, moduleItem);
            hospitales.addAll(list);
            mMySelectHospitalRecyclerViewAdapter.notifyDataSetChanged();
//            setselected(hospitales);
        }

        @Override
        public void onPrepare(String s) {
            hospitales.clear();
            // 打断
            if (cancleQueryInstanceListValNotNull == null) {
                cancleQueryInstanceListValNotNull = s;
            } else {
                AnalyzeManager.instence().cancelReq(cancleQueryInstanceListValNotNull);
                cancleQueryInstanceListValNotNull = s;
            }
        }

        @Override
        public void onFail(int i, String s) {
            // 打断置空
            cancleQueryInstanceListValNotNull = null;
        }
    };

    private void setselected(List<ModuleItem> items) {
        Bundle bundle = getArguments();//从activity传过来的Bundle
        String name = bundle.getString("name");
        if (name != null && !name.isEmpty()){
            for (int i = 0; i > items.size(); i++){
                if (name.equals(items.get(i))){
                    // 当前选中的医院是xxx
                }
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ModuleItem item);
    }
}
