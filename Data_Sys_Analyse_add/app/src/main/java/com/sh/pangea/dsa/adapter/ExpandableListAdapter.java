package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.bean.MenuInfo;

import java.util.List;
import java.util.Map;


/**
 * Created by yuangang on 2016/9/23.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<MenuInfo> groupList;
    private Map<Integer, List<MenuInfo>> childMap;

    public ExpandableListAdapter(Context context, List<MenuInfo> list, Map<Integer, List<MenuInfo>> map) {
        this.mContext = context;
        this.groupList = list;
        this.childMap = map;
        selected = new SparseBooleanArray();
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(childMap.get(groupPosition) != null){
            return childMap.get(groupPosition).size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childMap.get(groupPosition).get(childPosition).getName();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup viewGroup) {
        GroupHolder groupHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.expandablelist_groupitem, null);
            groupHolder = new GroupHolder();
//            groupHolder.groupImg = (ImageView) convertView.findViewById(R.id.img_indicator);
            groupHolder.groupText = (TextView) convertView.findViewById(R.id.tv_group_text);
            convertView.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) convertView.getTag();
        }
//        if (isExpanded) {
//            groupHolder.groupImg.setBackgroundResource(R.mipmap.expandable_itemdown);
//        } else {
//            groupHolder.groupImg.setBackgroundResource(R.mipmap.expandable_itemup);
//        }
        groupHolder.groupText.setText(groupList.get(groupPosition).getName());
        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup viewGroup) {
        ChildHolder childHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.expandablelist_childitem, null);
            childHolder = new ChildHolder();
            childHolder.childImg = (ImageView) convertView.findViewById(R.id.img_child);
            childHolder.childText = (TextView) convertView.findViewById(R.id.tv_child_text);
            childHolder.rl_child_view = (RelativeLayout) convertView.findViewById(R.id.rl_child_view);
            convertView.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) convertView.getTag();
        }
        if (selected.get(childPosition)&&this.parentPosition==groupPosition) {
            childHolder.rl_child_view.setBackgroundResource(R.color.transparent_white);
        } else {
            childHolder.rl_child_view.setBackgroundResource(R.color.white);
        }
        childHolder.childText.setText(childMap.get(groupPosition).get(childPosition).getName());
        if (childMap.get(groupPosition).get(childPosition).getChartType() == "L"){
            childHolder.childImg.setBackgroundResource(R.mipmap.line_chart);
        }else if(childMap.get(groupPosition).get(childPosition).getChartType() == "P"){
            childHolder.childImg.setBackgroundResource(R.mipmap.pie_chart);
        }else if(childMap.get(groupPosition).get(childPosition).getChartType() == "B"){
            childHolder.childImg.setBackgroundResource(R.mipmap.histogram);
        }else{
            childHolder.childImg.setBackgroundResource(R.mipmap.data_list);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class GroupHolder {
//        ImageView groupImg;
        TextView groupText;
    }

    private class ChildHolder {
        ImageView childImg;
        TextView childText;
        RelativeLayout rl_child_view;
    }

    public void refresh(List<MenuInfo> list, Map<Integer, List<MenuInfo>> map) {
        this.groupList = list;
        this.childMap = map;
        this.notifyDataSetChanged();
    }

    //用来装载某个item是否被选中
    SparseBooleanArray selected;
    int old = -1;
    int parentPosition = -1;

    public void setSelectedItem(int groupPosition,int childPosition) {
        this.parentPosition = groupPosition;
        if (old != -1) {
            this.selected.put(old, false);
        }
        this.selected.put(childPosition, true);
        old = childPosition;
    }
}