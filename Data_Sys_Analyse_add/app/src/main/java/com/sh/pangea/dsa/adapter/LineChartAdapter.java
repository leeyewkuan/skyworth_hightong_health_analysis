package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.lib.dsa.bean.analyzeresult.LinearItem;
import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Created by TianGenhu on 2016/10/10.
 */

public class LineChartAdapter extends BaseExpandableListAdapter {
//    private final Context mContext;
//    private List<String> mName = new ArrayList<>();
//    private String[] mValue = new String[]{};
//
//    public LineChartAdapter(Context context, List<String> xForGraph, String[] value) {
//        this.mContext = context;
//        this.mName = xForGraph;
//        this.mValue = value;
//    }
//
//    @Override
//    public int getCount() {
//        return mName.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mName.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        if (convertView == null) {
//            holder = new ViewHolder();
//            convertView = LayoutInflater.from(mContext).inflate(R.layout.public_listview_item, null);
//            holder.data_item = (LinearLayout) convertView.findViewById(R.id.data_item);
//            holder.data_id = (TextView) convertView.findViewById(R.id.data_id);
//            holder.tvName = (TextView) convertView.findViewById(R.id.data_name);
//            holder.tvValue = (TextView) convertView.findViewById(R.id.data_value);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        holder.tvName.setText(mName.get(position));
//        if (mValue.length > position) {
//            holder.data_id.setText((position + 1) + "");
//            holder.tvValue.setText(mValue[position]);
//        } else {
//            holder.tvValue.setText(R.string.no_data);
//        }
//        if (position % 2 != 0) {
//            holder.data_item.setBackgroundResource(R.color.colorListItemPressed);
//        } else {
//            holder.data_item.setBackgroundResource(R.color.transparent_white);
//        }
//        return convertView;
//    }
//
//    private static class ViewHolder {
//        TextView tvName;
//        TextView tvValue;
//        TextView data_id;
//        LinearLayout data_item;
//    }
//


    private Context context;
    LayoutInflater inflater;
    private List<LinearItem> mMonthsValues;
    private List<String> xForGraph;

    public LineChartAdapter(Context context, List<LinearItem> monthsValues,
                            List<String> xForGraph) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mMonthsValues = monthsValues;
        this.xForGraph = xForGraph;
    }

    public Object getChild(int groupPosition, int childPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > groupPosition) {
                LinearItem linearItem = mMonthsValues.get(groupPosition);
                if (linearItem != null) {
                    List<Integer> data = linearItem.getData();
                    if (data != null && data.size() > 0) {
                        if (data.size() > childPosition) {
                            return data.get(childPosition);
                        }
                    }
                }
            }
        }
        return "--";
    }

    public Object getChildX(int childPosition) {
        if (xForGraph != null && xForGraph.size() > 0) {
            if (xForGraph.size() > childPosition) {
                return xForGraph.get(childPosition);
            }
        }
        return "--";
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(int groupPosition, int childPosition, boolean arg2, View convertView,
                             ViewGroup arg4) {
        convertView = inflater.inflate(R.layout.public_listview_item, null);
        LinearLayout data_item = (LinearLayout) convertView.findViewById(R.id.data_item);
        TextView data_id = (TextView) convertView.findViewById(R.id.data_id);
        TextView tvName = (TextView) convertView.findViewById(R.id.data_name);
        TextView tvValue = (TextView) convertView.findViewById(R.id.data_value);
        tvValue.setText(getChild(groupPosition, childPosition).toString());
        tvName.setText(getChildX(childPosition).toString());
        data_id.setText((childPosition + 1) + "");
        if (childPosition % 2 != 0) {
            data_item.setBackgroundResource(R.color.colorListItemPressed);
        } else {
            data_item.setBackgroundResource(R.color.transparent_white);
        }
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > groupPosition) {
                LinearItem linearItem = mMonthsValues.get(groupPosition);
                if (linearItem != null) {
                    List<Integer> data = linearItem.getData();
                    if (data != null) {
                        return data.size();
                    }
                }
            }
        }
        return 0;
    }

    public Object getGroup(int groupPosition) {
        if (mMonthsValues != null && mMonthsValues.size() > 0) {
            if (mMonthsValues.size() > groupPosition) {
                LinearItem linearItem = mMonthsValues.get(groupPosition);
                if (linearItem != null) {
                    if (linearItem.getName() != null) {
                        return linearItem.getName();
                    } else {
                        return "";
                    }
                }
            }
        }
        return "";
    }

    public int getGroupCount() {
        return mMonthsValues.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup arg3) {
        convertView = inflater.inflate(R.layout.listview_group_item, null);
        TextView listview_group_num = (TextView) convertView.findViewById(R.id.listview_group_num);
        listview_group_num.setText(getChildrenCount(groupPosition) + "");
        TextView groupNameTextView = (TextView) convertView.findViewById(R.id.listview_group_name);
        groupNameTextView.setText(getGroup(groupPosition).toString());
        ImageView image = (ImageView) convertView.findViewById(R.id.listview_image);
//        image.setImageResource(R.mipmap.ic_user_album);
        image.setImageResource(R.mipmap.arrows_right);
        //更换展开分组图片
        if (!isExpanded) {
//            image.setImageResource(R.mipmap.ic_launcher);
            image.setImageResource(R.mipmap.arrows_down);
        }
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    // 子选项是否可以选择
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }


}
