package com.sh.pangea.dsa.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by yuangang on 2016/9/23.
 */

public class MenuInfo implements Serializable, Parcelable {

    /**
     * ID:
     * 第一级:T开头  (如:回看T3)
     * 第二级:S开头  (如:各地区S2)
     * 第三级:根据分类，A/B/C   (如:周回看节目排行榜C2)
     */
    private String id;
    /**
     * 名称
     */
    private String name;
    /**
     * 日期选择(日:222、周:999)
     */
    private String dateSelect;
    /**
     * 图表类型
     * L:折线图
     * B:柱状图
     * P:饼状图
     */
    private String chartType;

    @Override
    public String toString() {
        return "MenuInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", dateSelect='" + dateSelect + '\'' +
                ", chartType='" + chartType + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateSelect() {
        return dateSelect;
    }

    public void setDateSelect(String dateSelect) {
        this.dateSelect = dateSelect;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.dateSelect);
        dest.writeString(this.chartType);
    }

    public MenuInfo() {
    }

    protected MenuInfo(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.dateSelect = in.readString();
        this.chartType = in.readString();
    }

    public static final Parcelable.Creator<MenuInfo> CREATOR = new Parcelable.Creator<MenuInfo>() {
        @Override
        public MenuInfo createFromParcel(Parcel source) {
            return new MenuInfo(source);
        }

        @Override
        public MenuInfo[] newArray(int size) {
            return new MenuInfo[size];
        }
    };
}
