package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Data_Sys_Analyse
 * Created by LB on 2016/10/13,10:04
 */

public class AnticipativeDataAdapter extends RecyclerView.Adapter<AnticipativeDataAdapter.MyViewHolder> {

    private  List<String> items;
    private LayoutInflater mInflater;

    public interface OnItemClickListener
    {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    public AnticipativeDataAdapter(List<String> items, Context mContext)
    {
        this.items = items;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =mInflater.inflate(R.layout.anticipative_data_item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mText.setText(items.get(position));
//        for (int i= 0; i<items.size();i++){
//            if (position ==sidePosition ){
//                holder.total_layout.setBackgroundResource(R.color.transparent_white);
//            }
//            else {
//                holder.total_layout.setBackgroundResource(R.color.white);
//            }
//        }
        setClick(holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public void setClick(final MyViewHolder holder)
    {
        if (mOnItemClickListener != null)
        {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int layoutPosition = holder.getLayoutPosition();
                    mOnItemClickListener.onItemClick(holder.itemView,layoutPosition);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int layoutPosition = holder.getLayoutPosition();
                    mOnItemClickListener.onItemLongClick(holder.itemView,layoutPosition);
                    return false;
                }
            });
        }
    }

    public void add(int position)
    {

    }

    public void delete(int position)
    {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mText;
        public LinearLayout total_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            mText = (TextView) itemView.findViewById(R.id.tv_data);
            total_layout = (LinearLayout)itemView.findViewById(R.id.total_data_layout);
        }
    }

}
