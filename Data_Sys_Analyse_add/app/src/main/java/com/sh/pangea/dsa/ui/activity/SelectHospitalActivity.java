package com.sh.pangea.dsa.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sh.lib.dsa.bean.ModuleItem;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.ui.fragment.SelectHospitalFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creat By 675   2017-1-17
 */
public class SelectHospitalActivity extends AppCompatActivity implements SelectHospitalFragment.OnListFragmentInteractionListener {
    public static final int RESULT_CODE = 1;
    @BindView(R.id.title_layout_back)
    ImageButton titleLayoutBack;
    @BindView(R.id.title_layout_text)
    TextView titleLayoutText;
    @BindView(R.id.select_hospital_name)
    TextView selectHospitalName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            // Translucent status bar
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        setContentView(R.layout.activity_select_hospital);
        ButterKnife.bind(this);

        initView();
        initDate();
        initListener();
    }

    private void initView() {
        SelectHospitalFragment mSelectHospitalFragment = new SelectHospitalFragment();

        if (getIntent() != null) {
            Bundle bun = new Bundle();
            bun.putString("name", getIntent().getStringExtra("name"));
            bun.putString("value", getIntent().getStringExtra("value"));
            selectHospitalName.setVisibility(View.VISIBLE);
            selectHospitalName.setText("现在选择的医院是：" + getIntent().getStringExtra("name"));
            // 向Fragment中传现已选择的医院
            mSelectHospitalFragment.setArguments(bun);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.select_hospital_main, mSelectHospitalFragment).commit();
    }

    private void initDate() {
        titleLayoutText.setText(getResources().getString(R.string.select_hospital));
    }

    private void initListener() {
        titleLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onListFragmentInteraction(ModuleItem item) {
        // Item点击事件
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("name", item.getName());
        intent.putExtra("value", item.getValue());
        setResult(RESULT_CODE, intent);
        finish();
    }
}
