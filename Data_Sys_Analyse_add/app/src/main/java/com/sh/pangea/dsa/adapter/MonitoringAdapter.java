package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Data_Sys_Analyse
 * Created by LB on 2017/1/17,15:31
 */

public class MonitoringAdapter extends BaseAdapter {
    private List<String> list ;
    private Context context;
    public MonitoringAdapter(Context context,List<String> list){
        this.context = context;
        this.list = list;
    }

    public void updata(List<String> list){
       this.list = list;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        int count = 0;
        if(list != null){
            count = list.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = View.inflate(context, R.layout.two_textview_item, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.value = (TextView) convertView.findViewById(R.id.tv_name_value);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        String s = list.get(position);
        String[] split = s.split(":");
            holder.name.setText(split[0]);
        if(split.length == 2){
            holder.value.setText(split[1]);
        }else{
            holder.value.setText("");
        }
        return convertView;
    }

    static class ViewHolder{
        TextView name;
        TextView value;
    }
}
