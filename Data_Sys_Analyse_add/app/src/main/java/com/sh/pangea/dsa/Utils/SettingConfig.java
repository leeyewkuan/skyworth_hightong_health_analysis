package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.sh.lib.base.bean.SoftWare;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * 设置的配置类
 *
 * @author LeeYewKuan
 * @creatDate：2015年9月18日
 */
public class SettingConfig {
    /**
     * 配置文件名称
     */
    public static final String CONFIG_NAME = "SettingConfig";

    /**
     * 多次提示wifi网络
     */
    public static final String CHECK_WIFI_KEY = "checkWifi";

    /**
     * 今日是否已签到
     */
    public static final String SIGNIN_TIME_KEY = "_signInTime";

    /**
     * 是否需要每次都检测
     *
     * @param mContext
     * @return true 每次都要，false 只有一次要
     * @author： LeeYewKuan
     * @update： 2015年9月18日
     */
    public static boolean isNeedCheckWifiEveryTime(Context mContext) {
        boolean flag = true;
        SharedPreferences sp = mContext.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        if (sp.contains(CHECK_WIFI_KEY)) {
            flag = sp.getBoolean(CHECK_WIFI_KEY, true);
        }
        return flag;
    }

    /**
     * 是否每次都要检测wifi. true 每次都检测，false 检测一次
     */
    public static void setCheckWifiState(Context mContext, boolean everyTime) {
        SharedPreferences sp = mContext.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        sp.edit().putBoolean(CHECK_WIFI_KEY, everyTime).commit();
    }

    /**
     * 是否签过到
     *
     * @param mContext
     * @return true 今天已经签过到  false 今天还未签到
     * @author： LeeYewKuan
     * @update： 2015年9月18日
     */
    public static boolean isSignIn(Context mContext, String account) {
        //不判断有没有签到过了
        boolean flag = false;
        SharedPreferences sp = mContext.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        Log.i("", sp.contains(account + SIGNIN_TIME_KEY) + "");
        if (sp.contains(account + SIGNIN_TIME_KEY)) {
            long signTime = sp.getLong(account + SIGNIN_TIME_KEY, 0);
            Calendar mCaledarStart = Calendar.getInstance(Locale.CHINA);
            mCaledarStart.setTime(new Date(System.currentTimeMillis()));
            mCaledarStart.set(Calendar.HOUR, 0);
            mCaledarStart.set(Calendar.MINUTE, 0);
            mCaledarStart.set(Calendar.SECOND, 0);
            long startTime = mCaledarStart.getTimeInMillis();
            Log.i("", signTime + " - " + startTime + " = " + (signTime - startTime));
            if (signTime >= startTime) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 设置签到时间
     *
     * @param mContext
     * @param time     签到的时间
     * @author： LeeYewKuan
     * @update： 2015年9月18日
     */
    public static void setSignTime(Context mContext, String mobile, long time) {
        SharedPreferences sp = mContext.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        sp.edit().putLong(mobile + SIGNIN_TIME_KEY, time).commit();
    }

    public static SoftWare isNeedUpdate(Context mContext, List<SoftWare> softWare) {
        SoftWare item = null;
        boolean isNeedUpdate = false;
        String curentApkPageName = mContext.getApplicationInfo().packageName;
        int currentVersion = AppVersionUtil.getVersion(mContext);
        for (int i = 0; i < softWare.size(); i++) {
            item = softWare.get(i);
            String packageName = item.getPackageName();

            if (packageName != null && packageName.equals(curentApkPageName)) {
                String softWareVersion = item.getVersionCode();
                // 需要更新
                try {

                    if (softWareVersion != null && Integer.parseInt(softWareVersion) > currentVersion) {
                        isNeedUpdate = true;
                        break;
                    } else {
                        Log.i("", "");
                    }
                } catch (Exception e) {
                    Log.e("", "isNeedUpdate Exception\n" + e.getLocalizedMessage());
                }
            } else {
                continue;
            }
        }
        if (!isNeedUpdate) {
            item = null;
        }
        return item;
    }

    public static boolean isForceUpdate(Context mContext, SoftWare softWare) {
        int currentVersion = AppVersionUtil.getVersion(mContext);
        boolean forceUpdate = false;
        String minVersion = softWare.getForceUpgradeVersion();// 需要取最小版本
        // 需要强制更新
        try {
            if (minVersion != null && Integer.parseInt(minVersion) > currentVersion) {
                forceUpdate = true;
            }
        } catch (Exception e) {
            Log.e("", "get MinVersion Exception\n" + e.getLocalizedMessage());
        }
        return forceUpdate;

    }
}
