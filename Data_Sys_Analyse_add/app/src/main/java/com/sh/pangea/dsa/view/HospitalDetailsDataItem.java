package com.sh.pangea.dsa.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.lib.dsa.bean.SummaryInfo;
import com.sh.pangea.dsa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/** 昨日关键认证其中的一条数据*/
public class HospitalDetailsDataItem extends LinearLayout {
    private Context mContext;

    @BindView(R.id.tv_count)
    TextView tvName;
    @BindView(R.id.tv_count_value)
     TextView tvNameValue;
    @BindView(R.id.data01)
    TextDataItem item01;
    @BindView(R.id.data02)
    TextDataItem item02;
    @BindView(R.id.data03)
    TextDataItem item03;
    public HospitalDetailsDataItem(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public HospitalDetailsDataItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    public HospitalDetailsDataItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView(){
        View.inflate(mContext, R.layout.hospital_details_item,this);
        ButterKnife.bind(this);
    }

    public void update(SummaryInfo authDetailInfo) {
        Log.i("","authDetailInfo:"+authDetailInfo);
        tvName.setText(authDetailInfo.getName());
        tvNameValue.setText(authDetailInfo.getNums());
        String changeRangeDay = authDetailInfo.getChangeRangeDay();
        item01.updata("日",changeRangeDay);
        String changeRangWeekly = authDetailInfo.getChangeRangWeekly();
        item02.updata("周",changeRangWeekly);
        String changeRangeMonth = authDetailInfo.getChangeRangeMonth();
        item03.updata("月",changeRangeMonth);
    }
}
