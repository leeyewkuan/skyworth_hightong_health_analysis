package com.sh.pangea.dsa.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sh.lib.base.bean.SoftWare;
import com.sh.pangea.dsa.R;
import com.sh.pangea.dsa.Utils.AppManager;
import com.sh.pangea.dsa.Utils.AppVersionUtil;
import com.sh.pangea.dsa.Utils.PreferenceUtil;
import com.skyworth_hightong.bean.AppUpDateInfo;
import com.skyworth_hightong.update.server.DownLoadInterface;
import com.skyworth_hightong.update.server.DownLoadManager;

import java.io.File;

/**
 * 升级界面
 * 
 * @author wangchuang
 */
public class UpgradeActivity extends Activity implements OnClickListener {

	/**
	 * 进度条
	 */
	private ProgressBar progressBar;

	private TextView tv_pronum;

	private TextView tv_updata_ing;

	private LinearLayout ll_upgrade_lose;

	private TextView tv_upgrade_qu;

	TextView tv_reset;

	private TextView tv_version;

	private int isForceUpdate;

	private SoftWare mSoftWare;

	private DownLoadManager mDownLoadManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppManager.getAppManager().addActivity(UpgradeActivity.this);
		setContentView(R.layout.activity_upgrade);
		initView();
		if (mDownLoadManager == null) {
			mDownLoadManager = DownLoadManager.getInstance(this);
		}

		tv_reset.setOnClickListener(this);
		tv_upgrade_qu.setOnClickListener(this);
		tv_version.setText(AppVersionUtil.getVersionName(this));

		Intent intent = getIntent();
		if (intent != null) {
			mSoftWare = (SoftWare) intent
					.getSerializableExtra("software");
			if(mSoftWare != null && mSoftWare.getVersionName() != null){
				tv_version.setText(mSoftWare.getVersionName());
			}else{
				Log.e("","mSoftWare obj or filed is null"+mSoftWare);
			}
		} else {
			Log.e("","intent error");
			this.finish();
		}
		setUpdateListener(mSoftWare);
		setInfomation(mSoftWare);

	}

	private void initView() {
		progressBar = (ProgressBar) findViewById(R.id.myprogressbar);

		tv_pronum = (TextView) findViewById(R.id.tv_pronum);

		tv_updata_ing = (TextView) findViewById(R.id.tv_updata_ing);

		ll_upgrade_lose = (LinearLayout) findViewById(R.id.ll_upgrade_lose);

		tv_upgrade_qu = (TextView) findViewById(R.id.tv_upgrade_qu);

		tv_reset = (TextView) findViewById(R.id.tv_reset);

		tv_version = (TextView) findViewById(R.id.tv_version);

	}

	/**
	 * 准备数据，开始下载
	 * 
	 * @param softWare
	 * @author： LeeYewKuan
	 * @update： 2015年9月30日
	 */
	private void setInfomation(SoftWare softWare) {
		AppUpDateInfo updateInfo = new AppUpDateInfo();
		updateInfo.setAppVersion(softWare.getVersionCode());
		updateInfo.setPackageName(softWare.getPackageName());
		updateInfo.setAppVersionName(softWare.getVersionName());
		updateInfo.setDownloadAddr(softWare.getDownloadLink());
		String flag = softWare.getForceUpgradeVersion();
		updateInfo.setForceUpdateFlag(flag);
		mDownLoadManager.execute(updateInfo);
	}

	private void setUpdateListener(final SoftWare softWare) {
		mDownLoadManager.setDownLoadListener(new DownLoadInterface() {

			@Override
			public void onSuccess(File file) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(file),
						"application/vnd.android.package-archive");
				startActivity(intent);
				UpgradeActivity.this.finish();
			}

			@Override
			public void onStart() {
				if (progressBar != null) {
					progressBar.setProgress(0);
					tv_updata_ing.setVisibility(View.VISIBLE);
					ll_upgrade_lose.setVisibility(View.GONE);
				} else {
					Log.e("","onStart view null");
				}
			}

			@Override
			public void onProgressChange(int progress) {
				Log.e("","progress = " + progress);
				if (progressBar != null) {
					progressBar.setProgress(progress);
					tv_pronum.setText(progress + "%");
					if (progress == 100) {
						tv_version.setText(softWare.getVersionName());
					}
				} else {
					Log.e("","onProgressChange view null");
				}
			}

			@Override
			public void onFail() {
				if (null != tv_updata_ing) {
					tv_updata_ing.setVisibility(View.GONE);
					ll_upgrade_lose.setVisibility(View.VISIBLE);
				} else {
					Log.e("","onFail view null");
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_reset:
			setInfomation(mSoftWare);
			break;

		case R.id.tv_upgrade_qu:
			isForceUpdate = PreferenceUtil.getInstance(UpgradeActivity.this)
					.getInt("isForceUpdate", 0);
			if (isForceUpdate == 1) {
				AppManager.getAppManager().AppExit(UpgradeActivity.this);
			} else if (isForceUpdate == 0) {
				finish();
			}
			break;

		default:
			break;
		}

	}

}
