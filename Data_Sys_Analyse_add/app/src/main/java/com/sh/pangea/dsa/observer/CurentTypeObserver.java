package com.sh.pangea.dsa.observer;

/**
 * Created by zhouyucheng on 2017/5/31.
 */

public interface CurentTypeObserver {
    //通知切换类型
    void updateType(int  type);
}
