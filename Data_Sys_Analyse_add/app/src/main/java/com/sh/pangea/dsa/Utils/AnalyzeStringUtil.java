package com.sh.pangea.dsa.Utils;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;

import java.util.Random;

/**
 * Created by TianGenhu on 2016/10/19.
 */

public class AnalyzeStringUtil {
    private volatile static AnalyzeStringUtil mInstance;
    private final String TAG = "AnalyzeStringUtil";

    private AnalyzeStringUtil(/*Context context*/) {
        //this.mContext = context;
    }

    public synchronized static AnalyzeStringUtil getInstance(/*Context context*/) {
        if (mInstance == null) {
            mInstance = new AnalyzeStringUtil(/*context*/);
        }
        return mInstance;
    }


    /***
     * 把特定的字符串转化成字符串数组
     **/
    public String[] getStringArray(String cnt) {
        String[] split = null;
        if (cnt != null && !TextUtils.isEmpty(cnt)) {
            split = cnt.split(",");
            if (split.length > 0) {
                return split;
            }
        }
        return split;
    }

    /**
     * 用于截取字符串
     **/
    public String cutOutString(String cnt) {
        if (cnt != null && !TextUtils.isEmpty(cnt)) {
            if (cnt.contains("-")) {
                String[] split = cnt.split("-");
                cnt = split[(split.length - 1)];
                Log.i(TAG, "getString: cnt :" + cnt);
            } else {
                int length = cnt.length();
                if (length >= 6) {
                    cnt = cnt.substring(cnt.length() - 5, cnt.length());
                }
            }
            return cnt;
        } else {
            Log.i(TAG, "get: data is null");
        }
        return null;
    }

    /**
     * 匹配字符串
     **/
    public String mateString(String str, String[] mMonths) {
        if (mMonths != null && mMonths.length > 0) {
            for (int i = 0; i < mMonths.length; i++) {
                String mMonth = mMonths[i];
                if (mMonth.contains(str)) {
                    return mMonth;
                }
            }
        }
        return str;
    }


    public int getNumber(int number) {
        int length = COLORS.length;
        if (length >= number) {
            Random rand = new Random();
            number = rand.nextInt(25);
            Log.i(TAG, "getNumber     number: "+number);
        }
        return number;
    }

    public static final int[] COLORS = {
            //public static final int[] VORDIPLOM_COLORS = {
            Color.rgb(192, 255, 140), Color.rgb(255, 247, 140), Color.rgb(255, 208, 140),
            Color.rgb(140, 234, 255), Color.rgb(255, 140, 157),
            //public static final int[] LIBERTY_COLORS = {
            Color.rgb(207, 248, 246), Color.rgb(148, 212, 212), Color.rgb(136, 180, 187),
            Color.rgb(118, 174, 175), Color.rgb(42, 109, 130),
            //public static final int[] JOYFUL_COLORS = {
            Color.rgb(217, 80, 138), Color.rgb(254, 149, 7), Color.rgb(254, 247, 120),
            Color.rgb(106, 167, 134), Color.rgb(53, 194, 209),
            //public static final int[] PASTEL_COLORS = {
            Color.rgb(64, 89, 128), Color.rgb(149, 165, 124), Color.rgb(217, 184, 162),
            Color.rgb(191, 134, 134), Color.rgb(179, 48, 80),
            //public static final int[] COLORFUL_COLORS = {
            Color.rgb(193, 37, 82), Color.rgb(255, 102, 0), Color.rgb(245, 199, 0),
            Color.rgb(106, 150, 31), Color.rgb(179, 100, 53)
    };


}
