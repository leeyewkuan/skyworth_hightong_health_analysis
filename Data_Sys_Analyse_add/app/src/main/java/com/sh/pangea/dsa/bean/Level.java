package com.sh.pangea.dsa.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TianGenhu on 2017/1/18.
 */

public class Level {
    private String name;
    private int max;
    private int min = 1;

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public List<Integer> getLeveList() {
        return leveList;
    }

    public void setLeveList(List<Integer> leveList) {
        this.leveList = leveList;
    }

    List<Integer> list = new ArrayList<>();

    List<Integer> leveList = new ArrayList<>();


    public Level(int max, String name, List<Integer> list) {
        this.max = max;
        this.name = name;
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getLevel() {
        return getLen(max, min);
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    private int getLen(int max, int min) {
        if (min == 0) {
            min = 1;
        }
        int level = max / min;
        int i = 1;
        while (true) {
            i *= 10;
            if (i > level) {
                i /= 10;
                break;
            }
        }
        return i;
    }
}
