package com.sh.pangea.dsa.Utils;

import android.content.Context;
import android.util.Log;

import com.sh.lib.dsa.bean.analyzeresult.AnalyzeResultLinear;
import com.sh.lib.dsa.bean.analyzeresult.AnalyzeResultRatio;
import com.sh.lib.dsa.callback.AnalyzeLinearListener;
import com.sh.lib.dsa.callback.AnalyzeRatioListener;
import com.sh.lib.dsa.callback.CompareDateListener;
import com.sh.lib.dsa.callback.DataFilterListener;
import com.sh.lib.dsa.callback.ListListener;
import com.sh.lib.dsa.impl.AnalyzeManager;
import com.sh.pangea.dsa.bean.AnalyzeResult;
import com.sh.pangea.dsa.observer.AnalyzePopWindowObservable;
import com.sh.pangea.dsa.observer.AnalyzeResultObservable;

import java.util.List;
import java.util.Map;

/**
 * Created by TianGenhu on 2016/10/10.
 */

public class AnalyzeResultUtil {
    /**
     * 健康讲座
     */
    public static final String HEALTH_LECTURE_CODE = "T0";
    /**
     * 直播
     */
    public static final String LIVE_CODE = "T1";
    /**
     * 回看
     */
    public static final String EPG_CODE = "T2";
    /**
     * 点播
     */
    public static final String VOD_CODE = "T3";

    /**
     * 新兴业务
     */
    public static final String MEDICAL_CODE = "T4";

    /**
     * 无线认证
     */
    public static final String HISPORTAL_CODE = "T5";

    /**
     * 医院业务
     */
    public static final String AP_CODE = "T6";

    /**
     * 健康电视APP
     */
    public static final String HEALTH_TV_APP_CODE = "T7";


    public static final int CON_TIME_OUT = 1000;
    public static final int SO_TIME_OUT = 1000;
    private final Context mContext;
    private static final String SUCCESS = "Success";
    private volatile static AnalyzeResultUtil mInstance;

    private volatile static TransitionAnalyzeResultUtil mTransitionAnalyzeResultUtil;

    private AnalyzeResultUtil(Context context) {
        this.mContext = context;
        mTransitionAnalyzeResultUtil = TransitionAnalyzeResultUtil.getInstance(mContext);
    }

    public synchronized static AnalyzeResultUtil getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AnalyzeResultUtil(context);
        }
        return mInstance;
    }

    /**
     * 饼状图的回调
     */
    AnalyzeRatioListener mAnalyzeRatioListener = new AnalyzeRatioListener() {
        @Override
        public void onSuccess(AnalyzeResultRatio analyzeResultRatio) {
            Log.i("TGH", "*********饼状图的回调数据:/n" + analyzeResultRatio);
            AnalyzeResult analyzeResult = mTransitionAnalyzeResultUtil.resultRatioTransitionAnalyzeResult(analyzeResultRatio);
            AnalyzeResultObservable.getInstance().onAnalyzeResult(0, SUCCESS, analyzeResult);
        }

        @Override
        public void onPrepare(String s) {
            Log.i("TGH", "onPrepare: 饼状图接口准备的方法");
        }

        @Override
        public void onFail(int i, String s) {
            Log.i("TGH", "饼状图接口失败 onFail: " + s + "     " + i);
            AnalyzeResultObservable.getInstance().onAnalyzeResult(i, s, null);
        }
    };

    /**
     * 折线图、柱状图的回调
     */
    AnalyzeLinearListener mAnalyzeLinearListener = new AnalyzeLinearListener() {
        @Override
        public void onSuccess(AnalyzeResultLinear analyzeResultLinear) {
            Log.i("TGH", "onSuccess:  " + analyzeResultLinear);
            AnalyzeResult analyzeResult = mTransitionAnalyzeResultUtil.resultLinearTransitionAnalyzeResult(analyzeResultLinear);
            AnalyzeResultObservable.getInstance().onAnalyzeResult(0, SUCCESS, analyzeResult);
        }

        @Override
        public void onPrepare(String s) {
            Log.i("TGH", "onPrepare: 接口准备的方法");
        }

        @Override
        public void onFail(int i, String s) {
            Log.i("TGH", "接口失败 onFail: " + s + "     " + i);
            AnalyzeResultObservable.getInstance().onAnalyzeResult(i, s, null);
        }
    };

    private final AnalyzeLinearListener mAnalyzePopWindowListener = new AnalyzeLinearListener() {
        @Override
        public void onSuccess(AnalyzeResultLinear analyzeResultLinear) {
            Log.i("TGH", "PopWindow  onSuccess: " + analyzeResultLinear.toString());
            AnalyzeResult analyzeResult = mTransitionAnalyzeResultUtil.resultLinearTransitionAnalyzeResult(analyzeResultLinear);
            AnalyzePopWindowObservable.getInstance().onAnalyzeResult(0, SUCCESS, analyzeResult);
        }

        @Override
        public void onPrepare(String s) {
            Log.i("TGH", "PopWindow  onPrepare: 接口准备的方法");
        }

        @Override
        public void onFail(int i, String s) {
            Log.i("TGH", "PopWindow   接口失败 onFail: " + s + "     " + i);
            AnalyzePopWindowObservable.getInstance().onAnalyzeResult(i, s, null);
        }
    };

    /*ListListener mListListener = new ListListener() {
        @Override
        public void onSuccess(List list) {
            Log.i("TGH", "ListListener  onSuccess: " + list);
        }

        @Override
        public void onPrepare(String s) {
            Log.i("", "ListListener onPrepare: ");
        }

        @Override
        public void onFail(int i, String s) {
            Log.i("", "ListListener onFail   s: " + s + "    i ：" + i);
        }
    };
*/
    /**
     * 2.1.7获取医院编码列表信息（没有“请选择”）
     * 功能描述：获取医院编码列表信息（没有“请选择”）。
     **/
    public void queryInstanceListValNotNull(ListListener listener) {
        AnalyzeManager.instence().queryInstanceListValNotNull(CON_TIME_OUT, SO_TIME_OUT, listener);
    }

    /**
     * 2.1.8获取比较日期
     * 功能描述：根据间隔时间获取比较日期。
     * 输入参数：
     * timeRange	String	时间间隔（28天，14天，7天）
     **/

    public void queryCompareDate(String timeRange, CompareDateListener compareDateListener) {
        AnalyzeManager.instence().queryCompareDate(CON_TIME_OUT, SO_TIME_OUT, timeRange, compareDateListener);
    }

    /**
     * 2.2.1健康视频地区占比图
     * 查询健康视频地区占比图显示数据。
     **/
    public void queryRatioMedicalGraph() {
        AnalyzeManager.instence().queryRatioMedicalGraph(CON_TIME_OUT, SO_TIME_OUT, mAnalyzeRatioListener);
    }

    /**
     * 2.2.2健康视频频道占比图
     * 查询健康视频频道占比图显示数据。
     **/
    public void queryRatioMedicalChannelGraph() {
        AnalyzeManager.instence().queryRatioMedicalChannelGraph(CON_TIME_OUT, SO_TIME_OUT, mAnalyzeRatioListener);
    }

    /**
     * 2.2.3 健康视频总览日走势图
     * 功能描述：查询健康视频总览日走势图
     */
    public void queryTrendMedicalGraphAll(Map<String, String> headers) {
        AnalyzeManager.instence().queryTrendMedicalGraphAll(CON_TIME_OUT, SO_TIME_OUT, headers, mAnalyzeLinearListener);
    }

    /**
     * 2.2.4 健康视频频道统计日走势图
     * 查询健康视频日走势图。
     **/
    public void queryTrendMedicalGraph(Map<String, String> headers, String rootChannelId) {
        AnalyzeManager.instence().queryTrendMedicalGraph(CON_TIME_OUT, SO_TIME_OUT, headers, rootChannelId, mAnalyzeLinearListener);
    }

    /**
     * 健康视频周走势图
     * 查询健康视频周走势图
     * @此方法去掉了
     **/
    @Deprecated
    public void queryTrendMedicalWeeklyGraph(Map<String, String> headers, int playType, String rootChannelId) {
        AnalyzeManager.instence().queryTrendMedicalWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, rootChannelId, mAnalyzeLinearListener);
    }

    /**
     * 2.2.5健康视频日排行榜
     * 查询健康视频日排行榜排行榜Top20。
     **/
    public void queryRankingMedicalTvGraph(Map<String, String> headers, int playType, String rootChannelId, String queryDate) {
        AnalyzeManager.instence().queryRankingMedicalTvGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, rootChannelId, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.2.6健康视频周排行榜
     * 查询健康视频周排行榜Top20。
     **/
    public void queryRankingMedicalTvWeeklyGraph(Map<String, String> headers, int playType, String rootChannelId, String weekly) {
        AnalyzeManager.instence().queryRankingMedicalTvWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, rootChannelId, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.2.7健康视频折线图
     * 查询健康视频日折线图。
     * 新添加了参数 ：rootChannelId	String	一级频道
     **/
    public void queryTrendMedicalDaysGraph(Map<String, String> headers, int playType, String medicalName, String rootChannelId, String weekly) {
        AnalyzeManager.instence().queryTrendMedicalDaysGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, medicalName, rootChannelId, weekly, mAnalyzePopWindowListener);
    }

    /**
     * 2.3.1直播地区占比图
     * 查询直播地区占比图显示数据。
     **/
    public void queryRatioTvGraph() {
        AnalyzeManager.instence().queryRatioTvGraph(CON_TIME_OUT, SO_TIME_OUT, mAnalyzeRatioListener);
    }

    /**
     * 2.3.2直播频道日走势图
     * 查询直播频道日走势图。
     * 新添加参数  ：operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryTrendTvGraph(Map<String, String> headers, String operatorCode) {
        AnalyzeManager.instence().queryTrendTvGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, mAnalyzeLinearListener);
    }

    /**
     * 直播频道周走势图
     * 查询直播频道周走势图。
     * @此方法去掉了
     **/
    @Deprecated
    public void queryTrendTvWeeklyGraph(Map<String, String> headers, int playType) {
        AnalyzeManager.instence().queryTrendTvWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, mAnalyzeLinearListener);
    }

    /**
     * 2.3.3直播频道日排行榜
     * 查询直播频道日排行榜Top20。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryRankingTvGraph(Map<String, String> headers, String operatorCode, int playType, String queryDate) {
        AnalyzeManager.instence().queryRankingTvGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.3.4直播频道周排行榜
     * 查询直播频道周排行榜Top20。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryRankingTvWeeklyGraph(Map<String, String> headers, String operatorCode, int playType, String weekly) {
        AnalyzeManager.instence().queryRankingTvWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.3.5直播频道折线图
     * 查询直播频道日折线图。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryTrendTvDaysGraph(Map<String, String> headers, String operatorCode, int playType, String tvName, String weekly) {
        AnalyzeManager.instence().queryTrendTvDaysGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, tvName, weekly, mAnalyzePopWindowListener);
    }

    /**
     * 2.3.6回看地区占比图
     * 查询回看地区占比图显示数据。
     **/
    public void queryRatioEventGraph() {
        AnalyzeManager.instence().queryRatioEventGraph(CON_TIME_OUT, SO_TIME_OUT, mAnalyzeRatioListener);
    }

    /**
     * 2.3.7回看频道日走势图
     * 查询所有回看频道每日的点击次数和播放时长。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryTrendEventGraph(Map<String, String> headers, String operatorCode) {
        AnalyzeManager.instence().queryTrendEventGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, mAnalyzeLinearListener);
    }

    /**
     * 回看频道周走势图
     * 查询所有回看频道每周的点击次数和播放时长。
     * @此方法去掉了
     **/
    @Deprecated
    public void queryTrendEventWeeklyGraph(Map<String, String> headers, int playType) {
        AnalyzeManager.instence().queryTrendEventWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, mAnalyzeLinearListener);
    }

    /**
     * 2.3.8回看频道日排行榜
     * 查询所有回看频道每日点击次数或收视时长排行榜Top20。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryRankingEventGraph(Map<String, String> headers, String operatorCode, int playType, String queryDate) {
        AnalyzeManager.instence().queryRankingEventGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.3.9回看频道周排行榜
     * 查询所有回看频道每周点击次数或收视时长排行榜Top20。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryRankingEventWeeklyGraph(Map<String, String> headers, String operatorCode, int playType, String weekly) {
        AnalyzeManager.instence().queryRankingEventWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.3.10回看频道折线图
     * 查询回看频道日折线图。
     * 新添加参数 ： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryTrendEventDaysGraph(Map<String, String> headers, String operatorCode, int playType, String tvName, String weekly) {
        AnalyzeManager.instence().queryTrendEventDaysGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, tvName, weekly, mAnalyzePopWindowListener);
    }

    /**
     * 2.3.11回看节目日排行榜
     * 查询所有回看节目每日点击次数或收视时长排行榜Top20。
     **/
    public void queryRankingEventEventGraph(Map<String, String> headers, int playType, String queryDate) {
        AnalyzeManager.instence().queryRankingEventEventGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.3.12回看节目周排行榜
     * 查询所有回看节目每周点击次数或收视时长排行榜Top20。
     **/
    public void queryRankingEventEventWeeklyGraph(Map<String, String> headers, int playType, String weekly) {
        AnalyzeManager.instence().queryRankingEventEventWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.3.13回看节目折线图
     * 查询回看节目日折线图。
     **/
    public void queryTrendEventEventDaysGraph(Map<String, String> headers, int playType, String tvName, String weekly) {
        AnalyzeManager.instence().queryTrendEventEventDaysGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, tvName, weekly, mAnalyzePopWindowListener);
    }

    /**
     * 2.3.14点播地区占比图
     * 查询点播地区占比图显示数据。
     **/
    public void queryRatioVodGraph() {
        AnalyzeManager.instence().queryRatioVodGraph(CON_TIME_OUT, SO_TIME_OUT, mAnalyzeRatioListener);
    }

    /**
     * 2.3.15点播节目日走势图
     * 查询所有点播节目每日的点击次数和播放时长。
     * 新添加参数： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryTrendVodGraph(Map<String, String> headers, String operatorCode) {
        AnalyzeManager.instence().queryTrendVodGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, mAnalyzeLinearListener);
    }

    /**
     * 点播节目周走势图
     * 查询所有点播节目每周的点击次数和播放时长。
     * @此方法去掉了
     **/
    @Deprecated
    public void queryTrendVodWeeklyGraph(Map<String, String> headers, int playType) {
        AnalyzeManager.instence().queryTrendVodWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, playType, mAnalyzeLinearListener);
    }


    /**
     * 2.3.16点播节目日排行榜
     * 访问路径：/rankingvod/rankingVod/queryRankingVodGraph。
     * 新添加参数： operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryRankingVodGraph(Map<String, String> headers, String operatorCode, int playType, String queryDate) {
        AnalyzeManager.instence().queryRankingVodGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.3.17点播节目周排行榜
     * 查询所有点播节目每周点击次数或收视时长排行榜Top20。
     * 新添加参数 ：operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryRankingVodWeeklyGraph(Map<String, String> headers, String operatorCode, int playType, String weekly) {
        AnalyzeManager.instence().queryRankingVodWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.3.18点播节目折线图
     * 查询点播节目日折线图。
     * 新添加参数 ：operatorCode	String	运营商编码（总览时为空）
     **/
    public void queryTrendVodDaysGraph(Map<String, String> headers, String operatorCode, int playType, String vodName, String weekly) {
        AnalyzeManager.instence().queryTrendVodDaysGraph(CON_TIME_OUT, SO_TIME_OUT, headers, operatorCode, playType, vodName, weekly, mAnalyzePopWindowListener);
    }

    /**
     * 2.4.1养生保健日排行榜
     * 查询养生保健日排行榜Top20。
     **/
    public void queryRankingHealthDetailGraph(Map<String, String> headers, int en, String queryDate) {
        AnalyzeManager.instence().queryRankingHealthDetailGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.4.2养生保健周排行榜
     * 查询养生保健周排行榜Top20。
     **/
    public void queryRankingHealthDetailWeeklyGraph(Map<String, String> headers, int en, String weekly) {
        AnalyzeManager.instence().queryRankingHealthDetailWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.5.1 报表无线认证总览
     * 功能描述：查询无线认证总览计统计数据
     */
    public void querytDAuthDayList(String playDay, ListListener detailInfoListener) {
        AnalyzeManager.instence().querytDAuthDayList(CON_TIME_OUT, SO_TIME_OUT, playDay, detailInfoListener);
    }

    /**
     * 2.5.2 报表公众号用户总览
     * 功能描述：查询微信用户统计数据
     */
    public void queryUserWxDayList(String playDay, ListListener detailInfoListener) {
        AnalyzeManager.instence().queryUserWxDayList(CON_TIME_OUT, SO_TIME_OUT, playDay, detailInfoListener);
    }

    /**
     * 2.5.3 报表无线认证按医院
     * 功能描述：查询无线认证按医院计统计数据
     */
    public void querytDAuthDayListArea(String instanceCode, String playDay, ListListener detailInfoListener) {
        AnalyzeManager.instence().querytDAuthDayListArea(CON_TIME_OUT, SO_TIME_OUT, instanceCode, playDay, detailInfoListener);
    }

    /**
     * 2.5.4 总览微信用户分布图
     * 功能描述：查询微信用户分布情况统计数据
     */
    public void queryRatioWeChatUserGraph(String playDay) {
        AnalyzeManager.instence().queryRatioWeChatUserGraph(CON_TIME_OUT, SO_TIME_OUT, playDay, mAnalyzeRatioListener);
    }

    /**
     * 2.5.5 总览各医院日关注用户数占比图
     * 功能描述：查询总览各医院日关注用户占比图数据
     */
    public void queryRatioAttentionUserWxGraph() {
        AnalyzeManager.instence().queryRatioAttentionUserWxGraph(CON_TIME_OUT, SO_TIME_OUT, mAnalyzeRatioListener);
    }

    /**
     * AP冷热排行榜
     * 查询AP冷热排行榜Top20。
     * @此方法去掉了
     **/
    @Deprecated
    public void queryApHeatRangeGraph(String instanceCode, String aphot) {
        AnalyzeManager.instence().queryApHeatRangeGraph(CON_TIME_OUT, SO_TIME_OUT, instanceCode, aphot, mAnalyzeLinearListener);
    }

    /**
     * 2.5.6AP日排行榜
     * 查询AP日排行榜Top20。
     * 访问路径改变为:/rankingap/rankingAp/queryRankingApGraph
     **/
    public void queryRankingApGraph(String instanceCode, String playDay) {
        AnalyzeManager.instence().queryRankingApGraph(CON_TIME_OUT, SO_TIME_OUT, instanceCode, playDay, mAnalyzeLinearListener);
    }

    /**
     * 2.5.7 AP周排行榜
     * 功能描述：查询AP周排行榜Top20
     */
    public void queryRankingApWeeklyGraph(String instanceCode, String weekly) {
        AnalyzeManager.instence().queryRankingApWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, instanceCode, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.5.8AP日走势图
     * 查询AP日走势图数据。
     * 访问路径改变为:/rankingap/rankingAp/queryTrendApGraph
     **/
    public void queryTrendApGraph(String instanceCode, String apmac) {
        AnalyzeManager.instence().queryTrendApGraph(CON_TIME_OUT, SO_TIME_OUT, instanceCode, apmac, mAnalyzeLinearListener);
    }

    /**
     * 2.5.9无线认证总览昨日关键指标信息
     * 功能描述：查询无线认证总览昨日关键指标信息。
     **/
    public void queryTrendAuthDayYestDayInfo(ListListener listListener) {
        AnalyzeManager.instence().queryTrendAuthDayYestDayInfo(CON_TIME_OUT, SO_TIME_OUT, listListener);
    }

    /**
     * 2.5.10无线认证总览走势图
     * 功能描述：查询无线认证总览走势图数据。
     * timeRange	String	时间间隔（28天，14天，7天）
     **/
    public void queryTrendAuthDayGraph(String timeRange) {
        AnalyzeManager.instence().queryTrendAuthDayGraph(CON_TIME_OUT, SO_TIME_OUT, timeRange, mAnalyzeLinearListener);
    }


    /**
     * 2.5.11无线认证总览数据列表
     * 功能描述：查询无线认证总览数据列表。
     * timeRange	String	时间间隔（28天，14天，7天）
     **/
    public void queryTrendAuthDayList(String timeRange, ListListener listListener) {
        AnalyzeManager.instence().queryTrendAuthDayList(CON_TIME_OUT, SO_TIME_OUT, timeRange, listListener);
    }

    /**
     * 2.5.12公众号用户总览昨日关键指标信息
     * 功能描述：查询公众号用户总览昨日关键指标信息。
     **/
    public void queryTrendUserWxYestDayInfo(ListListener listener) {
        AnalyzeManager.instence().queryTrendUserWxYestDayInfo(CON_TIME_OUT, SO_TIME_OUT, listener);
    }

    /**
     * 2.5.13公众号用户总览走势图
     * 功能描述：查询公众号用户总览走势图数据。
     * timeRange	String	时间间隔（28天，14天，7天）
     * viewType	String	显示内容区分（0：新关注人数 1：取消关注人数 2：净增关注人数 3：累计关注人数）
     **/
    public void queryTrendUserWxGraph(String viewType, String timeRange) {
        AnalyzeManager.instence().queryTrendUserWxGraph(CON_TIME_OUT, SO_TIME_OUT, viewType, timeRange, mAnalyzeLinearListener);
    }

    /**
     * 2.5.14公众号用户总览比较走势图
     * 功能描述：查询公众号用户总览比较走势图数据
     * viewType	String	显示内容区分（0：新关注人数 1：取消关注人数 2：净增关注人数 3：累计关注人数）
     * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
     * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
     **/
    public void queryTrendUserWxGraphCompare(String viewType, String compareDate1, String compareDate2) {
        AnalyzeManager.instence().queryTrendUserWxGraphCompare(CON_TIME_OUT, SO_TIME_OUT, viewType, compareDate1, compareDate2, mAnalyzeLinearListener);
    }

    /**
     * 2.5.15公众号用户总览数据列表
     * 功能描述：查询公众号用户总览数据列表。
     * 输入参数：
     * timeRange	String	时间间隔（28天，14天，7天）
     **/
    public void queryTrendUserWxList(String timeRange, ListListener listener) {
        AnalyzeManager.instence().queryTrendUserWxList(CON_TIME_OUT, SO_TIME_OUT, timeRange, listener);
    }

    /**
     * 2.5.16公众号用户总览比较数据列表
     * 功能描述：查询公众号用户总览比较数据列表。
     * 输入参数：
     * viewType	String	显示内容区分（0：新关注人数 1：取消关注人数 2：净增关注人数 3：累计关注人数）
     * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
     * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
     **/
    public void queryTrendUserWxCompareList(String compareDate1, String compareDate2, ListListener listener) {
        AnalyzeManager.instence().queryTrendUserWxCompareList(CON_TIME_OUT, SO_TIME_OUT, compareDate1, compareDate2, listener);
    }


    /**
     * 2.5.17无线认证按医院昨日关键指标信息
     * 功能描述：查询无线认证安医院昨日关键指标信息。
     * 输入参数：
     * instanceCode	String	医院编码
     **/
    public void queryTrendAuthDayAreaYestDayInfo(String instanceCode, ListListener listener) {
        AnalyzeManager.instence().queryTrendAuthDayAreaYestDayInfo(CON_TIME_OUT, SO_TIME_OUT, instanceCode, listener);
    }

    /**
     * 2.5.18无线认证按医院走势图
     * 功能描述：查询无线认证按医院走势图数据。
     * 输入参数：
     * viewType	String	显示内容区分（0：认证设备数 1：放行设备数 2：新增设备数 3：新增用户数）
     * instanceCode	String	医院编码
     * timeRange	String	时间间隔（28天，14天，7天）
     **/
    public void queryTrendAuthDayAreaGraph(String viewType, String instanceCode, String timeRange) {
        AnalyzeManager.instence().queryTrendAuthDayAreaGraph(CON_TIME_OUT, SO_TIME_OUT, viewType, instanceCode, timeRange, mAnalyzeLinearListener);
    }


    /**
     * 2.5.19无线认证按医院比较走势图
     * 功能描述：查询无线认证按医院比较走势图数据。
     * 输入参数：
     * viewType	String	显示内容区分（0：认证设备数 1：放行设备数 2：新增设备数 3：新增用户数）
     * instanceCode	String	医院编码
     * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
     * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
     **/
    public void queryTrendAuthDayAreaGraphCompare(String viewType, String instanceCode, String compareDate1, String compareDate2) {
        AnalyzeManager.instence().queryTrendAuthDayAreaGraphCompare(CON_TIME_OUT, SO_TIME_OUT, viewType, instanceCode, compareDate1, compareDate2, mAnalyzeLinearListener);
    }

    /**
     * 2.5.20无线认证按医院数据列表
     * 功能描述：查询无线认证按医院数据列表。
     * 输入参数：
     * instanceCode	String	医院编码
     * timeRange	String	时间间隔（28天，14天，7天）
     **/
    public void queryTrendAuthDayAreaList(String instanceCode, String timeRange, ListListener listener) {
        AnalyzeManager.instence().queryTrendAuthDayAreaList(CON_TIME_OUT, SO_TIME_OUT, instanceCode, timeRange, listener);
    }

    /**
     * 2.5.21无线认证按医院比较数据列表
     * 功能描述：查询无线认证按医院比较数据列表。
     * 输入参数：
     * instanceCode	String	医院编码
     * compareDate1	String	比较时间段1（yyyy-mm-dd--yyyy-mm-dd）
     * compareDate2	String	比较时间段2（yyyy-mm-dd--yyyy-mm-dd）
     **/
    public void queryTrendAuthDayAreaCompareList(String instanceCode, String compareDate1, String compareDate2, ListListener listener) {
        AnalyzeManager.instence().queryTrendAuthDayAreaCompareList(CON_TIME_OUT, SO_TIME_OUT, instanceCode, compareDate1, compareDate2, listener);
    }

    /**
     * 2.6.1就诊导航日排行榜
     * 查询就诊导航日排行榜Top20。
     **/
    public void queryRankingMedicalNavigationGraph(Map<String, String> headers, int en, String queryDate) {
        AnalyzeManager.instence().queryRankingMedicalNavigationGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.6.2就诊导航周排行榜
     * 查询就诊导航周排行榜Top20。
     **/
    public void queryRankingMedicalNavigationWeeklyGraph(Map<String, String> headers, int en, String weekly) {
        AnalyzeManager.instence().queryRankingMedicalNavigationWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.6.3医院资讯日排行榜
     * 查询医院资讯日排行榜Top20。
     **/
    public void queryRankingHospitalInfoGraph(Map<String, String> headers, int en, String queryDate) {
        AnalyzeManager.instence().queryRankingHospitalInfoGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.6.4医院资讯周排行榜
     * 查询医院资讯周排行榜Top20。
     **/
    public void queryRankingHospitalInfoWeeklyGraph(int en, String weekly) {
        AnalyzeManager.instence().queryRankingHospitalInfoWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, en, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.6.5出诊专家日排行榜
     * 查询出诊专家日排行榜Top20。
     **/
    public void queryRankingDoctorGraph(Map<String, String> headers, int en, String queryDate) {
        AnalyzeManager.instence().queryRankingDoctorGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, queryDate, mAnalyzeLinearListener);
    }

    /**
     * 2.6.6出诊专家周排行榜
     * 查询出诊专家周排行榜Top20。
     **/
    public void queryRankingDoctorWeeklyGraph(Map<String, String> headers, int en, String weekly) {
        AnalyzeManager.instence().queryRankingDoctorWeeklyGraph(CON_TIME_OUT, SO_TIME_OUT, headers, en, weekly, mAnalyzeLinearListener);
    }

    /**
     * 2.7.1启动次数走势图
     * 查询每日健康电视APP走势图数据。
     **/
    public void queryTrendAppStartupDayGraph(Map<String, String> headers) {
        AnalyzeManager.instence().queryTrendAppStartupDayGraph(CON_TIME_OUT, SO_TIME_OUT, headers, mAnalyzeLinearListener);
    }

    /**
     * 2.7.2首页模块走势图
     * 查询首页模块每个模块的日走势图（人数/次数）。
     **/
    public void queryMedicalHealthGraph(Map<String, String> headers, String healthId) {
        AnalyzeManager.instence().queryMedicalHealthGraph(CON_TIME_OUT, SO_TIME_OUT, headers, healthId, mAnalyzeLinearListener);
    }


    private DataFilterListener mDataFilterListener = null;

    /**
     * 设置所有筛选的监听
     **/
    public void setDataFilterListener(DataFilterListener dataFilterListener) {
        mDataFilterListener = dataFilterListener;
    }

    /**
     * 2.8.1获取健康视频总览排行榜的查询条件列表
     * 获取健康视频总览排行榜的查询条件列表
     **/
    public void queryMedicalTvOverviewRankingSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryMedicalTvOverviewRankingSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.2获取健康视频走势图的查询条件列表
     * 获取健康视频走势图的查询条件列表。
     **/
    public void queryMedicalTvTrendSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryMedicalTvTrendSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.3获取健康视频排行榜的查询条件列表
     * 获取健康视频排行榜的查询条件列表。
     **/
    public void queryMedicalTvRankingSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryMedicalTvRankingSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.4获取娱乐视频总览排行榜的查询条件列表
     * 获取娱乐视频总览排行榜的查询条件列表。
     **/
    public void queryTvRankingOverviewSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryTvRankingOverviewSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.5获取娱乐视频各地区排行榜的查询条件列表
     * 获取娱乐视频各地区排行榜的查询条件列表。
     **/
    public void queryTvRankingSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryTvRankingSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.6获取娱乐视频各地区走势图的查询条件列表
     **/
    public void queryTvTrendSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryTvTrendSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.7获取就诊导航的查询条件列表
     **/
    public void queryMedicalNavigationSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryMedicalNavigationSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.8获取出诊专家的查询条件列表
     **/
    public void queryExpertSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryExpertSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }


    /**
     * 2.8.9获取健康电视APP首页模块的查询条件列表
     **/
    public void queryHomePageModuleSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryHomePageModuleSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 获取AP冷热排行榜的查询条件列表
     * @此方法去掉了
     **/
    @Deprecated
    public void queryHeatRangeSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryHeatRangeSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.10获取AP排行榜的查询条件列表
     **/
    public void queryAPDayRankingSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryAPDayRankingSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.11获取AP日走势图的查询条件列表
     **/
    public void queryHeatTrendSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryHeatTrendSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.12获取无线认证总览的查询条件列表
     * 功能描述：获取无线认证总览的查询条件列表。
     **/
    public void queryTrendAuthDaySelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryTrendAuthDaySelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.13获取公众号用户总览的查询条件列表
     * 功能描述：获取公众号用户总览的查询条件列表。
     **/
    public void queryTrendUserWxSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryTrendUserWxSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.14获取无线认证安医院的查询条件列表
     * 功能描述：获取无线认证安医院的查询条件列表。
     **/
    public void queryTrendAuthDayAreaSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryTrendAuthDayAreaSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }

    /**
     * 2.8.15 获取报表无线认证安医院的查询条件列表
     * 功能描述：获取报表无线认证安医院的查询条件列表
     */
    public void queryExcelTrendAuthDayAreaSelCondList(DataFilterListener dataFilterListener) {
        AnalyzeManager.instence().queryExcelTrendAuthDayAreaSelCondList(CON_TIME_OUT, SO_TIME_OUT, dataFilterListener);
    }
}
