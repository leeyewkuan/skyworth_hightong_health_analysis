package com.sh.pangea.dsa.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sh.lib.dsa.bean.filter.FilterCondition;
import com.sh.pangea.dsa.R;

import java.util.List;

/**
 * Data_Sys_Analyse
 * Created by LB on 2017/1/9,15:41
 */

public class SingleTextAdapter extends BaseAdapter {
    private Context context;
    private List<FilterCondition> comboxList;

    public SingleTextAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        int n = 0;
        if (comboxList != null) {
            n = comboxList.size();
        }
        return n;
    }

    @Override
    public Object getItem(int position) {
        return comboxList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View inflate = View.inflate(context, R.layout.text_screen_adapter, null);
        TextView tv = (TextView) inflate.findViewById(R.id.tv_week);
        FilterCondition filterCondition = comboxList.get(position);
        if (filterCondition != null) {
            String name = filterCondition.getName();
            if (!TextUtils.isEmpty(name)) {
                tv.setText(name);
            }
        }
        if (position == select) {
            tv.setTextColor(context.getResources().getColor(R.color.colorGreen));
            tv.setBackgroundResource(R.mipmap.bg_xuanji_focus);
        } else {
            tv.setTextColor(context.getResources().getColor(R.color.black));
            tv.setBackgroundResource(R.mipmap.bg_xuanji);
        }

        return inflate;
    }

    public void updata(List<FilterCondition> comboxList) {
        this.comboxList = comboxList;
        notifyDataSetChanged();
    }

    private int select;

    public void setSelect(int select) {
        this.select = select;
        notifyDataSetChanged();
    }
}
